<?php
/**
 * RssFeedPlugin for phplist.
 *
 * This file is a part of RssFeedPlugin.
 *
 * @category  phplist
 *
 * @author    Duncan Cameron
 * @copyright 2015 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 */

namespace phpList\plugin\RssFeedPlugin\Controller;

use DateTimeZone;
use phpList\plugin\Common\Context;
use phpList\plugin\Common\Controller;
use phpList\plugin\RssFeedPlugin\DAO;
use PicoFeed\Config\Config;
use PicoFeed\Parser\Item;
use PicoFeed\PicoFeedException;
use PicoFeed\Reader\Reader;

/**
 * This class retrieves items from RSS feeds.
 */
class Get extends Controller
{
    private $context;
    private $dao;

    /**
     * Convert characters above the BMP to numeric entities to avoid a restriction of the MySQL utf8 character set to
     * only three bytes.
     *
     * @param string $content
     *
     * @return string encoded content
     */
    private function convertToEntities($content)
    {
        $convmap = array(0x10000, 0x10FFFF, 0, 0xFFFFFF);

        return mb_encode_numericentity($content, $convmap);
    }

    private function getCustomElementsValues(array $customElements, Item $item)
    {
        $values = array();

        foreach ($customElements as $c) {
            $parts = explode(':', $c, 2);

            if (count($parts) == 1) {
                $values[$c] = $item->getTag($c);
            } else {
                if ($item->hasNamespace($parts[0])) {
                    $tagValues = $item->getTag($c);

                    if (count($tagValues) > 0) {
                        $values[$c] = $tagValues[0];
                    }
                }
            }
        }

        return $values;
    }

    /**
     * Get the content for an item.
     * For an RSS feed use the description element if present.
     * For an ATOM feed use the summary element if present.
     *
     * @param PicoFeed\Parser\Item $item
     *
     * @return string the content for the item
     */
    private function getItemContent(Item $item)
    {
        $content = '';
        $values = $item->getTag('encoded');

        if ($values === false || count($values) == 0) {
            $values = $item->getTag('summary');

            if ($values === false || count($values) == 0) {
                $content = $item->getContent();
            } else {
                $content = $values[0];
            }
        } else {
            $content = $values[0];
        }

        return $content;
    }

    private function getRssFeeds(DAO $dao, callable $output)
    {
        // function random_color_part() {
        //     return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
        // }

        // function random_color() {
        //     return random_color_part() . random_color_part() . random_color_part();
        // }

        $utcTimeZone = new DateTimeZone('UTC');
        $config = new Config();
        $config->setContentFiltering(false);
        $feeds = $dao->activeFeeds();

        if (count($feeds) == 0) {
            $output(s('There are no active RSS feeds to fetch'));

            return;
        }
        $customElementsConfig = getConfig('rss_custom_elements');
        $customElements = $customElementsConfig === ''
            ? array()
            : explode("\n", $customElementsConfig);

        foreach ($feeds as $row) {
            $feedId = $row['id'];
            $feedUrl = $row['url'];

            try {
                $output(s('Fetching') . ' ' . $feedUrl);
                $reader = new Reader($config);
                $resource = $reader->download($feedUrl, $row['lastmodified'], $row['etag']);

                if (!$resource->isModified()) {
                    $output(s('Not modified'));
                    continue;
                }

                $parser = $reader->getParser(
                    $resource->getUrl(),
                    $resource->getContent(),
                    $resource->getEncoding()
                );
                $feed = $parser->execute();
                $itemCount = 0;
                $newItemCount = 0;

                $myCounter = -1;
                $anotherCounter = -1;

                foreach ($feed->getItems() as $item) {
		   							$xml = simplexml_load_file($feedUrl);
                    ++$myCounter;

                    ++$itemCount;
                    $date = $item->getDate();
                    $date->setTimeZone($utcTimeZone);
                    $published = $date->format('Y-m-d H:i:s');
										$category = $xml->channel->item[$myCounter]->category;
                    // Kim
                    if (empty($category)) {
											$category = 'walbro_index';
										}

                    $itemId = $dao->addItem($item->getId(), $published, $feedId, $category);

                    if ($itemId > 0) {
                        ++$newItemCount;

                        unset($a);
                        $a[] = '<table align="left" cellpadding="0" cellspacing="0" margin="0" padding="0" style="width:auto;border:collapse;display:inline-block;font-family:Helvetica, Arial, sans-serif;"> <tbody> <tr> <td style="padding:1px;"><span style="font-size:12px;text-decoration: none; color: #666666;line-height:15px;display:inline-block;padding:2px 0;font-family:Helvetica, Arial, sans-serif;">Tags: </a></td></tr></tbody></table>';
                        $sepa = '';
                        foreach ($xml->channel->item[$myCounter]->tag as $data) {
                            // $color = random_color();
                            $string1 = '<table align="left" cellpadding="0" cellspacing="0" margin="0" padding="0" style="font-family:Helvetica, Arial, sans-serif;width:auto;border:collapse;display:inline-block;"><tbody><tr>';
                            $a[] = $string1.'<td style="padding:1px;"><a href="https://www.archintel.com/search?tag='.$data.'" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;text-decoration: none; color: white; background-color: #4599c5; line-height:15px;display:inline-block;padding:2px 0;">&nbsp;&nbsp;'.$data.'&nbsp;&nbsp;</a></td></tr></tbody></table>';
                            ++$anotherCounter;
                        }      

                        $itemContent = $this->getItemContent($item);
                                                $itemContent = $this->convertToEntities($itemContent);
                        $dao->addItemData(
                            $itemId,
                            array(
                                'title' => $item->getTitle(),
                                'url' => $item->getUrl(),
                                'language' => $item->getLanguage(),
                                'author' => $item->getAuthor(),
                                'image' => $xml->channel->item[$myCounter]->children( 'media', True )->content->attributes()['url'],
                                'tag' => $a !== null ? implode(' ', $a) : null ,
                                'category' => $category,
				                'enclosureurl' => $item->getEnclosureUrl(),
                                'enclosuretype' => $item->getEnclosureType(),
                                'content' => $itemContent,
                                'rtl' => $item->isRTL(),
                            ) + $this->getCustomElementsValues($customElements, $item)
                        );
                    }
                }
                $etag = $resource->getEtag();
                $lastModified = $resource->getLastModified();
                $dao->updateFeed($feedId, $etag, $lastModified);

                $line = s('%d items, %d new items', $itemCount, $newItemCount);
                $output($line);

                if ($newItemCount > 0) {
                    logEvent(s('Feed') . " $feedUrl $line");
                }
            } catch (PicoFeedException $e) {
                $output($e->getMessage());
            }
        }
    }

    protected function actionDefault()
    {
        $this->context->start();
        $this->getRssFeeds($this->dao, [$this->context, 'output']);
        $this->context->finish();
    }

    public function __construct(DAO $dao, Context $context)
    {
        parent::__construct();
        $this->dao = $dao;
        $this->context = $context;
    }
}
