<?php
/**
 * RssFeedPlugin for phplist.
 *
 * This file is a part of RssFeedPlugin.
 *
 * @category  phplist
 *
 * @author    Duncan Cameron
 * @copyright 2015 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 */
 
/**
 * Registers plugin with phplist
 * Provides hooks into message processing.
 */
class RssFeedPlugin extends phplistPlugin
{

    const VERSION_FILE = 'version.txt';
    const OLDEST_FIRST = 1;
    const LATEST_FIRST = 2;
    const TWITTER_PAGE = 'twitterfeed';

    private $dao;
    private $rssHtml;
    // ARNOLDJOS HTML 
		private $rssTopList;
		private $rssWalbroIndex;
    private $rssHtmls = array();
    private $rssItems = array();
    private $allItems;
		private $categories;
    // END ARNOLDJOSs

    private $rssText;
    private $errorLevel;

    public $name = 'RSS Feed Manager';
    public $authors = 'Duncan Cameron';
    public $description = 'Send campaigns that contain RSS feed items';
    public $documentationUrl = 'https://resources.phplist.com/plugin/rssfeed';

    //ARNOLDJOS FUNCTIONS
    private function setRssItems($mid) {
			foreach($this->categories as $category) {
				$this->rssItems[$category] = $this->dao->messageFeedItems($mid, getConfig('rss_maximum'), true, $category);
			}     
			$this->allItems = $this->dao->getAllDataCategories($mid, getConfig('rss_maximum'), true);
    }

    private function setRssHtmls($messageData) {
        foreach($this->categories as $category) {
            $this->rssHtmls[$category] =  $this->generateItemHtml($this->rssItems[$category], $messageData['rss_template']);
        }
    }

    public $commandlinePluginPages = array(
        'get',
    );

    public $publicPages = array(self::TWITTER_PAGE);

    public $topMenuLinks = array(
        'view' => array('category' => 'campaigns'),
        'get' => array('category' => 'campaigns'),
        'delete' => array('category' => 'campaigns'),
    );

    public $DBstruct = array(
        'feed' => array(
            'id' => array('integer not null primary key auto_increment', 'ID'),
            'url' => array('text not null', ''),
            'etag' => array('varchar(100) not null', ''),
            'lastmodified' => array('varchar(100) not null', ''),
        ),
        'item' => array(
            'id' => array('integer not null primary key auto_increment', 'ID'),
            'uid' => array('varchar(100) not null', 'unique id'),
            'feedid' => array('integer not null', 'fk to feed'),
            'published' => array('datetime not null', 'published datetime'),
            'added' => array('datetime not null', 'datetime added'),
            'index_1' => array('feedpublishedindex (feedid, published)', ''),
            'index_2' => array('feeduidindex (feedid, uid)', ''),
        ),
        'item_data' => array(
            'itemid' => array('integer not null', 'fk to item'),
            'property' => array('varchar(100) not null', ''),
            'value' => array('text', ''),
            'primary key' => array('(itemid, property)', ''),
        ),
    );

    private function isRssMessage(array $messageData)
    {
        return isset($messageData['rss_feed']) && $messageData['rss_feed'] != '';
    }

    private function validateFeed($feedUrl)
    {
        $reader = new PicoFeed\Reader\Reader();
        $resource = $reader->download($feedUrl);
        $parser = $reader->getParser(
            $resource->getUrl(),
            $resource->getContent(),
            $resource->getEncoding()
        );
        $feed = $parser->execute();
    }

    private function replaceProperties($template, $properties)
    {
        foreach ($properties as $key => $value) {
            $template = str_ireplace("[$key]", $value, $template);
        }
        return $template;
    }

    private function sampleItems()
    {
        return array(
            array(
                'published' => date('Y-m-d H:i:s', time() - 10000),
                'title' => 'These are just some sample entries for the test RSS message',
                'content' => '<p>The phpList manual is available online, or you can download it to your favourite device.</p>',
                'url' => 'https://www.phplist.org/manual/',
            ),
            array(
                'published' => date('Y-m-d H:i:s', time() - 8000),
                'title' => 'Adding your first Subscribers ',
                'content' => '<p>phpList Manual chapter explaining how to add subscribers.</p>',
                'url' => 'https://www.phplist.org/manual/ch006_adding-your-first-subscribers.xhtml',
            ),
            array(
                'published' => date('Y-m-d H:i:s', time() - 6000),
                'title' => 'Composing your first campaign',
                'content' => '<p>How to write your first campaign in phpList.</p>',
                'url' => 'https://www.phplist.org/manual/ch007_sending-your-first-campaign.xhtml',
            ),
            array(
                'published' => date('Y-m-d H:i:s', time() - 4000),
                'title' => 'Sending a campaign',
                'content' => '<p>The phpList manual pages, explaining how to send your campaign.</p>',
                'url' => 'https://www.phplist.org/manual/ch008_your-first-campaign.xhtml',
            ),
            array(
                'published' => date('Y-m-d H:i:s', time()),
                'title' => 'Campaign Statistics',
                'content' => '<p>Once you have sent your campaign, just sit back and watch the statistics grow.</p>',
                'url' => 'https://www.phplist.org/manual/ch009_basic-campaign-statistics.xhtml',
            ),
        );
    }

    private function generateItemHtml(array $items, $customTemplate)
    {
        $htmltemplate = trim($customTemplate) === ''
            ? getConfig('rss_htmltemplate')
            : $customTemplate;
        $html = '';

        if ($order == self::LATEST_FIRST) {
            $items = array_reverse($items);
        }

        foreach ($items as $item) {
            $d = new DateTime($item['published']);
            $html .= $this->replaceProperties(
                $htmltemplate,
                array(
                    'published' => $d->format('d/m/Y H:i'),
                    'title' => htmlspecialchars($item['title']),
                ) + $item
            );
        }

        return $html;
    }

    private function newSubject($subject, array $items)
    {
        $size = count($items);

        if ($size == 0) {
            $titleReplace = 'No title';
        } else {
            $item = $items[$size - 1];
            $titleReplace = $item['title'];

            if ($size > 1 && ($suffix = getConfig('rss_subjectsuffix'))) {
                $titleReplace .= $suffix;
            }
        }

        return $this->replaceProperties(
            $subject,
            array('RSSITEM:TITLE' => $titleReplace)
        );
    }

    private function modifySubject(array $messageData, array $items)
    {
        global $MD;

        $MD[$messageData['id']]['subject'] = $this->newSubject($messageData['subject'], $items);
    }

    private function itemsForTestMessage($mid)
    {
        $this->categories = $this->dao->getCategories($mid);
        $items = $this->dao->messageFeedItems($mid, getConfig('rss_maximum'), false, $this->categories[0]);
        $this->setRssItems($mid);

        if (count($items) == 0) {
            $items = $this->sampleItems();
        }

        return $items;
    }
		
    public function __construct()
    {
        $this->pageTitles = array(
            'get' => s('Fetch RSS items'),
            'view' => s('View RSS items'),
            'delete' => s('Delete outdated RSS items'),
        );
        $this->settings = array(
            'rss_minimum' => array(
                'description' => s('Minimum number of items to send in an RSS email'),
                'type' => 'integer',
                'value' => 1,
                'allowempty' => 0,
                'min' => 1,
                'max' => 50,
                'category' => 'RSS',
            ),
            'rss_maximum' => array(
                'description' => s('Maximum number of items to send in an RSS email'),
                'type' => 'integer',
                'value' => 30,
                'allowempty' => 0,
                'min' => 1,
                'max' => 50,
                'category' => 'RSS',
            ),
            'rss_htmltemplate' => array(
                'description' => s('Item HTML template'),
                'type' => 'textarea',
                'value' => '
                <a href="[URL]"><b>[TITLE]</b></a><br/>
                [PUBLISHED]<br/>
                [CONTENT]
                <hr/>',
                'allowempty' => 0,
                'category' => 'RSS',
            ),
            'rss_subjectsuffix' => array(
                'description' => s('Text to append when the title of the latest item is used in the subject'),
                'type' => 'text',
                'value' => '',
                'allowempty' => true,
                'category' => 'RSS',
            ),
            'rss_custom_elements' => array(
                'description' => s('Additional feed elements to be included in each item\'s data'),
                'type' => 'textarea',
                'value' => '',
                'allowempty' => true,
                'category' => 'RSS',
            ),
        );
        $this->errorLevel = E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_STRICT;
        $this->coderoot = dirname(__FILE__) . '/' . __CLASS__ . '/';

        parent::__construct();
        $this->version = (is_file($f = $this->coderoot . self::VERSION_FILE))
            ? file_get_contents($f)
            : '';
    }

    public function dependencyCheck()
    {
        global $plugins;

        return array(
            'Common plugin v3.7.5 or later installed' => (
                phpListPlugin::isEnabled('CommonPlugin')
                && version_compare($plugins['CommonPlugin']->version, '3.7.5') >= 0
            ),
            'View in Browser plugin v2.4.0 or later installed' => (
                phpListPlugin::isEnabled('ViewBrowserPlugin')
                && version_compare($plugins['ViewBrowserPlugin']->version, '2.4.0') >= 0
                || !phpListPlugin::isEnabled('ViewBrowserPlugin')
            ),
            'phpList version 3.2.0 or later' => version_compare(VERSION, '3.2') > 0,
            'PHP version 5.4.0 or later' => version_compare(PHP_VERSION, '5.4') > 0,
            'iconv extension installed' => extension_loaded('iconv'),
            'xml extension installed' => extension_loaded('xml'),
            'dom extension installed' => extension_loaded('dom'),
            'SimpleXML extension installed' => extension_loaded('SimpleXML'),
            'Multibyte String extension installed' => extension_loaded('mbstring'),
        );
    }

    /**
     * Use this method as a hook to create the dao
     * Need to create autoloader because of the unpredictable order in which plugins are called.
     */
    public function sendFormats()
    {
        global $plugins;

        require_once $plugins['CommonPlugin']->coderoot . 'Autoloader.php';
        $this->dao = new phpList\plugin\RssFeedPlugin\DAO(new phpList\plugin\Common\DB());
    }

    public function adminmenu()
    {
        return $this->pageTitles;
    } 

    public function cronJobs()
    {
        return array(
            array(
                'page' => 'get',
                'frequency' => 60,
            ),
        );
    }

    /*
     *  Methods for composing a campaign
     */
    public function sendMessageTab($messageid = 0, $data = array())
    {
        $feedUrl = isset($data['rss_feed']) ? htmlspecialchars($data['rss_feed']) : '';
        $feedUrl2 = isset($data['rss_feed2']) ? htmlspecialchars($data['rss_feed2']) : '';
        //ORDER BY DATE
        // $order = CHtml::dropDownList(
        //     'rss_order',
        //     isset($data['rss_order']) ? $data['rss_order'] : self::OLDEST_FIRST,
        //     array(self::OLDEST_FIRST => s('Oldest items first'), self::LATEST_FIRST => s('Latest items first'))
        // );
        // <label>$orderLabel
        // $order</label>
        $content1 = '<!--[if mso | IE]>
  <table
          align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
  >
    <tr>
      <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
  <![endif]-->
  <div style="background:#F6F6F6;background-color:#F6F6F6;Margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#F6F6F6;background-color:#F6F6F6;width:100%;">
      <tbody>
      <tr>
        <td style="border-bottom:1px solid #A09E9E;border-left:1px solid #CCCCCC;border-right:1px solid #CCCCCC;direction:ltr;font-size:0px;padding-top:10px;text-align:center;vertical-align:top;">
          <!--[if mso | IE]>
          <table role="presentation" border="0" cellpadding="0" cellspacing="0">

            <tr>

              <td
                      class="" style="vertical-align:top;width:180px;"
              >
          <![endif]-->
          <div class="mj-column-per-30 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
              <tbody>
              <tr>
                <td style="vertical-align:top;padding:0px;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                    <tr>
                      <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                          <tbody>
                          <tr>
                            <td style="width:150px;"> <img height="auto" src="[IMAGE]" style="border:0;border-radius:7px;display:block;outline:none;text-decoration:none;height:auto;width:100%;"
                                                           width="150" /> </td>
                          </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          <!--[if mso | IE]>
          </td>

          <td
                  class="" style="vertical-align:top;width:300px;"
          >
          <![endif]-->
          <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
              <tbody>
              <tr>
                <td style="vertical-align:top;padding:0px;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                    <tr>
                      <td align="left" style="font-size:0px;padding:10px;word-break:break-word;">
                        <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;"> <a href="[URL]" style="text-decoration:none;color:#1596d9;font-size:14px;font-weight: bold; letter-spacing: 1px;">
                          [TITLE]
                        </a> </div>
                      </td>
                    </tr>
                    <tr>
                      <td align="justify" style="font-size:0px;padding:10px;padding-top:10px;word-break:break-word;">
                        <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:14px;line-height:18px;text-align:justify;color:#666666;">[CONTENT]</div>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" style="font-size:0px;padding:10px;padding-top:15px;word-break:break-word;">
                        <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;"> <a style="color:#1596d9;text-decoration: none; text-transform:uppercase; font-weight: bold; font-size: 14px;letter-spacing: 2px;" href="[URL]">
                          Read More
                        </a> </div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          <!--[if mso | IE]>
          </td>

          </tr>

          </table>
          <![endif]-->
        </td>
      </tr>
      </tbody>
    </table>
  </div>
  <!--[if mso | IE]>
  </td>
  </tr>
  </table>

  <table
          align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
  >
    <tr>
      <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
  <![endif]-->';
        $content2 = '<ul style="
    margin-top: 0px;
    margin-bottom: 0px;
">
<li style="padding-bottom:10px;"> <a href="[URL]"
                                                           data-type="text" style="text-decoration:none;color:#4599c5;line-height:1;font-size:16px">
                        [TITLE]
                      </a> </li>
</ul>';
        $content3 = '<!--[if mso | IE]>
  <table
          align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
  >
    <tr>
      <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
  <![endif]-->


  <div  style="background:#B2B2B2;background-color:#B2B2B2;Margin:0px auto;max-width:600px;">

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#B2B2B2;background-color:#B2B2B2;width:100%;"
    >
      <tbody>
      <tr>
        <td
                style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;"
        >
          <!--[if mso | IE]>
          <table role="presentation" border="0" cellpadding="0" cellspacing="0">

            <tr>

              <td
                      class="" style="vertical-align:middle;width:600px;"
              >
          <![endif]-->

          <div
                  class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;"
          >

            <table
                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:middle;" width="100%"
            >

              <tr>
                <td
                        align="center" style="font-size:0px;padding:0px;word-break:break-word;"
                >

                  <table
                          align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                  >
                    <tbody>
                    <tr>
                      <td  style="width:600px;">

                        <img
                                height="auto" src="[IMAGE]" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;" width="600"
                        />

                      </td>
                    </tr>
                    </tbody>
                  </table>

                </td>
              </tr>

            </table>

          </div>

          <!--[if mso | IE]>
          </td>

          </tr>

          </table>
          <![endif]-->
        </td>
      </tr>
      </tbody>
    </table>

  </div>


  <!--[if mso | IE]>
  </td>
  </tr>
  </table>
  <![endif]-->';
        $template = isset($data['rss_template']) ? htmlspecialchars($data['rss_template']) : $content1;
        $template2 = isset($data['rss_template2']) ? htmlspecialchars($data['rss_template2']) : $content2;
        $template3 = isset($data['rss_template3']) ? htmlspecialchars($data['rss_template3']) : $content3;
        $feedLabel = s('RSS feed URL');
        $feedLabel2 = s('RSS feed URL 2');
        $orderLabel = s('How to order feed items');
        $templateLabel = s('Article Template');
        $templateLabel2 = s('List Template');
        $templateLabel3 = s('Index Template');
        $html = <<<END
    <label>$feedLabel
		<input type="text" name="rss_feed" value="$feedUrl" /></label>
		<label>$feedLabel2
    <input type="text" name="rss_feed2" value="$feedUrl2" /></label>
    <label>$templateLabel</label><textarea name="rss_template" rows="10" cols="40">$template</textarea>
    <label>$templateLabel2</label><textarea name="rss_template2" rows="10" cols="40">$template2</textarea>
    <label>$templateLabel3</label><textarea name="rss_template3" rows="10" cols="40">$template3</textarea>

END;

        return $html;
    }

    public function sendMessageTabTitle($messageid = 0)
    {
        return 'RSS';
    }

    public function sendMessageTabInsertBefore()
    {
        return 'Format';
    }

    /**
     * Generate RSS items for a test message.
     *
     * @param array $messageData message fields
     */
    public function sendTestAllowed($messageData)
    {
        if (!$this->isRssMessage($messageData)) {
            $this->rssHtml = null;
            return true;
        }

        $items = $this->itemsForTestMessage($messageData['id']);
        $this->setRssHtmls($messageData);

        $this->rssHtml = $this->generateItemHtml($items, $messageData['rss_template']);
        //ARNOLDJOS RSSHTML 2
				$this->rssTopList = $this->generateItemHtml($this->allItems, $messageData['rss_template2']);
				if (isset($this->rssItems['walbro_index'])) {
					$this->rssWalbroIndex = $this->generateItemHtml($this->rssItems['walbro_index'], $messageData['rss_template3']);
				}	
				
        $this->rssText = HTML2Text($this->rssHtml);
        $this->modifySubject($messageData, $items);

        return true;
    }

    /**
     * Provide a read-only view of the RSS fields for a campaign.
     *
     * @param int   $messageId   message id
     * @param array $messageData message fields
     */
    public function viewMessage($messageId, array $messageData)
    {
        if (!$this->isRssMessage($messageData)) {
            return false;
        }
        $html = $this->sendMessageTab($messageId, $messageData);
        $html = <<<END
    <fieldset disabled>
    $html
    </fieldset>
END;

        return array('RSS', $html);
    }

    /**
     * Validate that the RSS fields have been entered and the feed url is valid.
     *
     * @param array $messageData message fields
     *
     * @return string empty string for success otherwise an error message
     */
    public function allowMessageToBeQueued($messageData = array())
    {
        if (!$this->isRssMessage($messageData)) {
            return '';
        }

				$feedUrl = $messageData['rss_feed'];
				
			

        if (!preg_match('/^http/i', $feedUrl)) {
            return "Invalid URL $feedUrl for RSS feed";
        }
				$exists = $this->dao->feedExists($feedUrl);
				
				if (isset($messageData['rss_feed2']) && $messageData['rss_feed2'] != '') {
					$feedUrl2 = $messageData['rss_feed2'];
					$exists2 = $this->dao->feedExists($feedUrl2);

					if (!$exists2) {
							try {
									$this->validateFeed($feedUrl2);
							} catch (PicoFeed\PicoFeedException $e) {
									return s('Failed to fetch URL %s %s', $feedUrl2, $e->getMessage());
							}
							$this->dao->addFeed($feedUrl2);
					}
				}
        
 
        if (!$exists) {
            try {
								$this->validateFeed($feedUrl);
								
            } catch (PicoFeed\PicoFeedException $e) {
                return s('Failed to fetch URL %s %s', $feedUrl, $e->getMessage());
            }
            $this->dao->addFeed($feedUrl);
        }

        /*if (stripos($messageData['message'], '[RSS]') === false) {
            if ($messageData['template'] === 0) {
                $templateHasPlaceholder = false;
            } else {
                $templateBody = $this->dao->templateBody($messageData['template']);
                $templateHasPlaceholder = stripos($templateBody, '[RSS]') !== false;
            }

            if (!$templateHasPlaceholder) {
                return s('Must have [RSS] placeholder in an RSS message');
            }
        }*/

        if (!USE_REPETITION) {
            return s('Campaign repetition must be enabled in config.php');
        }

        if ($messageData['repeatinterval'] == 0) {
            return s('Repeat interval must be selected for an RSS campaign');
        }

        return '';
    }

    /**
     * Use this hook to see whether any RSS messages have sufficient items to be sent.
     * If not then the embargo of the campaign is moved forward to avoid sending a message with no RSS content.
     */
    public function processQueueStart()
    {
        $level = error_reporting($this->errorLevel);
				
        foreach ($this->dao->readyRssMessages() as $mid) {
            $items = $this->dao->getAllDataCategories($mid, getConfig('rss_maximum'), true);
					
            if (count($items) < getConfig('rss_minimum')) {
                $count = $this->dao->reEmbargoMessage($mid);

                if ($count > 0) {
                    logEvent(s('Embargo advanced for RSS message %s', $mid));
                } else {
                    $this->dao->setMessageSent($mid);
                    repeatMessage($mid);
                    logEvent(s('RSS message %d marked as "sent" because because the RSS feed is empty', $mid));
//                    logEvent(s('RSS message %d didn\'t send because the RSS feed is empty', $mid));
                }
            }
        }
        error_reporting($level);
    }

    /**
     * Use this hook to generate the html and plain text of the RSS items and modify the subject.
     *
     * @param array $messageData message fields
     */
    public function campaignStarted($messageData = array())
    {
        if (!$this->isRssMessage($messageData)) {
            $this->rssHtml = null;
            return;
        }
        $this->categories = $this->dao->getCategories($messageData['id']);
        $this->setRssItems($messageData['id']);
        $this->setRssHtmls($messageData);
        $items = $this->dao->messageFeedItems($messageData['id'], getConfig('rss_maximum'), true, $this->categories[0]);
				$this->rssHtml = $this->generateItemHtml($items, $messageData['rss_template']);
				if (isset($this->rssItems['walbro_index'])) {
					$this->rssWalbroIndex = $this->generateItemHtml($this->rssItems['walbro_index'], $messageData['rss_template3']);
				}	
        //My RSS
        $this->rssTopList = $this->generateItemHtml($this->allItems, $messageData['rss_template2']);
        $this->rssText = HTML2Text($this->rssHtml);
        $this->modifySubject($messageData, $this->allItems);
    }
 
    /**
     * When a campaign has finished sending replace the placeholder with the actual RSS content used, and modify
     * the subject to the actual subject used.
     *
     * @param int   $messageId   message id
     * @param array $messageData message fields
     */
    public function processSendingCampaignFinished($messageId, array $messageData)
    {
        global $MD;

        if (!$this->isRssMessage($messageData)) {
            return;
        }

        /*if (stripos($messageData['message'], '[RSS]') !== false) {
            $content = str_ireplace('[RSS]', $this->rssHtml, $messageData['message']);
            $this->dao->setMessage($messageId, $content);
        }*/
        $this->dao->setSubject($messageId, $MD[$messageData['id']]['subject']);
    }

    /**
     * Replace the placeholder by the html RSS content.
     *
     * @param int    $messageid   the message id
     * @param string $content     the message content
     * @param string $destination destination email address
     * @param array  $userdata    user fields
     *
     * @return string
     */
    public function parseOutgoingHTMLMessage($messageid, $content, $destination = '', $userdata = array())
    {
        if ($this->rssHtml === null) {
            return $content;
        }

        $the_message = str_ireplace('[RSS]', $this->rssHtml, $content);
				$the_message = str_ireplace('[CONTENT1]', $this->rssTopList, $the_message);
				if (isset($this->rssWalbroIndex)) {
					$the_message = str_ireplace('[walbro_index]', $this->rssWalbroIndex, $the_message);
				}
        foreach ($this->categories as $category) {
						if ($category === 'walbro_index') {
							continue;
						}
            $the_message = str_ireplace('['.$category.']', $this->rssHtmls[$category], $the_message);
				}
				
        // return str_ireplace('[RSS]', $this->rssHtml, $content);
        return $the_message;
    }

    /**
     * Replace the placeholder by the text RSS content.
     *
     * @param int    $messageid   the message id
     * @param string $content     the message content
     * @param string $destination destination email address
     * @param array  $userdata    user fields
     *
     * @return string
     */
    public function parseOutgoingTextMessage($messageid, $content, $destination = '', $userdata = array())
    {
        if ($this->rssHtml === null) {
            return $content;
        }

        return str_ireplace('[RSS]', $this->rssText, $content);
    }

    /**
     * Called by ViewBrowser plugin to manipulate template and message.
     * Gets the RSS HTML content and modifies the message subject.
     *
     * @param string &$templateBody the body of the template
     * @param array  &$messageData  the message data
     */
    public function viewBrowserHook(&$templateBody, array &$messageData)
    {
        if (!$this->isRssMessage($messageData)) {
            return;
        }

        if ($messageData['status'] == 'draft') {
            $items = $this->itemsForTestMessage($messageData['id']);
        } else {
            //$items = $this->dao->messageFeedItems($messageData['id'], getConfig('rss_maximum'), false, $this->categories[0]);
            $items = $this->dao->getAllDataCategories($messageData['id'], getConfig('rss_maximum'), true);
        }

        $this->categories = $this->dao->getCategories($messageData['id']);
        $this->setRssItems($messageData['id']);
        $this->setRssHtmls($messageData);
        $this->rssHtml = $this->generateItemHtml($items, $messageData['rss_template']);
				$this->rssTopList = $this->generateItemHtml($this->allItems, $messageData['rss_template2']);
				if (isset($messageData[rss_feed2]) && $messageData[rss_feed2] !== '' ) {
					$this->rssWalbroIndex = $this->generateItemHtml($this->rssItems['walbro_index'], $messageData['rss_template3']);
				}
        $messageData['subject'] = $this->newSubject($messageData['subject'], $items);
    }

    /**
     * Called when a campaign is being copied.
     * Allows this plugin to specify which rows of the messagedata table should also
     * be copied.
     *
     * @return array rows of messagedata table that should be copied
     */
    public function copyCampaignHook()
    {
        return array('rss_feed', 'rss_feed2', 'rss_order', 'rss_template','rss_template2', 'rss_template3');
    }
}
