<!doctype html>
<html lang="en" id="top">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="shortcut icon" type="image/png" href="images/favicon.png">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
  <link rel="stylesheet" href="style/style.css">
  <script src="style/style.js"></script>

  <title>Sales Robot: ArchIntel Email Server</title>
</head>
<body>
<a href="#top" id="top-button" class="top js-scroll-trigger" title="Go to top"><img src="https://www.freeiconspng.com/uploads/house-top-icon-12.png" alt="top-image"></a>
<header class="masthead">
  <div class="navbarContainer">
    <nav class="navbar is-transparent">
      <div class="navbar-brand">
        <a class="navbar-item" href="http://salesrobot.com">
          <img src="images/salesrobotheader.png" alt="SalesRobot" id="resizeHeader">
        </a>
        <div id="navbar-burger" class="navbar-burger burger" data-target="navbarExampleTransparentExample">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>

      <div class="navbar-menu" id="navbar-menu">
        <div class="navbar-end">
          <a class="navbar-item" href="http://salesrobot.com/">
            Home
          </a>
          <a class="navbar-item js-scroll-trigger" href="#about">
            About
          </a>
          <div class="navbar-item has-dropdown is-hoverable">
            <a class="navbar-link js-scroll-trigger" href="#products">
              Products
            </a>
            <div class="navbar-dropdown is-boxed">
              <a class="navbar-item js-scroll-trigger" href="#products">
                Overview
              </a>
              <hr class="navbar-divider">
              <a class="navbar-item js-scroll-trigger" href="#products">
                Potomac Officers Club
              </a>
            </div>
          </div>
          <div class="navbar-item">
            <a class="button is-info" href="http://salesrobot.com/admin">Mail Login</a>
          </div>
        </div>
      </div>
    </nav>
    <div class="columns">
      <div class="column">
        <p class="header-text">SalesRobot is an email server created solely for ArchIntel Corporation</p>
        <p class="header-content">What is ArchIntel? ArchIntel provides business leaders with customized daily executive briefs about their competitors, customers and partners every weekday morning.
          <a href="https://archintel.com">Read more...</a></p>
        <a class="button is-info" href="#products">Check our other products</a>
      </div>
      <div class="column is-center has-text-centered-mobile has-text-centered-desktop">
        <img src="./images/salesrobotwhole.png" alt="Hands">
      </div>
    </div>
    <!--<div class="container">
      <p class="header-text">SalesRobot is an email server created</p>
      <p class="header-text">solely for ArchIntel Corporation</p>
      <p class="header-content">What is ArchIntel? ArchIntel provides business leaders with customized daily executive briefs about their competitors, customers and partners every weekday morning.
        <a href="https://archintel.com">Read more...</a></p>
      <a class="button is-info" href="http://salesrobot.com/admin">Mail Login</a>
    </div>-->
  </div>
</header>

<div class="about" id="about">
  <div class="columns is-vcentered">
    <div class="column is-center has-text-centered-mobile has-text-centered-desktop">
      <img src="./images/jim.png" alt="Hands">
    </div>
    <div class="column">
      <h1>ABOUT</h1>
      <p class="about-content">Jim Garrettson, President and CEO of Executive Mosaic created ArchIntel™ in 2016 as a new offering by Executive Mosaic. For the past 12 years, Executive Mosaic has focused almost exclusively on federal government contracting companies and the sector itself. With the introduction of the ArchIntel™ Intelligence product offering, Executive Mosaic now has the ability to serve both the GovCon industry and commercial accounts. The decision to open a Commercial Division for the ArchIntel™ Daily Customized Intelligence Briefing was prompted by the early success with existing customers in the GovCon industry and rapid adoption by commercial enterprise accounts.</p>
    </div>
  </div>
</div>

<div class="products" id="products">
  <div class="columns is-vcentered">
    <div class="column">
      <h2 class="product-header">PRODUCTS</h2>
      <h1>Potomac Officers Club</h1>
      <p class="poc-content">The Potomac Officers Club (POC) is a membership organization dedicated to connecting and empowering executives within the Government Contracting community. POC provides the opportunity to learn from peer business executives, hear from government thought leaders, and create an outstanding forum to develop key business and partnerships.
        <a href="https://www.potomacofficersclub.com/">Read more...</a></p>
    </div>
    <div class="column is-center has-text-centered-mobile has-text-centered-desktop">
      <img src="./images/poc.png" alt="Hands">
    </div>
  </div>
</div>
<footer>
  Copyright © 2018 by Executive Mosaic. All rights reserved
</footer>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="style/creative.js"></script>
</body>
</html>