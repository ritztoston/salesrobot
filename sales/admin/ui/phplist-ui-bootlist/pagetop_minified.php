<?php include_once( dirname(__FILE__) . '/functions.php');

/*
  We request you retain the full headers below including the links.
  This not only gives respect to the large amount of time given freely
  by the developers, but also helps build interest, traffic and use of
  phpList, which is beneficial to it's future development.

  Michiel Dethmers, phpList Ltd 2003 - 2017
*/
?>
    <!DOCTYPE html>
<html lang="<?php echo $_SESSION['adminlanguage']['iso']?>" dir="<?php echo $_SESSION['adminlanguage']['dir']?>" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8" />
    <?php if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) header('X-UA-Compatible: IE=edge'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="License" content="GNU Affero General Public License, http://www.gnu.org/licenses/agpl.html" />
    <meta name="Author" content="Michiel Dethmers - http://www.phplist.com" />
    <meta name="Copyright" content="Michiel Dethmers, phpList Ltd - http://phplist.com" />
    <meta name="Powered-By" content="phplist version <?php echo VERSION?>" />
    <meta property="og:title" content="phpList" />
    <meta property="og:url" content="http://phplist.com" />
    <meta name="theme-color" content="#2C2C2C"/>
    <link rel="shortcut icon" type="image/png" href="./images/favicon.png"/>
    <link rel="icon" type="image/png" href="./images/favicon.png"/>
    <link rel="apple-touch-icon" href="./images/phplist-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" href="./images/phplist-touch-icon.png" />

    <!-- initial styles and JS from basic application -->
    <link rel="stylesheet" type="text/css" data-them="" href="myStyles/bootstrap.min.css">
    <script src="myStyles/jquery-2.1.1.min.js"></script>
    <link rel="stylesheet" data-them="" href="myStyles/styles-green.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" data-them="" href="myStyles/mass_email.css">
    <link rel="stylesheet" data-them="" href="myStyles/spectrum.css">
    <link rel="stylesheet" data-them="" href="myStyles/bootstrap-switch.min.css">
    <script src="https://cdn.tinymce.com/4.8/tinymce.min.js"></script>
    <script src="myStyles/tinymce_script.js"></script>

    <!-- the CSS is already mostly minified -->
    <link rel="stylesheet" href="css/app.css" />
    <link rel="stylesheet" href="ui/phplist-ui-bootlist/css/style.css?v=<?php echo filemtime(dirname(__FILE__).'/css/style.css'); ?>" />

<?php
if (isset($GLOBALS['config']['head'])) {
    foreach ($GLOBALS['config']['head'] as $sHtml) {
        print $sHtml;
        print "\n";
        print "\n";
    }
}
