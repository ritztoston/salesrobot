<!DOCTYPE html>
<html lang="en">

<head>
      <link href="https://salesrobot.com/sales/images/favicon.png" rel="shortcut icon" type="image/png">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
      <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
            integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
      <title>Executive Mosaic’s Weekly GovCon Round-Up: Past Issues</title>
      <script>
            function sortDescending(a, b) {
                  var date1 = $(a).find(".date").text();
                  date1 = date1.split('/');
                  date1 = new Date(date1[2], date1[0] - 1, date1[1]);
                  var date2 = $(b).find(".date").text();
                  date2 = date2.split('/');
                  date2 = new Date(date2[2], date2[0] - 1, date2[1]);

                  return date1 < date2 ? 1 : -1;
            }

            $(document).ready(function () {
                  $('ul > li').sort(sortDescending).appendTo('ul');
            });
      </script>
</head>

<body style="background-image: url(https://www.govconwire.com/wp-content/themes/sahifa/images/patterns/body-bg26.png); background-position: top center;font-family: 'Roboto', sans-serif;">
      <div style="text-align:center;">

            <div style="margin-top:70px;max-width:600px;width:100%;background-color: #ffffff;border-radius:5px;padding:10px 10px;display: inline-block;text-align: left;font-size:16px">
                  <div style="display:inline-block;padding-left:10px;font-size:20px;">Executive Mosaic’s Weekly GovCon
                        Round-Up Past Issues</div>

                  <?php
                        // $directory = "../../sales/admin/archives/";
                        $directory = "https://executivebiz.secure.force.com/archintelmedia/apex/mediatrackervfp";
                        
                        $content =  file_get_contents("https://executivebiz.secure.force.com/archintelmedia/apex/mediatrackervfp");
                        echo $content;
                        echo 'hello';
                  ?>
            </div>
      </div>
</body>

</html>