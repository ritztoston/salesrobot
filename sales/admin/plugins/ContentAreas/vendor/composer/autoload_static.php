<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf33b3a17c07f7e612c9ad471b87aeaee
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Symfony\\Component\\CssSelector\\' => 30,
        ),
        'P' => 
        array (
            'Pelago\\' => 7,
        ),
        'C' => 
        array (
            'Crossjoin\\PreMailer\\' => 20,
            'Crossjoin\\Css\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Symfony\\Component\\CssSelector\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/css-selector',
        ),
        'Pelago\\' => 
        array (
            0 => __DIR__ . '/..' . '/pelago/emogrifier/Classes',
        ),
        'Crossjoin\\PreMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/crossjoin/pre-mailer/src/Crossjoin/PreMailer',
        ),
        'Crossjoin\\Css\\' => 
        array (
            0 => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css',
        ),
    );

    public static $classMap = array (
        'Crossjoin\\Css\\Format\\Rule\\AtCharset\\CharsetRule' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtCharset/CharsetRule.php',
        'Crossjoin\\Css\\Format\\Rule\\AtDocument\\DocumentRule' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtDocument/DocumentRule.php',
        'Crossjoin\\Css\\Format\\Rule\\AtFontFace\\FontFaceDeclaration' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtFontFace/FontFaceDeclaration.php',
        'Crossjoin\\Css\\Format\\Rule\\AtFontFace\\FontFaceRule' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtFontFace/FontFaceRule.php',
        'Crossjoin\\Css\\Format\\Rule\\AtImport\\ImportRule' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtImport/ImportRule.php',
        'Crossjoin\\Css\\Format\\Rule\\AtKeyframes\\KeyframesDeclaration' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtKeyframes/KeyframesDeclaration.php',
        'Crossjoin\\Css\\Format\\Rule\\AtKeyframes\\KeyframesKeyframe' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtKeyframes/KeyframesKeyframe.php',
        'Crossjoin\\Css\\Format\\Rule\\AtKeyframes\\KeyframesRule' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtKeyframes/KeyframesRule.php',
        'Crossjoin\\Css\\Format\\Rule\\AtKeyframes\\KeyframesRuleSet' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtKeyframes/KeyframesRuleSet.php',
        'Crossjoin\\Css\\Format\\Rule\\AtMedia\\MediaCondition' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtMedia/MediaCondition.php',
        'Crossjoin\\Css\\Format\\Rule\\AtMedia\\MediaQuery' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtMedia/MediaQuery.php',
        'Crossjoin\\Css\\Format\\Rule\\AtMedia\\MediaRule' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtMedia/MediaRule.php',
        'Crossjoin\\Css\\Format\\Rule\\AtNamespace\\NamespaceRule' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtNamespace/NamespaceRule.php',
        'Crossjoin\\Css\\Format\\Rule\\AtPage\\PageDeclaration' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtPage/PageDeclaration.php',
        'Crossjoin\\Css\\Format\\Rule\\AtPage\\PageRule' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtPage/PageRule.php',
        'Crossjoin\\Css\\Format\\Rule\\AtPage\\PageSelector' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtPage/PageSelector.php',
        'Crossjoin\\Css\\Format\\Rule\\AtRuleAbstract' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtRuleAbstract.php',
        'Crossjoin\\Css\\Format\\Rule\\AtRuleConditionalAbstract' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtRuleConditionalAbstract.php',
        'Crossjoin\\Css\\Format\\Rule\\AtSupports\\SupportsCondition' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtSupports/SupportsCondition.php',
        'Crossjoin\\Css\\Format\\Rule\\AtSupports\\SupportsRule' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/AtSupports/SupportsRule.php',
        'Crossjoin\\Css\\Format\\Rule\\ConditionAbstract' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/ConditionAbstract.php',
        'Crossjoin\\Css\\Format\\Rule\\DeclarationAbstract' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/DeclarationAbstract.php',
        'Crossjoin\\Css\\Format\\Rule\\HasRulesInterface' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/HasRulesInterface.php',
        'Crossjoin\\Css\\Format\\Rule\\RuleAbstract' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/RuleAbstract.php',
        'Crossjoin\\Css\\Format\\Rule\\RuleGroupableInterface' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/RuleGroupableInterface.php',
        'Crossjoin\\Css\\Format\\Rule\\SelectorAbstract' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/SelectorAbstract.php',
        'Crossjoin\\Css\\Format\\Rule\\Style\\StyleDeclaration' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/Style/StyleDeclaration.php',
        'Crossjoin\\Css\\Format\\Rule\\Style\\StyleRuleSet' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/Style/StyleRuleSet.php',
        'Crossjoin\\Css\\Format\\Rule\\Style\\StyleSelector' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/Style/StyleSelector.php',
        'Crossjoin\\Css\\Format\\Rule\\TraitComments' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/TraitComments.php',
        'Crossjoin\\Css\\Format\\Rule\\TraitConditions' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/TraitConditions.php',
        'Crossjoin\\Css\\Format\\Rule\\TraitDeclarations' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/TraitDeclarations.php',
        'Crossjoin\\Css\\Format\\Rule\\TraitIsValid' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/TraitIsValid.php',
        'Crossjoin\\Css\\Format\\Rule\\TraitRules' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/TraitRules.php',
        'Crossjoin\\Css\\Format\\Rule\\TraitVendorPrefix' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/Rule/TraitVendorPrefix.php',
        'Crossjoin\\Css\\Format\\StyleSheet\\StyleSheet' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/StyleSheet/StyleSheet.php',
        'Crossjoin\\Css\\Format\\StyleSheet\\TraitStyleSheet' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Format/StyleSheet/TraitStyleSheet.php',
        'Crossjoin\\Css\\Helper\\Condition' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Helper/Condition.php',
        'Crossjoin\\Css\\Helper\\Optimizer' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Helper/Optimizer.php',
        'Crossjoin\\Css\\Helper\\Placeholder' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Helper/Placeholder.php',
        'Crossjoin\\Css\\Helper\\Url' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Helper/Url.php',
        'Crossjoin\\Css\\Reader\\CssFile' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Reader/CssFile.php',
        'Crossjoin\\Css\\Reader\\CssString' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Reader/CssString.php',
        'Crossjoin\\Css\\Reader\\HtmlFile' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Reader/HtmlFile.php',
        'Crossjoin\\Css\\Reader\\HtmlString' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Reader/HtmlString.php',
        'Crossjoin\\Css\\Reader\\ReaderAbstract' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Reader/ReaderAbstract.php',
        'Crossjoin\\Css\\Reader\\ReaderFileAbstract' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Reader/ReaderFileAbstract.php',
        'Crossjoin\\Css\\Writer\\Compact' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Writer/Compact.php',
        'Crossjoin\\Css\\Writer\\Pretty' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Writer/Pretty.php',
        'Crossjoin\\Css\\Writer\\WriterAbstract' => __DIR__ . '/..' . '/crossjoin/css/src/Crossjoin/Css/Writer/WriterAbstract.php',
        'Crossjoin\\PreMailer\\HtmlFile' => __DIR__ . '/..' . '/crossjoin/pre-mailer/src/Crossjoin/PreMailer/HtmlFile.php',
        'Crossjoin\\PreMailer\\HtmlString' => __DIR__ . '/..' . '/crossjoin/pre-mailer/src/Crossjoin/PreMailer/HtmlString.php',
        'Crossjoin\\PreMailer\\PreMailerAbstract' => __DIR__ . '/..' . '/crossjoin/pre-mailer/src/Crossjoin/PreMailer/PreMailerAbstract.php',
        'Pelago\\Emogrifier' => __DIR__ . '/..' . '/pelago/emogrifier/Classes/Emogrifier.php',
        'Symfony\\Component\\CssSelector\\CssSelector' => __DIR__ . '/..' . '/symfony/css-selector/CssSelector.php',
        'Symfony\\Component\\CssSelector\\CssSelectorConverter' => __DIR__ . '/..' . '/symfony/css-selector/CssSelectorConverter.php',
        'Symfony\\Component\\CssSelector\\Exception\\ExceptionInterface' => __DIR__ . '/..' . '/symfony/css-selector/Exception/ExceptionInterface.php',
        'Symfony\\Component\\CssSelector\\Exception\\ExpressionErrorException' => __DIR__ . '/..' . '/symfony/css-selector/Exception/ExpressionErrorException.php',
        'Symfony\\Component\\CssSelector\\Exception\\InternalErrorException' => __DIR__ . '/..' . '/symfony/css-selector/Exception/InternalErrorException.php',
        'Symfony\\Component\\CssSelector\\Exception\\ParseException' => __DIR__ . '/..' . '/symfony/css-selector/Exception/ParseException.php',
        'Symfony\\Component\\CssSelector\\Exception\\SyntaxErrorException' => __DIR__ . '/..' . '/symfony/css-selector/Exception/SyntaxErrorException.php',
        'Symfony\\Component\\CssSelector\\Node\\AbstractNode' => __DIR__ . '/..' . '/symfony/css-selector/Node/AbstractNode.php',
        'Symfony\\Component\\CssSelector\\Node\\AttributeNode' => __DIR__ . '/..' . '/symfony/css-selector/Node/AttributeNode.php',
        'Symfony\\Component\\CssSelector\\Node\\ClassNode' => __DIR__ . '/..' . '/symfony/css-selector/Node/ClassNode.php',
        'Symfony\\Component\\CssSelector\\Node\\CombinedSelectorNode' => __DIR__ . '/..' . '/symfony/css-selector/Node/CombinedSelectorNode.php',
        'Symfony\\Component\\CssSelector\\Node\\ElementNode' => __DIR__ . '/..' . '/symfony/css-selector/Node/ElementNode.php',
        'Symfony\\Component\\CssSelector\\Node\\FunctionNode' => __DIR__ . '/..' . '/symfony/css-selector/Node/FunctionNode.php',
        'Symfony\\Component\\CssSelector\\Node\\HashNode' => __DIR__ . '/..' . '/symfony/css-selector/Node/HashNode.php',
        'Symfony\\Component\\CssSelector\\Node\\NegationNode' => __DIR__ . '/..' . '/symfony/css-selector/Node/NegationNode.php',
        'Symfony\\Component\\CssSelector\\Node\\NodeInterface' => __DIR__ . '/..' . '/symfony/css-selector/Node/NodeInterface.php',
        'Symfony\\Component\\CssSelector\\Node\\PseudoNode' => __DIR__ . '/..' . '/symfony/css-selector/Node/PseudoNode.php',
        'Symfony\\Component\\CssSelector\\Node\\SelectorNode' => __DIR__ . '/..' . '/symfony/css-selector/Node/SelectorNode.php',
        'Symfony\\Component\\CssSelector\\Node\\Specificity' => __DIR__ . '/..' . '/symfony/css-selector/Node/Specificity.php',
        'Symfony\\Component\\CssSelector\\Parser\\Handler\\CommentHandler' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Handler/CommentHandler.php',
        'Symfony\\Component\\CssSelector\\Parser\\Handler\\HandlerInterface' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Handler/HandlerInterface.php',
        'Symfony\\Component\\CssSelector\\Parser\\Handler\\HashHandler' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Handler/HashHandler.php',
        'Symfony\\Component\\CssSelector\\Parser\\Handler\\IdentifierHandler' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Handler/IdentifierHandler.php',
        'Symfony\\Component\\CssSelector\\Parser\\Handler\\NumberHandler' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Handler/NumberHandler.php',
        'Symfony\\Component\\CssSelector\\Parser\\Handler\\StringHandler' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Handler/StringHandler.php',
        'Symfony\\Component\\CssSelector\\Parser\\Handler\\WhitespaceHandler' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Handler/WhitespaceHandler.php',
        'Symfony\\Component\\CssSelector\\Parser\\Parser' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Parser.php',
        'Symfony\\Component\\CssSelector\\Parser\\ParserInterface' => __DIR__ . '/..' . '/symfony/css-selector/Parser/ParserInterface.php',
        'Symfony\\Component\\CssSelector\\Parser\\Reader' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Reader.php',
        'Symfony\\Component\\CssSelector\\Parser\\Shortcut\\ClassParser' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Shortcut/ClassParser.php',
        'Symfony\\Component\\CssSelector\\Parser\\Shortcut\\ElementParser' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Shortcut/ElementParser.php',
        'Symfony\\Component\\CssSelector\\Parser\\Shortcut\\EmptyStringParser' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Shortcut/EmptyStringParser.php',
        'Symfony\\Component\\CssSelector\\Parser\\Shortcut\\HashParser' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Shortcut/HashParser.php',
        'Symfony\\Component\\CssSelector\\Parser\\Token' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Token.php',
        'Symfony\\Component\\CssSelector\\Parser\\TokenStream' => __DIR__ . '/..' . '/symfony/css-selector/Parser/TokenStream.php',
        'Symfony\\Component\\CssSelector\\Parser\\Tokenizer\\Tokenizer' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Tokenizer/Tokenizer.php',
        'Symfony\\Component\\CssSelector\\Parser\\Tokenizer\\TokenizerEscaping' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Tokenizer/TokenizerEscaping.php',
        'Symfony\\Component\\CssSelector\\Parser\\Tokenizer\\TokenizerPatterns' => __DIR__ . '/..' . '/symfony/css-selector/Parser/Tokenizer/TokenizerPatterns.php',
        'Symfony\\Component\\CssSelector\\XPath\\Extension\\AbstractExtension' => __DIR__ . '/..' . '/symfony/css-selector/XPath/Extension/AbstractExtension.php',
        'Symfony\\Component\\CssSelector\\XPath\\Extension\\AttributeMatchingExtension' => __DIR__ . '/..' . '/symfony/css-selector/XPath/Extension/AttributeMatchingExtension.php',
        'Symfony\\Component\\CssSelector\\XPath\\Extension\\CombinationExtension' => __DIR__ . '/..' . '/symfony/css-selector/XPath/Extension/CombinationExtension.php',
        'Symfony\\Component\\CssSelector\\XPath\\Extension\\ExtensionInterface' => __DIR__ . '/..' . '/symfony/css-selector/XPath/Extension/ExtensionInterface.php',
        'Symfony\\Component\\CssSelector\\XPath\\Extension\\FunctionExtension' => __DIR__ . '/..' . '/symfony/css-selector/XPath/Extension/FunctionExtension.php',
        'Symfony\\Component\\CssSelector\\XPath\\Extension\\HtmlExtension' => __DIR__ . '/..' . '/symfony/css-selector/XPath/Extension/HtmlExtension.php',
        'Symfony\\Component\\CssSelector\\XPath\\Extension\\NodeExtension' => __DIR__ . '/..' . '/symfony/css-selector/XPath/Extension/NodeExtension.php',
        'Symfony\\Component\\CssSelector\\XPath\\Extension\\PseudoClassExtension' => __DIR__ . '/..' . '/symfony/css-selector/XPath/Extension/PseudoClassExtension.php',
        'Symfony\\Component\\CssSelector\\XPath\\Translator' => __DIR__ . '/..' . '/symfony/css-selector/XPath/Translator.php',
        'Symfony\\Component\\CssSelector\\XPath\\TranslatorInterface' => __DIR__ . '/..' . '/symfony/css-selector/XPath/TranslatorInterface.php',
        'Symfony\\Component\\CssSelector\\XPath\\XPathExpr' => __DIR__ . '/..' . '/symfony/css-selector/XPath/XPathExpr.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf33b3a17c07f7e612c9ad471b87aeaee::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf33b3a17c07f7e612c9ad471b87aeaee::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitf33b3a17c07f7e612c9ad471b87aeaee::$classMap;

        }, null, ClassLoader::class);
    }
}
