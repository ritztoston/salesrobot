<?php

require_once dirname(__FILE__).'/accesscheck.php';

$actionresult = '';

if (!empty($_FILES['file_template']) && is_uploaded_file($_FILES['file_template']['tmp_name'])) {
    $content = file_get_contents($_FILES['file_template']['tmp_name']);
} elseif (isset($_POST['template'])) {
    $content = $_POST['template'];
} else {
    $content = '';
}
$sendtestresult = '';
$testtarget = getConfig('admin_address');
$systemTemplateID = getConfig('systemmessagetemplate');

if (isset($_REQUEST['id'])) {
    $id = sprintf('%d', $_REQUEST['id']);
} else {
    $id = 0;
}

function getTemplateImages($content)
{
    $image_types = array(
        'gif'  => 'image/gif',
        'jpg'  => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpe'  => 'image/jpeg',
        'bmp'  => 'image/bmp',
        'png'  => 'image/png',
        'tif'  => 'image/tiff',
        'tiff' => 'image/tiff',
        'swf'  => 'application/x-shockwave-flash',
    );
    $regexp = sprintf('/"([^"]+\.(%s))"/Ui', implode('|', array_keys($image_types)));
    preg_match_all($regexp, stripslashes($content), $images);

    return array_count_values($images[1]);
}

function getTemplateLinks($content)
{
    preg_match_all('/href="([^"]+)"/Ui', stripslashes($content), $links);

    return $links[1];
}

$msg = '';
$checkfullimages = !empty($_POST['checkfullimages']) ? 1 : 0;
$checkimagesexist = !empty($_POST['checkimagesexist']) ? 1 : 0;
$checkfulllinks = !empty($_POST['checkfulllinks']) ? 1 : 0;
$baseurl = '';

if (!empty($_POST['action']) && $_POST['action'] == 'addimages') {
    if (!$id) {
        $msg = $GLOBALS['I18N']->get('No such template');
    } else {
        $content_req = Sql_Fetch_Row_Query("select template from {$tables['template']} where id = $id");
        $images = getTemplateImages($content_req[0]);

        if (count($images)) {
            include 'class.image.inc';
            $image = new imageUpload();
            foreach ($images as $key => $val) {
                // printf('Image name: <b>%s</b> (%d times used)<br />',$key,$val);
                $image->uploadImage($key, $id);
            }
            $msg = $GLOBALS['I18N']->get('Images stored');
        } else {
            $msg = $GLOBALS['I18N']->get('No images found');
        }
    }
    $_SESSION['action_result'] = $msg.'<br/>'.s('Template saved and ready for use in campaigns');
    Redirect('templates');

    return;
    //print '<p class="actionresult">'.$msg.'</p>';
    //$msg = '';
} elseif (!empty($_POST['save']) || !empty($_POST['sendtest'])) { //# let's save when sending a test
    $templateok = 1;
    $title = $_POST['title'];
//    if (!empty($title) && strpos($content, '[CONTENT]') !== false) {
    if (!empty($title)) {
        $images = getTemplateImages($content);

        //   var_dump($images);

        if (($checkfullimages || $checkimagesexist) && count($images)) {
            foreach ($images as $key => $val) {
                if (!preg_match('#^https?://#i', $key)) {
                    if ($checkfullimages) {
                        $actionresult .= $GLOBALS['I18N']->get('Image')." $key => ".$GLOBALS['I18N']->get('"not full URL')."<br/>\n";
                        $templateok = 0;
                    }
                } else {
                    if ($checkimagesexist) {
                        $imageFound = testUrl($key);
                        if ($imageFound != 200) {
                            $actionresult .= $GLOBALS['I18N']->get('Image')." $key => ".$GLOBALS['I18N']->get('does not exist')."<br/>\n";
                            $templateok = 0;
                        }
                    }
                }
            }
        }
        if ($checkfulllinks) {
            $links = getTemplateLinks($content);
            foreach ($links as $key => $val) {
                if (!preg_match('#^https?://#i', $val) && !preg_match('#^mailto:#i', $val)
                    && !(strtoupper($val) == '[PREFERENCESURL]' || strtoupper($val) == '[UNSUBSCRIBEURL]' || strtoupper($val) == '[BLACKLISTURL]' || strtoupper($val) == '[FORWARDURL]' || strtoupper($val) == '[CONFIRMATIONURL]')
                ) {
                    $actionresult .= $GLOBALS['I18N']->get('Not a full URL').": $val<br/>\n";
                    $templateok = 0;
                }
            }
        }
    } else {
        if (!$title) {
            $actionresult .= $GLOBALS['I18N']->get('No Title').'<br/>';
        } /*else {
            $actionresult .= $GLOBALS['I18N']->get('Template does not contain the [CONTENT] placeholder').'<br/>';
        }*/
        $templateok = 0;
    }
    if ($templateok) {
        if (!$id) {
            Sql_Query(sprintf('insert into %s (title) values("%s")', $tables['template'], sql_escape($title)));
            $id = Sql_Insert_id();
        }
        Sql_Query(sprintf('update %s set title = "%s",template = "%s" where id = %d',
            $tables['template'], sql_escape($title), sql_escape($content), $id));
        Sql_Query(sprintf('select * from %s where filename = "%s" and template = %d',
            $tables['templateimage'], 'powerphplist.png', $id));
        if (!Sql_Affected_Rows()) {
            Sql_Query(sprintf('insert into %s (template,mimetype,filename,data,width,height)
      values(%d,"%s","%s","%s",%d,%d)',
                $tables['templateimage'], $id, 'image/png', 'powerphplist.png',
                $newpoweredimage,
                70, 30));
        }
        $actionresult .= '<p class="information">'.s('Template saved').'</p>';

        //# ##17419 don't prompt for remote images that exist
        $missingImages = array();
        foreach ($images as $key => $val) {
            $key = trim($key);
            if (preg_match('~^https?://~i', $key)) {
                $imageFound = testUrl($key);
                if (!$imageFound) {
                    $missingImages[$key] = $val;
                }
            } else {
                $missingImages[$key] = $val;
            }
        }

        if (count($missingImages) && empty($_POST['sendtest'])) {
            include dirname(__FILE__).'/class.image.inc';
            $image = new imageUpload();
            echo '<h3>'.$GLOBALS['I18N']->get('Images').'</h3><p class="information">'.$GLOBALS['I18N']->get('Below is the list of images used in your template. If an image is currently unavailable, please upload it to the database.').'</p>';
            echo '<p class="information">'.$GLOBALS['I18N']->get('This includes all images, also fully referenced ones, so you may choose not to upload some. If you upload images, they will be included in the campaigns that use this template.').'</p>';
            echo formStart('enctype="multipart/form-data" class="template1" ');
            echo '<input type="hidden" name="id" value="'.$id.'" />';
            ksort($images);
            reset($images);
            foreach ($images as $key => $val) {
                $key = trim($key);
                if (preg_match('~^https?://~i', $key)) {
                    $missingImage = true;
                    $imageFound = testUrl($key);
                    if ($imageFound != 200) {
                        printf($GLOBALS['I18N']->get('Image name:').' <b>%s</b> ('.$GLOBALS['I18N']->get('%d times used').')<br/>',
                            $key, $val);
                        echo $image->showInput($key, $val, $id);
                    }
                } else {
                    printf($GLOBALS['I18N']->get('Image name:').' <b>%s</b> ('.$GLOBALS['I18N']->get('%d times used').')<br/>',
                        $key, $val);
                    echo $image->showInput($key, $val, $id);
                }
            }

            echo '<input type="hidden" name="id" value="'.$id.'" /><input type="hidden" name="action" value="addimages" />
        <input class="submit" type="submit" name="addimages" value="' .$GLOBALS['I18N']->get('Save Images').'" /></form>';
            if (empty($_POST['sendtest'])) {
                return;
            }
            //    return;
        } else {
            $_SESSION['action_result'] = s('Template was successfully saved');
//      print '<p class="information">'.$GLOBALS['I18N']->get('Template does not contain local images')."</p>";
            if (empty($_POST['sendtest'])) {
                Redirect('templates');

                return;
            }
            //    return;
        }
    } else {
        $actionresult .= $GLOBALS['I18N']->get('Some errors were found, template NOT saved!');
        $data['title'] = $title;
        $data['template'] = $content;
    }
    if (!empty($_POST['sendtest'])) {
        //# check if it's the system message template or a normal one:

        $targetEmails = explode(',', $_POST['testtarget']);
        $testtarget = '';

        if ($id == $systemTemplateID) {
            $actionresult .= '<h3>'.$GLOBALS['I18N']->get('Sending test').'</h3>';
            foreach ($targetEmails as $email) {
                if (validateEmail($email)) {
                    $testtarget .= $email.', ';
                    $actionresult .= $GLOBALS['I18N']->get('Sending test "Request for confirmation" to').' '.$email.'  ';
                    if (sendMail($email, getConfig('subscribesubject'), getConfig('subscribemessage'))) {
                        $actionresult .= s('OK');
                    } else {
                        $actionresult .= s('FAILED');
                    }
                    $actionresult .= '<br/>';
                    $actionresult .= $GLOBALS['I18N']->get('Sending test "Welcome" to').' '.$email.'  ';
                    if (sendMail($email, getConfig('confirmationsubject'), getConfig('confirmationmessage'))) {
                        $actionresult .= s('OK');
                    } else {
                        $actionresult .= s('FAILED');
                    }
                    $actionresult .= '<br/>';
                    $actionresult .= $GLOBALS['I18N']->get('Sending test "Unsubscribe confirmation" to').' '.$email.'  ';
                    if (sendMail($email, getConfig('unsubscribesubject'), getConfig('unsubscribemessage'))) {
                        $actionresult .= s('OK');
                    } else {
                        $actionresult .= s('FAILED');
                    }
                } elseif (trim($email) != '') {
                    $actionresult .= '<p>'.$GLOBALS['I18N']->get('Error sending test messages to').' '.htmlspecialchars($email).'</p>';
                }
            }
        } else {
            //# Sending test emails of non system templates to be added.
            $actionresult .= '<p>'.s('Sending a test from templates only works for the system template.').' '.
                s('To test your template, go to campaigns and send a test campaign using the template.').
                '</p>';
        }
        if (empty($testtarget)) {
            $testtarget = getConfig('admin_address');
        }
        $testtarget = preg_replace('/, $/', '', $testtarget);
    }
}
if (!empty($actionresult)) {
    echo '<div class="actionresult">'.$actionresult.'</div>';
}

if ($id) {
    $req = Sql_Query("select * from {$tables['template']} where id = $id");
    $data = Sql_Fetch_Array($req);
    //# keep POSTED data, even if not saved
    if (!empty($_POST['template'])) {
        $data['template'] = $content;
    }
} else {
    $data = array();
    $data['title'] = '';
    $data['template'] = '';
}

?>
<?php echo '<p class="button pull-right">'.PageLink2('templates', $GLOBALS['I18N']->get('List of Templates')).'</p><div class="clearfix"></div>'; ?>





<p class="information"><?php echo $msg ?></p>


<?php echo formStart(' enctype="multipart/form-data" class="template2" ') ?>
<input type="hidden" name="id" id="post_id" value="<?php echo $id ?>"/>
<div class="panel"><div class="content">
    <table class="templateForm">
      <tr>
        <td>
          <div class="form-group">
            <label for="template_file">Template File</label>
            <input type="file" name="file_template" class="form-control-file" id="template_file"/>
          </div>
        </td>
        <td style="display:none;">
            <?php echo $GLOBALS['I18N']->get('Title of this template') ?>
        </td>
        <td style="display:none;"><input type="text" id="post_title" name="title" value="<?php echo stripslashes(htmlspecialchars($data['title'])) ?>"
                                         size="30"/></td>
      </tr>
      <!--      <tr>-->
      <!--        <td colspan="2">--><?php //echo s('Content of the template.') ?>
      <!--          <br/>--><?php //echo s('The content should at least have <b>[CONTENT]</b> somewhere.') ?>
      <!--          <br/>--><?php //echo s('You can upload a template file or paste the text in the box below'); ?><!--</td>-->
      <!--      </tr>-->
      <!--      <tr>-->
      <!--        <td>--><?php //echo s('Template file.') ?><!--</td>-->
      <!--        <td><input type="file" name="file_template"/></td>-->
      <!--      </tr>-->
      <tr>
        <td colspan="2" style="display:none">
          <!-- <textarea id="m-tinymce-main"></textarea> -->
            <?php
            echo $GLOBALS['plugins'][$GLOBALS['editorplugin']]->editor('template',
                    stripslashes($data['template'])).'</div>';
            ?>
        </td>
      </tr>

      <!--tr>
  <td>Make sure all images<br/>start with this URL (optional)</td>
  <td><input type="text" name="baseurl" size="40" value="<?php echo htmlspecialchars($baseurl) ?>" /></td>
</tr-->
      <!--      <tr>-->
      <!--        <td>--><?php //echo $GLOBALS['I18N']->get('Check that all links have a full URL') ?><!--</td>-->
      <!--        <td><input type="checkbox" name="checkfulllinks" --><?php //echo $checkfulllinks ? 'checked="checked"' : '' ?><!-- />-->
      <!--        </td>-->
      <!--      </tr>-->
      <!--      <tr>-->
      <!--        <td>--><?php //echo $GLOBALS['I18N']->get('Check that all images have a full URL') ?><!--</td>-->
      <!--        <td><input type="checkbox"-->
      <!--                   name="checkfullimages" --><?php //echo $checkfullimages ? 'checked="checked"' : '' ?><!-- /></td>-->
      <!--      </tr>-->
      <!---->
      <!--        --><?php //if ($GLOBALS['can_fetchUrl']) {
        //            ?>
      <!--          <tr>-->
      <!--            <td>--><?php //echo $GLOBALS['I18N']->get('Check that all external images exist') ?><!--</td>-->
      <!--            <td>-->
      <!--              <input type="checkbox"-->
      <!--                     name="checkimagesexist" --><?php //echo $checkimagesexist ? 'checked="checked"' : '' ?><!-- />-->
      <!--            </td>-->
      <!--          </tr>-->
      <!--            --><?php
        //
        //        } ?>
      <!--      <tr>-->
      <!--        <td colspan="2">-->
      <!--          <input class="submit" type="submit" name="save"-->
      <!--                 value="--><?php //echo $GLOBALS['I18N']->get('Save Changes') ?><!--"/>-->
      <!--        </td>-->
      <!--      </tr>-->
    </table>
    <!-- LOADING -->
    <div class="all_content">
      <div class="dima-main" style="float:none;">
        <section>
          <div class="col-md-12 ">
            <div class="col-md-2" style="padding: 30px 0">
              <h2 class="hidden"></h2>
              <h3 class="hidden"></h3>
              <h4 class="hidden"></h4>

              <h5 style="text-align: left;color: #828077 !important;">ELEMENTS</h5>
              <div>
                <table width="100%">
                  <tr>
                    <td colspan="3" align="center">
                      Default Blocks
                      <div class="clear"></div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" align="center">
                      <i class="btn btn-primary" onclick="change_background(this)" data-target="#bgc-modal" data-toggle="modal">Change Background Color</i>
                      <div class="clear"></div>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding: 0 8px;border-top: 0;">
                      <div class="div-element draggable-text" id="drag-text"
                           style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                        <img class="text-element-img" src="icons/text.png" alt="text icon element">
                      </div>
                    </td>
                    <td style="padding: 0 8px;border-top: 0;">
                      <div class="div-element draggable-double" id="drag-double"
                           style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                        <img class="text-element-img" src="icons/double.png" alt="double icon element">
                      </div>
                      <br>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding: 0 8px;border-top: 0;">
                      <div class="div-element draggable-left" id="drag-left"
                           style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                        <img class="left-element-img" src="icons/left.png" alt="left icon element">
                      </div>
                    </td>
                    <td style="padding: 0 8px;border-top: 0;">
                      <div class="div-element draggable-right" id="drag-right"
                           style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                        <img class="right-element-img" src="icons/right.png" alt="right icon element">
                      </div>
                      <br>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding: 0 8px;border-top: 0;">
                      <div class="div-element draggable-twoimg" id="drag-twoimg"
                           style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                        <img class="left-element-img" src="icons/twoimg.png" alt="twoimg icon element">
                      </div>
                    </td>
                    <!-- <td style="padding: 0 8px;border-top: 0;">
                      <div class="div-element draggable-right" id="drag-right"
                           style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                        <img class="right-element-img" src="icons/right.png" alt="right icon element">
                      </div>
                      <br>
                    </td> -->
                  </tr>
                </table>
              </div>
              <div class="clear"></div>
              <div>
                <table width="100%">
                  <tr>
                    <td colspan="3" align="center">
                      Special Blocks
                      <div class="clear"></div>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding: 0 8px;border-top: 0;">
                      <div class="div-element draggable-space" id="drag-space"
                           style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                        <img class="divider-element-img" src="icons/space.png" alt="space icon element">
                      </div>
                    </td>
                    <td style="padding: 0 8px;border-top: 0;">
                      <div class="div-element draggable-divider" id="drag-divider"
                           style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                        <img class="divider-element-img" src="icons/divider.png" alt="divider icon element">
                      </div>
                      <br>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding: 0 8px;border-top: 0;">
                      <div class="div-element draggable-left-fixed" id="drag-left-fixed"
                           style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                        <img class="left-element-img" src="icons/left-fixed.png" alt="left icon element">
                      </div>
                    </td>
                    <td style="padding: 0 8px;border-top: 0;display:none;">
                      <div class="div-element draggable-right-fiexed" id="drag-right-fixed"
                           style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                        <img class="right-element-img" src="icons/right-fixed.png" alt="right icon element">
                      </div>
                      <br>
                    </td>
                  </tr>
                </table>
              </div>
              <br>
              <br>
            </div>
            <div class="col-md-9" id="tag-id" style="margin-left:50px;padding-left:0;">
              <div style="margin-top: 2em;border: 1px solid #8A9798;box-shadow: 0px 1px 3px 0px #888888;background: #8A9798;color: white;border-radius: 8px 8px 0px 0px;" id="tag-id">
                <nav role="navigation" class="clearfix">
                  <div style="margin: 0; padding: 0; float: right;">
                    <input class="submit" type="submit" name="save"
                           value="<?php echo $GLOBALS['I18N']->get('Save Changes') ?>"/>
                  </div>
                  <div style="margin: 0;padding: 0;float: right;">
                      <?php
                      echo '<span class="button">'.MyPageLinkDialogOnly('viewtemplate&amp;id='.$_GET['id'],
                              $GLOBALS['img_view'])
                      ?>
                  </div>
                  <!-- menu content -->
                  <ul class="dima-menu sf-menu" style="padding: 0;">
                    <li class="sub-icon" style="font-size: 18px;float: left;padding: 10px 20px 5px 10px;">
                      <!--                  kim-->
                      <div style="font-size:16px;float:left;padding-right:10px;color:#000000">Template Name:</div>
                        <?php
                        if (!empty($data['title'])) {
                            echo '<p style="font-size:16px; padding: 0;margin: 0;float: left;" class="my-title">'.stripslashes(htmlspecialchars($data['title'])).'</p>';
                        } else {
                            echo '<p style="font-size:16px; padding: 0;margin: 0;float: left;" class="my-title">No Title</p>';
                        }?>
                      <a id="edit-title" onclick="title_dialog(this)" data-toggle="modal" style="color: black;margin-left: 5px;"><i class="fa fa-pencil">&nbsp;</i></a>
                      <div class="modal fade title-modal" role="dialog">
                        <div class="modal-dialog modal-sm">

                          <!-- kim Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title" style="color: #000000;">Template Name</h4>
                            </div>
                            <div class="modal-body">
                              <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                              <div class="clear"></div>
                              <div class="">
                                <div class="accordion">
                                  <div class="accordion-section">
                                    <div class="accordion-section-title">
                                      <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                        <tbody>
                                        <tr>
                                          <td style="color: #000000;width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                            Text
                                          </td>
                                          <td style="width:70%;border: none !important;">
                                            <div>
                                              <input type="text" name="btn_text" class="btn_text-title" style="background: white !important"/>
                                            </div>
                                          </td>
                                        </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <!-- !menu content -->
                </nav>
              </div>
              <!--[if !mso]><!-- -->
              <style type="text/css">#outlook a{padding:0}.ReadMsgBody{width:100%}.ExternalClass{width:100%}.ExternalClass *{line-height:100%}body{margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt}img{border:0;height:auto;line-height:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}p{display:block;margin:13px 0}.poc-combo-block a{text-decoration:underline;color:blue;cursor:pointer;}
              </style>
              <!--[if !mso]><!-->
              <style type="text/css">@media only screen and (max-width:480px){@-ms-viewport {width:320px}@viewport {width:320px}}
              </style>
              <!--<![endif]--><!--[if mso]>
              <xml>
                <o:OfficeDocumentSettings>
                  <o:AllowPNG></o:AllowPNG>
                  <o:PixelsPerInch>96</o:PixelsPerInch>
                </o:OfficeDocumentSettings>
              </xml>
              <![endif]--><!--[if lte mso 11]>
              <style type="text/css">
                .outlook-group-fix { width:100% !important; }
              </style>
              <![endif]--><!--[if !mso]><!-->
              <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css" />
              <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css" /><!--<![endif]-->
              <style type="text/css">@media only screen and (min-width:480px){.mj-column-per-100{width:100%!important;max-width:100%}.mj-column-per-20{width:20%!important;max-width:20%}.mj-column-per-60{width:60%!important;max-width:60%}.mj-column-per-80{width:80%!important;max-width:80%}.mj-column-per-50{width:50%!important;max-width:50%}}
              </style>
              <style type="text/css">@media only screen and (max-width:480px){table.full-width-mobile{width:100%!important}td.full-width-mobile{width:auto!important}}
              </style>
              <div id="w-template" class="col-md-12" style="border: 1px solid #D0CDBC;box-shadow: 0px 1px 3px 0px #888888;height: 600px;overflow: auto;padding:0;">

                  <?php
                  $new_content = stripslashes($data['template']);
                  $new_content = str_replace('<!DOCTYPE html>','', $new_content);
                  $new_content = str_replace('<html>','', $new_content);
                  $new_content = str_replace('<head>','', $new_content);
                  $new_content = str_replace('<link href="https://salesrobot.com/sales/images/favicon.png" rel="shortcut icon" type="image/png" />','', $new_content);
                  $new_content = str_replace('<title>Executive Mosaic Weekly Roundup</title>','', $new_content);
                  $new_content = str_replace('<!--[if !mso]><!-- -->','', $new_content);
                  $new_content = str_replace('<meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]--><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1">','', $new_content);
                  $new_content = str_replace('<style type="text/css">#outlook a{padding:0}.ReadMsgBody{width:100%}.ExternalClass{width:100%}.ExternalClass *{line-height:100%}body{margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt}img{border:0;height:auto;line-height:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}p{display:block;margin:13px 0}','', $new_content);
                  $new_content = str_replace('</style>','', $new_content);
                  $new_content = str_replace('<!--[if !mso]><!-->','', $new_content);
                  $new_content = str_replace('<style type="text/css">@media only screen and (max-width:480px){@-ms-viewport {width:320px}@viewport {width:320px}}','', $new_content);
                  $new_content = str_replace('</style>','', $new_content);
                  $new_content = str_replace('<!--<![endif]--><!--[if mso]>','', $new_content);
                  $new_content = str_replace('<xml>','', $new_content);
                  $new_content = str_replace('<o:OfficeDocumentSettings>','', $new_content);
                  $new_content = str_replace('<o:AllowPNG></o:AllowPNG>','', $new_content);
                  $new_content = str_replace('<o:PixelsPerInch>96</o:PixelsPerInch>','', $new_content);
                  $new_content = str_replace('</o:OfficeDocumentSettings>','', $new_content);
                  $new_content = str_replace('</xml>','', $new_content);
                  $new_content = str_replace('<![endif]--><!--[if lte mso 11]>','', $new_content);
                  $new_content = str_replace('<style type="text/css">','', $new_content);
                  $new_content = str_replace('.outlook-group-fix { width:100% !important; }','', $new_content);
                  $new_content = str_replace('</style>','', $new_content);
                  $new_content = str_replace('<![endif]--><!--[if !mso]><!-->','', $new_content);
                  $new_content = str_replace('<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css" />','', $new_content);
                  $new_content = str_replace('<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css" /><!--<![endif]-->','', $new_content);
                  $new_content = str_replace('<style type="text/css">@media only screen and (min-width:480px){.mj-column-per-100{width:100%!important;max-width:100%}.mj-column-per-20{width:20%!important;max-width:20%}.mj-column-per-60{width:60%!important;max-width:60%}.mj-column-per-80{width:80%!important;max-width:80%}.mj-column-per-50{width:50%!important;max-width:50%}}','', $new_content);
                  $new_content = str_replace('</style>','', $new_content);
                  $new_content = str_replace('<style type="text/css">@media only screen and (max-width:480px){table.full-width-mobile{width:100%!important}td.full-width-mobile{width:auto!important}}','', $new_content);
                  $new_content = str_replace('</style>','', $new_content);
                  $new_content = str_replace('<!--[endif]---->','', $new_content);
                  $new_content = str_replace('<!--<![endif]-->','', $new_content);
                  $new_content = str_replace('@media only screen and (min-width:480px){.mj-column-per-100{width:100%!important;max-width:100%}.mj-column-per-20{width:20%!important;max-width:20%}.mj-column-per-60{width:60%!important;max-width:60%}.mj-column-per-80{width:80%!important;max-width:80%}.mj-column-per-50{width:50%!important;max-width:50%}}','', $new_content);
                  $new_content = str_replace('@media only screen and (max-width:480px){table.full-width-mobile{width:100%!important}td.full-width-mobile{width:auto!important}}','', $new_content);
                  $new_content = str_replace('</head>','', $new_content);
                  $new_content = str_replace('<body>','', $new_content);
                  $new_content = str_replace('</body>','', $new_content);
                  $new_content = str_replace('</html>','', $new_content);

                  $new_content = str_replace('<a onclick="left_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-left" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;"></a><i class="fa fa-pencil" style="font-size: 20px;">&nbsp;</i><a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a>','<a onclick="left_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-left" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;">&nbsp;</i></a><a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a>', $new_content);

                  
                  if($new_content == null) {
                    echo '<div id="bgc-template" style="background-color: #3b4856;">
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table align="center" cellpadding="0" cellspacing="0" class="table" role="presentation" style="background: #ffffff; background-color: #ffffff; width: 100%;">
<tbody>
<tr>
<td style="border-top: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 5px 0; text-align: center; vertical-align: top; color: black; border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; border-bottom: 0;"></td>
</tr>
</tbody>
</table>
</div>
<div class="draggableContainer  ui-droppable" id="595014239083">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="615958165117">
<div class="poc-content-block-outline remove-after" id="369856102086" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- IMAGE AT RIGHT OF TEXT - TEMPLATE START --><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0 10px;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" align="left" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<table align="left" cellpadding="0" cellspacing="0" class="table">
<tbody>
<tr>
<td rowspan="2" style="padding: 0 !important;" height="20"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--><!-- IMAGE AT RIGHT OF TEXT - TEMPLATE END --></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="110820214311">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="697613037548">
<div class="poc-content-block-outline remove-after" id="897219422069" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-edit edit-double double_dialog" data-toggle="modal" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" data-target="#double-697613037548" onclick="double_dialog(this)"><i class="fa fa-pencil" style="font-size: 20px;">&nbsp;</i></a><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- [if mso | IE]>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
<tr>
<td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table align="center" cellpadding="0" cellspacing="0" role="presentation" style="background: #ffffff; background-color: #ffffff; width: 100%;" class="table">
<tbody>
<tr>
<td style="border: 7px solid #eeeeee; border-bottom: 0px; border-top: 0px; direction: ltr; font-size: 0px; padding: 20px 0; padding-bottom: 0px; padding-top: 0px; text-align: center; vertical-align: top;"><!-- [if mso | IE]>
<table role="presentation" border="0" cellpadding="0" cellspacing="0">
<tr>
<td
class="" style="vertical-align:top;width:120px;"
<![endif]-->
<div class="mj-column-per-50 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellpadding="0" cellspacing="0" role="presentation" width="100%" class="table">
<tbody>
<tr>
<td style="vertical-align: top; padding: 0 5px;">
<table cellpadding="0" cellspacing="0" role="presentation" width="100%" class="table">
<tbody>
<tr>
<td align="justify" style="font-size: 0px; padding: 0px; word-break: break-word;">
<div class="left-paragraph-double" style="margin: 0 !important; font-family: Ubuntu, Helvetica, Arial, sans-serif; font-size: 13px; line-height: 14px; text-align: justify; color: #000000;" id="left-paragraph-double-697613037548">
<div style="text-align: center;"><span style="font-size: 12pt;">Sample Email Header</span></div>
</div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]>
</td>
<td
style="vertical-align:top;width:360px;">
<![endif]-->
<div class="mj-column-per-50 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellpadding="0" cellspacing="0" role="presentation" width="100%" class="table">
<tbody>
<tr>
<td style="vertical-align: top; padding: 0 5px;">
<table cellpadding="0" cellspacing="0" role="presentation" width="100%" class="table">
<tbody>
<tr>
<td align="justify" style="font-size: 0px; padding: 0px; word-break: break-word;">
<div class="right-paragraph-double" style="margin: 0 !important; font-family: Ubuntu, Helvetica, Arial, sans-serif; font-size: 13px; line-height: 14px; text-align: justify; color: #000000;" id="right-paragraph-double-697613037548">
<div style="text-align: center;"><a href="[VIEWBROWSERURL]"><span style="font-size: 12pt;">View in browser</span></a></div>
</div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]>
</td>
<![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]>
</td></tr>
</table>
<![endif]--></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="319151051041">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="819049217627">
<div class="poc-content-block-outline remove-after" id="302477574363" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- IMAGE AT RIGHT OF TEXT - TEMPLATE START --><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0 10px;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" align="left" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<table align="left" cellpadding="0" cellspacing="0" class="table">
<tbody>
<tr>
<td rowspan="2" style="padding: 0 !important;" height="20"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--><!-- IMAGE AT RIGHT OF TEXT - TEMPLATE END --></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="882896343681">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="570662987662">
<div class="poc-content-block-outline remove-after" id="334285755027" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- IMAGE AT RIGHT OF TEXT - TEMPLATE START --><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0 10px;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" align="left" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<table align="left" cellpadding="0" cellspacing="0" class="table">
<tbody>
<tr>
<td rowspan="2" style="padding: 0 !important;" height="20"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--><!-- IMAGE AT RIGHT OF TEXT - TEMPLATE END --></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="92142344597">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="517624868146">
<div class="poc-content-block-outline remove-after" id="529310815946" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-edit edit-text text_dialog" data-toggle="modal" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" data-target="#text-517624868146" onclick="text_dialog(this)"><i class="fa fa-pencil" style="font-size: 20px;">&nbsp;</i></a><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0 5px 0 5px; text-align: center; vertical-align: top;"><!-- [if mso | IE]><tablerole="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class=""style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" style="max-width: 600px; width: 100%;" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<div class="text-content" style="margin: 0 !important; font-family: Ubuntu, Helvetica, Arial,sans-serif; font-size: 13px; line-height: 14px; width: 100%; color: #000000;" id="text-content-517624868146">
<div style="padding: 0px !important; text-align: center;"><strong><span style="font-size: 36pt;">SAMPLE HEADER</span></strong></div>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></div>
<!-- [if mso | IE]></td></tr></table><![endif]-->
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="354260160642">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="947758048341">
<div class="poc-content-block-outline remove-after" id="703585748942" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- IMAGE AT RIGHT OF TEXT - TEMPLATE START --><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0 10px;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" align="left" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<table align="left" cellpadding="0" cellspacing="0" class="table">
<tbody>
<tr>
<td rowspan="2" style="padding: 0 !important;" height="20"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--><!-- IMAGE AT RIGHT OF TEXT - TEMPLATE END --></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="248260483473">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="951149699870">
<div class="poc-content-block-outline remove-after" id="856938350258" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- IMAGE AT RIGHT OF TEXT - TEMPLATE START --><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0 10px;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" align="left" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<table align="left" cellpadding="0" cellspacing="0" class="table">
<tbody>
<tr>
<td rowspan="2" style="padding: 0 !important;" height="20"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--><!-- IMAGE AT RIGHT OF TEXT - TEMPLATE END --></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="415105892440">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="930547319027">
<div class="poc-content-block-outline remove-after" id="939112047889" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-edit edit-left left_dialog" data-toggle="modal" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="left_dialog(this)"><i class="fa fa-pencil" style="font-size: 20px;">&nbsp;</i></a><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- IMAGE AT LEFT OF TEXT - TEMPLATE START --><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0 5px 0 5px; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0px10px;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" align="left" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<table align="left" cellpadding="0" cellspacing="0" class="table">
<tbody>
<tr><!-- the image or text -->
<td width="5" rowspan="2" style="padding: 0 !important;"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
<td class="left-img-content" style="padding: 0 5px 0 15px; margin: 0;"><img style="width: 100px; height: auto;" src="https://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" alt="Image" width="100" height="100"></td>
<td width="1" rowspan="2" style="padding: 0 !important;"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
</tr>
<tr style="padding: 0 !important;">
<td style="text-align: center; padding: 10px 5px 0 15px;"><span class="left-text-caption">Sample Caption</span></td>
</tr>
</tbody>
</table>
<!-- here your tekst -->
<div class="left-text-content" style="margin: 0 !important; font-family: Ubuntu, Helvetica, Arial, sans-serif; font-size: 13px; line-height: 14px; text-align: justify; color: #000000;"><span style="font-size: 12pt;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&nbsp;Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span></div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--><!-- IMAGE AT LEFT OF TEXT - TEMPLATE END --></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="719579187477">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="134845834948">
<div class="poc-content-block-outline remove-after" id="882214965210" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- IMAGE AT RIGHT OF TEXT - TEMPLATE START --><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0 10px;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" align="left" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<table align="left" cellpadding="0" cellspacing="0" class="table">
<tbody>
<tr>
<td rowspan="2" style="padding: 0 !important;" height="20"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--><!-- IMAGE AT RIGHT OF TEXT - TEMPLATE END --></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable poc-drop-zone-hover" id="177481120653">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="823045878353">
<div class="poc-content-block-outline remove-after" id="609277177077" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-edit edit-right right_dialog" data-toggle="modal" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="right_dialog(this)"><i class="fa fa-pencil" style="font-size: 20px;">&nbsp;</i></a><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- IMAGE AT RIGHT OF TEXT - TEMPLATE START --><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0 5px 0 5px; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0px10px;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" align="left" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<table align="right" cellpadding="0" cellspacing="0" class="table">
<tbody>
<tr><!-- the image or text -->
<td width="5" rowspan="2" style="padding: 0 !important;"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
<td class="right-img-content" style="padding: 0 15px 0 5px; margin: 0;"><img style="width: 100px; height: auto;" src="https://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" alt="Image" width="100" height="100"></td>
<td width="1" rowspan="2" style="padding: 0 !important;"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
</tr>
<tr style="padding: 0 !important;">
<td style="text-align: center; padding: 10px 15px 0 5px;"><span class="right-text-caption">Sample Caption</span></td>
</tr>
</tbody>
</table>
<!-- here your tekst -->
<div class="right-text-content" style="margin: 0 !important; font-family: Ubuntu, Helvetica, Arial, sans-serif; font-size: 13px; line-height: 14px; text-align: justify; color: #000000;"><span style="font-size: 12pt;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&nbsp;Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span></div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--><!-- IMAGE AT RIGHT OF TEXT - TEMPLATE END --></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="538734440067">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="735598283399">
<div class="poc-content-block-outline remove-after" id="847241062648" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- IMAGE AT RIGHT OF TEXT - TEMPLATE START --><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0 10px;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" align="left" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<table align="left" cellpadding="0" cellspacing="0" class="table">
<tbody>
<tr>
<td rowspan="2" style="padding: 0 !important;" height="20"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--><!-- IMAGE AT RIGHT OF TEXT - TEMPLATE END --></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="394535556">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="379683724178">
<div class="poc-content-block-outline remove-after" id="769123813377" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-edit edit-left left_dialog" data-toggle="modal" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="left_dialog(this)"><i class="fa fa-pencil" style="font-size: 20px;">&nbsp;</i></a><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- IMAGE AT LEFT OF TEXT - TEMPLATE START --><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0 5px 0 5px; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0px10px;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" align="left" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<table align="left" cellpadding="0" cellspacing="0" class="table">
<tbody>
<tr><!-- the image or text -->
<td width="5" rowspan="2" style="padding: 0 !important;"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
<td class="left-img-content" style="padding: 0 5px 0 15px; margin: 0;"><img style="width: 100px; height: auto;" src="https://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" alt="Image" width="100" height="100"></td>
<td width="1" rowspan="2" style="padding: 0 !important;"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
</tr>
<tr style="padding: 0 !important;">
<td style="text-align: center; padding: 10px 5px 0 15px;"><span class="left-text-caption">Sample Caption</span></td>
</tr>
</tbody>
</table>
<!-- here your tekst -->
<div class="left-text-content" style="margin: 0 !important; font-family: Ubuntu, Helvetica, Arial, sans-serif; font-size: 13px; line-height: 14px; text-align: justify; color: #000000;"><span style="font-size: 12pt;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&nbsp;Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span></div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--><!-- IMAGE AT LEFT OF TEXT - TEMPLATE END --></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="300874330908">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<div class="poc-combo-block poc-content-block we" id="677740855995">
<div class="poc-content-block-outline remove-after" id="57328900704" style="padding-bottom: 0 !important;">
<ul class="dima-menu sf-menu sf-js-enabled sf-arrows" style="float: right; touch-action: pan-y;">
<li style="display: flex;" class="actions-icon"><a class="a-icon-del delete_element" style="padding: 3px 10px 3px 10px; border: 1px solid; border-radius: 0px 0px 6px 6px;" onclick="delete_element(this)"><i class="fa fa-trash" style="font-size: 20px;">&nbsp;</i></a></li>
</ul>
</div>
<!-- IMAGE AT RIGHT OF TEXT - TEMPLATE START --><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table style="background: #ffffff; width: 100%;" role="presentation" cellspacing="0" cellpadding="0" align="center" class="table">
<tbody>
<tr>
<td style="border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0"cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;padding: 10px;"><![endif]-->
<div style="padding: 0 10px;">
<div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
<table cellspacing="0" cellpadding="0" align="left" class="table">
<tbody>
<tr>
<td style="padding: 0 !important;">
<table align="left" cellpadding="0" cellspacing="0" class="table">
<tbody>
<tr>
<td rowspan="2" style="padding: 0 !important;" height="20"><img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--></td>
</tr>
</tbody>
</table>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]--><!-- IMAGE AT RIGHT OF TEXT - TEMPLATE END --></div>
<div class="clear remove-after" style="padding-bottom: 0 !important;"></div>
<div class="draggableContainer  ui-droppable" id="594883218933">
<div class="poc-drop-zone-text" style="display: none;"><span class="poc-drop-here">Drop Here</span></div>
</div>
<!-- [if mso | IE]></td></tr></table><![endif]-->
<div style="background: #ffffff; background-color: #ffffff; margin: 0px auto; max-width: 600px;">
<table align="center" cellpadding="0" cellspacing="0" class="table" role="presentation" style="background: #ffffff; background-color: #ffffff; width: 100%;">
<tbody>
<tr>
<td style="border-bottom: 7px solid #eeeeee; direction: ltr; font-size: 0px; padding: 5px 0; text-align: center; vertical-align: top; color: black; border-left: 7px solid #eeeeee; border-right: 7px solid #eeeeee; border-top: 0;"></td>
</tr>
</tbody>
</table>
</div>
</div>';
                  } else {
                    echo $new_content;
                  }
                  
                  ?>
              </div>


              <table id="modals">
                <tr>
                  <td style="border-top: none;">
                    <div class="modal fade header-modal" role="dialog">
                      <div class="modal-dialog modal-sm">

                        <!-- kim Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="color: #000000;">Header</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                            <div class="clear"></div>
                            <div class="">
                              <div class="accordion">
                                <div class="accordion-section">
                                  <div class="accordion-section-title">
                                    <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                      <tbody>
                                      <tr>
                                        <td style="color: #000000;width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                          Date
                                        </td>
                                        <td style="width:70%;border: none !important;">
                                          <div>
                                            <input type="text" name="date_text" class="text-date" style="background: white !important"/>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="color: #000000;width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                          Title
                                        </td>
                                        <td style="width:70%;border: none !important;">
                                          <div>
                                            <input type="text" name="title_text" class="text-title" style="background: white !important"/>
                                          </div>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td style="border-top: none;">
                    <div class="modal fade img-modal" role="dialog">
                      <div class="modal-dialog modal-sm">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Replace Image</h4>
                          </div>
                          <div class="modal-body">
                            <form role="form" id="uploadMassEmail" method="post" enctype="multipart/form-data">
                              <div>
                                <input type="hidden" name="div-id-img" id="div-id-img" value="">
                                <input type='file' accept='image/*' name="profile_pic" class="profile_pic"
                                       style="margin-bottom: 7px;"/>
                                <img class="hover hidden pp-img" alt="image" style="width:586px;height:135px;"/>
                              </div>
                              <div class="hidden img_loader" style="text-align:center">
                                <p>Please wait</p>
                                <img src="icons/loader.gif" style="height: 2em;width: 24em;">
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="submit_btn text-center btn btn-success" id="update_pic">Update
                                </button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td style="border-top: none;">
                    <div class="modal fade btn-modal" role="dialog">
                      <div class="modal-dialog modal-sm">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Button</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                            <div class="clear"></div>
                            <div class="">
                              <div class="accordion">
                                <div class="accordion-section">
                                  <div class="accordion-section-title">
                                    <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                      <tbody>
                                      <tr>
                                        <td style="width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                          Text
                                        </td>
                                        <td style="width:70%;border: none !important;">
                                          <div>
                                            <input type="text" name="btn_text" class="btn_text" style="background: white !important">
                                          </div>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                                <div class="accordion-section">
                                  <div class="accordion-section-title">
                                    <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                      <tbody>
                                      <tr>
                                        <td style="width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                          Link
                                        </td>
                                        <td style="width:70%;border: none !important;">
                                          <div>
                                            <input type="text" name="src_btn" class="src-btn" style="background: white !important">
                                          </div>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                                <div class="accordion-section">
                                  <a class="accordion-section-title" href="#accordion-1">Spacing<span style="float: right;"><i class="fa fa-chevron-down">&nbsp;</i></span></a>
                                  <div id="accordion-1" class="accordion-section-content">
                                    <div class="" style="">
                                      <table width="100%">
                                        <tbody>
                                        <tr>
                                          <td style="width:50%">
                                            Top Margin
                                          </td>
                                          <td style="width:50%" align="center">
                                            <div class="input-group" style="width: 70%;">
                                              <input type="text" class="top-margin-btn" name="top-margin-btn" placeholder="" size="5px">
                                              <span class="input-group-addon">PX</span>
                                            </div>
                                            <br>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td style="width:50%">
                                            Bottom Margin
                                          </td>
                                          <td style="width:50%" align="center">
                                            <div class="input-group" style="width: 70%;">
                                              <input type="text" class="bottom-margin-btn" name="bottom-margin-btn" placeholder="" size="5px">
                                              <span class="input-group-addon">PX</span>
                                            </div>
                                            <br>
                                          </td>
                                        </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                                <div class="accordion-section">
                                  <a class="accordion-section-title" href="#accordion-3">Style<span style="float: right;"><i class="fa fa-chevron-down">&nbsp;</i></a>
                                  <div id="accordion-3" class="accordion-section-content">
                                    <div class="" style="">
                                      <table width="100%">
                                        <tbody>
                                        <tr>
                                          <td style="width:50%">
                                            Border
                                          </td>
                                          <td style="width:50%" align="center">
                                            <div class="input-group" style="width: 70%;">
                                              <input type="text" class="border-btn" placeholder="" size="5px">
                                              <span class="input-group-addon">PX</span>
                                            </div>
                                            <br>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td style="width:50%">
                                            Border Color
                                          </td>
                                          <td style="width:50%" align="center">
                                            <input type="text" class="border-color-btn">
                                            <br>
                                            <br>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td style="width:50%">
                                            Text Color
                                          </td>
                                          <td style="width:50%" align="center">
                                            <input type="text" class="text-color-btn">
                                            <br>
                                            <br>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td style="width:50%">
                                            Background Colors
                                          </td>
                                          <td style="width:50%" align="center">
                                            <input type="text" class="background-color-btn">
                                            <br>
                                            <br>
                                          </td>
                                        </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td style="border-top: none;">
                    <div class="modal fade right-modal" role="dialog">
                      <div class="modal-dialog">

                        <!-- kim Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Right Dialog</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                            <div class="clear"></div>
                            <div class="">
                              <div class="accordion">
                                <div class="accordion-section">
                                  <div class="accordion-section-title">
                                    <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                      <tbody>
                                      <tr>
                                        <td style="width:100%;border: none !important;padding: 13px 0px 0px 0px;">
                                          <div id="accordion">
                                            <div class="card">
                                              <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                  <i class="btn btn-primary" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Add Caption
                                                  </i>
                                                </h5>
                                              </div>

                                              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="height:0px;">
                                                <div class="card-body">
                                                  <textarea id="caption1-tinymce"></textarea>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:100%;border: none !important;padding: 13px 0px 0px 0px;">
                                          <div id="accordion">
                                            <div class="card">
                                              <div class="card-header" id="headingTwo">
                                                <h5 class="mb-0">
                                                  <i class="btn btn-primary" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                    Add Image
                                                  </i>
                                                </h5>
                                              </div>

                                              <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion" style="height:0px;">
                                                <div class="card-body">
                                                  <textarea id="img-tinymce"><img height="auto" src="https://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100px;" width="100" data-mce-selected="1"/></textarea>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" style="width:30%;border: none !important;padding: 13px 0 0 0;">
                                          Content
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">
                                          <textarea id="m-tinymce"></textarea>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td style="border-top: none;">
                    <div class="modal fade" id="bgc-modal" role="dialog">
                      <div class="modal-dialog">

                        <!-- kim Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Background Color</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                            <div class="clear"></div>
                            <div class="">
                              <div class="accordion">
                                <div class="accordion-section">
                                  <div class="accordion-section-title">
                                    <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                      <tbody>
                                      <tr>
                                          <td style="width:50%">
                                            Background Color
                                          </td>
                                          <td style="width:50%" align="center">
                                            <input type="text" class="background-color-btn">
                                            <br>
                                            <br>
                                          </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td style="border-top: none;">
                    <div class="modal fade twoimg-modal" role="dialog">
                      <div class="modal-dialog">
                        <!-- kim Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Two Images Dialog</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                            <div class="clear"></div>
                            <div class="">
                              <div class="accordion">
                                <div class="accordion-section">
                                  <div class="accordion-section-title">
                                    <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                      <tbody>
                                      <tr>
                                        <td style="border: none !important;padding: 13px 0px 0px 0px;">
                                          <div id="accordion">
                                            <div class="card">
                                              <div class="card-header" id="headingSix">
                                                <h5 class="mb-0">
                                                  <i class="btn btn-primary" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                                    Add Left Caption
                                                  </i>
                                                </h5>
                                              </div>

                                              <div id="collapseSix" class="collapse show" aria-labelledby="headingSix" data-parent="#accordion" style="height:0px;">
                                                <div class="card-body">
                                                  <textarea id="caption3-tinymce"></textarea>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="border: none !important;padding: 13px 0px 0px 0px;">
                                          <div id="accordion">
                                            <div class="card">
                                              <div class="card-header" id="headingSeven">
                                                <h5 class="mb-0">
                                                  <i class="btn btn-primary" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                                    Add Right Caption
                                                  </i>
                                                </h5>
                                              </div>

                                              <div id="collapseSeven" class="collapse show" aria-labelledby="headingSeven" data-parent="#accordion" style="height:0px;">
                                                <div class="card-body">
                                                  <textarea id="caption4-tinymce"></textarea>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:100%;border: none !important;padding: 13px 0px 0px 0px;">
                                          <div id="accordion">
                                            <div class="card">
                                              <div class="card-header" id="headingeight">
                                                <h5 class="mb-0">
                                                  <i class="btn btn-primary" data-toggle="collapse" data-target="#collapseeight" aria-expanded="true" aria-controls="collapseeight">
                                                    Add Left Image
                                                  </i>
                                                </h5>
                                              </div>

                                              <div id="collapseeight" class="collapse show" aria-labelledby="headingeight" data-parent="#accordion" style="height:0px;">
                                                <div class="card-body">
                                                  <textarea id="img-tinymce-twoimg"><img height="auto" src="https://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100px;" width="100" data-mce-selected="1" /></textarea>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:100%;border: none !important;padding: 13px 0px 0px 0px;">
                                          <div id="accordion">
                                            <div class="card">
                                              <div class="card-header" id="headingnine">
                                                <h5 class="mb-0">
                                                  <i class="btn btn-primary" data-toggle="collapse" data-target="#collapsenine" aria-expanded="true" aria-controls="collapsenine">
                                                    Add Right Image
                                                  </i>
                                                </h5>
                                              </div>

                                              <div id="collapsenine" class="collapse show" aria-labelledby="headingnine" data-parent="#accordion" style="height:0px;">
                                                <div class="card-body">
                                                  <textarea id="img-tinymce-twoimg-two"><img height="auto" src="https://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100px;" width="100" data-mce-selected="1" /></textarea>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" style="width:30%;border: none !important;padding: 13px 0 0 0;">
                                          Content
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">
                                          <textarea id="m-tinymce-twoimg"></textarea>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td style="border-top: none;">
                    <div class="modal fade left-modal" role="dialog">
                      <div class="modal-dialog">
                        <!-- kim Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Left Dialog</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                            <div class="clear"></div>
                            <div class="">
                              <div class="accordion">
                                <div class="accordion-section">
                                  <div class="accordion-section-title">
                                    <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                      <tbody>
                                      <tr>
                                        <td style="width:100%;border: none !important;padding: 13px 0px 0px 0px;">
                                          <div id="accordion">
                                            <div class="card">
                                              <div class="card-header" id="headingFour">
                                                <h5 class="mb-0">
                                                  <i class="btn btn-primary" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                                    Add Caption
                                                  </i>
                                                </h5>
                                              </div>

                                              <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#accordion" style="height:0px;">
                                                <div class="card-body">
                                                  <textarea id="caption2-tinymce"></textarea>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:100%;border: none !important;padding: 13px 0px 0px 0px;">
                                          <div id="accordion">
                                            <div class="card">
                                              <div class="card-header" id="headingThree">
                                                <h5 class="mb-0">
                                                  <i class="btn btn-primary" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                                    Add Image
                                                  </i>
                                                </h5>
                                              </div>

                                              <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion" style="height:0px;">
                                                <div class="card-body">
                                                  <textarea id="img-tinymce-second"><img height="auto" src="https://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100px;" width="100" data-mce-selected="1" /></textarea>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" style="width:30%;border: none !important;padding: 13px 0 0 0;">
                                          Content
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">
                                          <textarea id="m-tinymce-second"></textarea>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td style="border-top: none;">
                    <div class="modal fade fixed-left-modal" role="dialog">
                      <div class="modal-dialog">
                        <!-- kim Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Fixed-Left Dialog</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                            <div class="clear"></div>
                            <div class="">
                              <div class="accordion">
                                <div class="accordion-section">
                                  <div class="accordion-section-title">
                                    <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                      <tbody>
                                      <tr>
                                        <td style="width:100%;border: none !important;padding: 13px 0px 0px 0px;">
                                          <div id="accordion">
                                            <div class="card">
                                              <div class="card-header" id="headingfive">
                                                <h5 class="mb-0">
                                                  <i class="btn btn-primary" data-toggle="collapse" data-target="#collapsefive" aria-expanded="true" aria-controls="collapsefive">
                                                    Add Image
                                                  </i>
                                                </h5>
                                              </div>

                                              <div id="collapsefive" class="collapse show" aria-labelledby="headingfive" data-parent="#accordion" style="height:0px;">
                                                <div class="card-body">
                                                  <textarea id="fixed-img-tinymce-second"><img height="auto" src="https://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100px;" width="100" data-mce-selected="1" /></textarea>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" style="width:30%;border: none !important;padding: 13px 0 0 0;">
                                          Content
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">
                                          <textarea id="fixed-m-tinymce-second"></textarea>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td style="border-top: none;">
                    <div class="modal fade text-modal" role="dialog">
                      <div class="modal-dialog">

                        <!-- kim Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Text Dialog</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                            <div class="clear"></div>
                            <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                              <tbody>
                              <tr>
                                <td colspan="2" style="width:30%;border: none !important;padding: 13px 0 0 0;">
                                  Content
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                  <textarea id="t-tinymce"></textarea>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                          <div class="modal-footer">
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td style="border-top: none;">
                    <div class="modal fade double-modal" role="dialog">
                      <div class="modal-dialog">
                        <!-- kim Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Side by side Dialog</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                            <div class="clear"></div>
                            <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                              <tbody>
                              <tr>
                                <td colspan="2" style="width:30%;border: none !important;padding: 13px 0 0 0;">
                                  Left Paragraph
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                  <textarea id="left-tinymce"></textarea>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2" style="width:30%;border: none !important;padding: 13px 0 0 0;">
                                  Right Paragraph
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                  <textarea id="right-tinymce"></textarea>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                          <div class="modal-footer">
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- <input class="submit" type="submit" name="save"
           value="<?php echo $GLOBALS['I18N']->get('Save Changes') ?>"/> -->
    <!--    <p id="information" style="transition: all .5s;background-color: #00ff5d;color: #FFFFFF;padding: 10px;-webkit-border-radius: 3px;-moz-border-radius: 3px;border-radius: 3px; position: fixed; bottom: 0; right: 1%; display: none;">&#10004</p>-->
    <!--  </div></div>-->
        <?php //$sendtest_content = sprintf('<div class="sendTest" id="sendTest">
        //    ' .$sendtestresult.'
        //    <input class="submit" type="submit" name="sendtest" value="%s"/>  %s:
        //    <input type="text" name="testtarget" size="40" value="' .htmlspecialchars($testtarget).'"/><br />%s
        //    </div>',
        //    $GLOBALS['I18N']->get('Send test message'), $GLOBALS['I18N']->get('to email addresses'),
        //    $GLOBALS['I18N']->get('(comma separate addresses - all must be existing subscribers)'));
        //$testpanel = new UIPanel($GLOBALS['I18N']->get('Send Test'), $sendtest_content);
        //$testpanel->setID('testpanel');
        ////  if ($systemTemplateID == $id) { ## for now, testing only for system message templates
        //echo $testpanel->display();
        ////  }
        //?>

    </form>
    <script>
      let main_flag = document.getElementById('w-template').innerHTML;


      setInterval(function(){

        const my_content2 = document.getElementById('w-template').innerHTML;
        if(my_content2 !== main_flag) {
          main_flag = my_content2;

          const $div = $('<div>').html(my_content2);
          $div.find('.remove-after').attr("style", "padding-bottom: 0 !important;display:none;");
          $div.find('.remove-after-2').attr("style", "display:none");
          const processedHTML =
             '<!DOCTYPE html>\n' +
             '<html>\n' +
             '<head>\n' +
             '<link href="https://salesrobot.com/sales/images/favicon.png" rel="shortcut icon" type="image/png" />\n' +
             '  <title>Executive Mosaic Weekly Roundup</title>'+
             '<!--[if !mso]><!-- -->\n' +
             '<meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]--><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1">\n' +
             '<style type="text/css">#outlook a{padding:0}.ReadMsgBody{width:100%}.ExternalClass{width:100%}.ExternalClass *{line-height:100%}body{margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt}img{border:0;height:auto;line-height:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}p{display:block;margin:13px 0}\n' +
             '</style>\n' +
             '<!--[if !mso]><!-->\n' +
             '<style type="text/css">@media only screen and (max-width:480px){@-ms-viewport {width:320px}@viewport {width:320px}}\n' +
             '</style>\n' +
             '<!--<![endif]--><!--[if mso]>\n' +
             '<xml>\n' +
             '   <o:OfficeDocumentSettings>\n' +
             '      <o:AllowPNG></o:AllowPNG>\n' +
             '      <o:PixelsPerInch>96</o:PixelsPerInch>\n' +
             '   </o:OfficeDocumentSettings>\n' +
             '</xml>\n' +
             '<![endif]--><!--[if lte mso 11]>\n' +
             '<style type="text/css">\n' +
             '   .outlook-group-fix { width:100% !important; }\n' +
             '</style>\n' +
             '<![endif]--><!--[if !mso]><!-->\n' +
             '<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css" />\n' +
             '<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css" /><!--<![endif]-->\n' +
             '<style type="text/css">@media only screen and (min-width:480px){.mj-column-per-100{width:100%!important;max-width:100%}.mj-column-per-20{width:20%!important;max-width:20%}.mj-column-per-60{width:60%!important;max-width:60%}.mj-column-per-80{width:80%!important;max-width:80%}.mj-column-per-50{width:50%!important;max-width:50%}}\n' +
             '</style>\n' +
             '<style type="text/css">@media only screen and (max-width:480px){table.full-width-mobile{width:100%!important}td.full-width-mobile{width:auto!important}}\n' +
             '</style>\n' +
             '</head>\n' +
             '<body>' + $div.html() + '</body>\n' +
             '</html>';
          // CKEDITOR.instances.template.setData(processedHTML);
          tinymce.get('template').setContent(processedHTML);
          const post_description = tinyMCE.get('template').getContent();
          let srcUrl = post_description.search('src="/uploadimages');
          let newDesc = post_description.replace(/<head><\/head><body><style type="text\/css">outlook a{padding:0}.ReadMsgBody{width:100%}.ExternalClass{width:100%}.ExternalClass *{line-height:100%}body{margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt}img{border:0;height:auto;line-height:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic}p{display:block;margin:13px 0}\r\n<\/style>\r\n<!-- [if !mso]>\r\n<style type="text\/css">@media only screen and (max-width:480px){@-ms-viewport {width:320px}@viewport {width:320px}}\r\n<\/style>\r\n<!--<![endif]-->\r\n<p style="display: none;"><\/p>\r\n<!-- [if mso]>\r\n<xml>\r\n<o:OfficeDocumentSettings>\r\n<o:AllowPNG><\/o:AllowPNG>\r\n<o:PixelsPerInch>96<\/o:PixelsPerInch>\r\n<\/o:OfficeDocumentSettings>\r\n<\/xml>\r\n<![endif]-->\r\n<p style="display: none;"><\/p>\r\n<!-- [if lte mso 11]>\r\n<style type="text\/css">\r\n.outlook-group-fix { width:100% !important; }\r\n<\/style>\r\n<![endif]-->\r\n<style type="text\/css">@media only screen and (min-width:480px){.mj-column-per-100{width:100%!important;max-width:100%}.mj-column-per-20{width:20%!important;max-width:20%}.mj-column-per-60{width:60%!important;max-width:60%}.mj-column-per-80{width:80%!important;max-width:80%}.mj-column-per-50{width:50%!important;max-width:50%}}\r\n<\/style>\r\n<style type="text\/css">@media only screen and (max-width:480px){table.full-width-mobile{width:100%!important}td.full-width-mobile{width:auto!important}}\r\n<\/style>/g,'');
          tinyMCE.get('template').setContent(newDesc);

          let srcDeleteElement = post_description.search('onclick="delete_element');
          let srcTextDialog = post_description.search('onclick="text_dialog');
          let srcDoubleDialog = post_description.search('onclick="double_dialog');
          let srcLeftDialog = post_description.search('onclick="left_dialog');
          let srcRightDialog = post_description.search('onclick="right_dialog');
          let srcHeaderDialog = post_description.search('onclick="header_dialog');
          let srcFixedLeftDialog = post_description.search('onclick="fixed_left_dialog');
          let srcTwoimgDialog = post_description.search('onclick="twoimg_dialog');

          if(srcDeleteElement > -1) {
            newDesc = newDesc.replace(/onclick="delete_element\(this\)"/g, '');
            tinyMCE.get('template').setContent(newDesc);
          }
          if(srcTwoimgDialog > -1) {
            newDesc = newDesc.replace(/onclick="twoimg_dialog\(this\)"/g, '');
            tinyMCE.get('template').setContent(newDesc);
          }
          if(srcFixedLeftDialog > -1) {
            newDesc = newDesc.replace(/onclick="fixed_left_dialog\(this\)"/g, '');
            tinyMCE.get('template').setContent(newDesc);
          }
          if(srcTextDialog > -1) {
            newDesc = newDesc.replace(/onclick="text_dialog\(this\)"/g, '');
            tinyMCE.get('template').setContent(newDesc);
          }
          if(srcDoubleDialog > -1) {
            newDesc = newDesc.replace(/onclick="double_dialog\(this\)"/g, '');
            tinyMCE.get('template').setContent(newDesc);
          }
          if(srcLeftDialog > -1) {
            newDesc = newDesc.replace(/onclick="left_dialog\(this\)"/g, '');
            tinyMCE.get('template').setContent(newDesc);
          }
          if(srcRightDialog > -1) {
            newDesc = newDesc.replace(/onclick="right_dialog\(this\)"/g, '');
            tinyMCE.get('template').setContent(newDesc);
          }
          if(srcHeaderDialog > -1) {
            newDesc = newDesc.replace(/onclick="header_dialog\(this\)"/g, '');
            tinyMCE.get('template').setContent(newDesc);
          }


          if(srcUrl > -1) {
            newDesc = newDesc.replace(/src="\/uploadimages/g, 'src="https:\/\/salesrobot.com/uploadimages');
            tinyMCE.get('template').setContent(newDesc);
          }

          const my_title =  document.getElementsByClassName('my-title');
          $('#post_title').val(my_title[0].innerHTML);

          const post_title = $('#post_title').val();
          const post_id = $('#post_id').val();

          if (post_title !== '' && post_description !== '') {
            $.ajax({
              // sales
              url: "../../admin/plugins/CKEditorPlugin/kcfinder/autosave.php",
              // archintel
              // url: "../../admin/plugins/CKEditorPlugin/kcfinder/save_post.php",
              method: "POST",
              data: {postTitle: post_title, postDescription: newDesc, postId: post_id},
              dataType: "text",
              success: function (data) {
                if (data !== '') {
                  $('#post_id').val(data);
                }
              }
            });
          }
        }

        $('.delete_element').attr("onclick", "delete_element(this)");
        $('.text_dialog').attr("onclick", "text_dialog(this)");
        $('.double_dialog').attr("onclick", "double_dialog(this)");
        $('.left_dialog').attr("onclick", "left_dialog(this)");
        $('.right_dialog').attr("onclick", "right_dialog(this)");
        $('.header_dialog').attr("onclick", "header_dialog(this)");
        $('.fixed_left_dialog').attr("onclick", "fixed_left_dialog(this)");
        $('.twoimg_dialog').attr("onclick", "twoimg_dialog(this)");

      }, 10);
    </script>
