<!DOCTYPE html>
<html lang="en">
<head>
      <link href="https://salesrobot.com/sales/images/favicon.png" rel="shortcut icon" type="image/png">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
      <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
            crossorigin="anonymous">
      <title>Executive Mosaic’s Weekly GovCon Round-Up: Past Issues</title>
      <script>
            function sortDescending(a, b) {
                  var date1 = $(a).find(".date").text();
                  date1 = date1.split('/');
                  date1 = new Date(date1[2], date1[0] - 1, date1[1]);
                  var date2 = $(b).find(".date").text();
                  date2 = date2.split('/');
                  date2 = new Date(date2[2], date2[0] - 1, date2[1]);

                  return date1 < date2 ? 1 : -1;
            }
            
            $( document ).ready(function() {
                  $('ul > li').sort(sortDescending).appendTo('ul');
            });
      </script>
</head>
<body style="background-image: url(https://www.govconwire.com/wp-content/themes/sahifa/images/patterns/body-bg26.png); background-position: top center;font-family: 'Roboto', sans-serif;">
      <div style="text-align:center;">
            
            <div style="margin-top:70px;max-width:600px;width:100%;background-color: #ffffff;border-radius:5px;padding:10px 10px;display: inline-block;text-align: left;font-size:16px">
            <div style="display:inline-block;padding-left:10px;font-size:20px;">Executive Mosaic’s Weekly GovCon Round-Up Past Issues</div>
            <?php
                  $directory = "../../sales/admin/archives/";
                  $archives = glob($directory."*.html");
                  $no_of_files = count($archives); 
                  $total_pages = ceil($no_of_files/5);
                  $page = $_REQUEST['page'];
                  $limit = 5;
                  $offset = 0;
                  $finalArray = array();

                  if(empty($page)) {
                        $offset = 0;
                        $page = 1;
                  } else {
                        $offset = ($page - 1) * $limit; 
                  }

                  
                  echo '<ul style="width:90%;padding:0 0 0 30px;">';
                  foreach($archives as $archive) {
                        $homepage = file_get_contents($archive);
                        $homepage = preg_replace('#<head(.*?)>(.*?)</head>#is', '', $homepage);
                        $html = strip_tags($homepage);
                        $html = str_replace('The Week\'s Top GovCon News Stories', '', $html);
                        $html = str_replace('View in the browser', '', $html);
                        $html = str_replace('Click here to see Real-Time GovCon Sector Quotes', '', $html);
                        $html = str_replace('Weekly Roundup |', '', $html);
                        $html = str_replace('Drop Here', '', $html);
                        $html = str_replace('&nbsp;', '', $html);
                        $html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
                        $html = mb_substr($html, 0, 600, 'UTF-8');
                        $html .= "…";               
                        $archive = str_replace("&", "&amp;", $archive);
                        $filename = str_replace($directory, "", $archive);
                        $filename = str_replace(".html", "", $filename);
                        $filename = str_replace("Executive Mosaic’s Weekly GovCon Round-Up: ", "", $filename);
                        $filename = str_replace("Executive Mosaic's Weekly GovCon Roundup: ", "", $filename);
                        $filename = str_replace("Executive Mosaic's Weekly GovCon RoundUp: ", "", $filename);
                        $filename = str_replace("Executive Mosaic’s Weekly GovCon Round-Up:", "", $filename);
                        $filename = str_replace("Executive Mosaic’s Weekly GovCon Round-Up_ ", "", $filename);
                        $filename = str_replace("Executive Mosaic’s Weekly GovCon Round-up_ ", "", $filename);
                        $filename = str_replace("&amp;", "&", $filename);
                        $myArray = explode('--', $filename);
                        $datePublished = str_replace("-", "/", $myArray[1]);
                        $datePublished = str_replace("-", "/", $datePublished);
                        $title = str_replace(" &amp; Top 10 Stories ", "", $myArray[0]);
                        $title = str_replace(" & Top 10 Stories ", "", $title);

                        // $date = $myArray[2];
                        // echo $date;
                        // date_default_timezone_set('Asia/Taipei');
                        // $time = date('h:i:s a', time());
                        // echo $time;

                        array_push($finalArray,  array($title, $html, $datePublished, $archive));
                  }

                  function date_compare($a, $b)
                  {
                        $t1 = strtotime($a[2]);
                        $t2 = strtotime($b[2]);
                        return $t2 - $t1;
                  }    
                  usort($finalArray, 'date_compare');

                  $filtered = array_slice($finalArray,$offset,$limit);

                  foreach($filtered as $item) {
                        $title = $item[0];
                        $html = $item[1];
                        $datePublished = $item[2];
                        $archive = $item[3];

                        echo '<li style="list-style:none;border-top:1px #eeeeee solid;padding:10px;">';
                        echo '<table>';
                        echo '<tr><td colspan="2"><a href="'.$archive.'" style="text-decoration:none;font-size:20px;" target="_blank">'.$title.'</a></td></tr>';
                        echo '<tr><td><a href="'.$archive.'" style="text-decoration:none;" target="_blank"><img src="https://salesrobot.com/uploadimages/image/default/emosaic.png" alt="image" style="width:100px;height:auto"></a></td>
                                                <td><a href="'.$archive.'" style="text-decoration:none;color:dimgray;" target="_blank">'.$html.'</a></td></tr>';
                        echo '<tr><td><div style="font-size:14px;margin-top:5px;color:dimgray;"><i class="fa fa-clock-o" aria-hidden="true"></i><span class="date"> '.$datePublished.'</span></div></td><td><a href="'.$archive.'" style="text-decoration:none;" target="_blank"><div style="font-size:14px;color:#ffffff;background-color: #404040;display:inline-block;padding: 4px; border-radius:2px;">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></div></a></td></tr>';
                        echo '</table>';
                        echo '</li>';
                  }

                  echo '</ul>';
                  
                  echo '<div style="margin-top:20px;text-align:left;width:100%;border-top:5px solid #404040;font-size:14px;">';
                  for ($x = 0; $x < $total_pages; $x++) {
                        $a = $x + 1;
                        if($a == $page) {
                              echo '<a href="?page='.$a.'" style="display:inline-block;text-decoration:none;color:#ffffff;padding: 2px 7px;background-color:#404040;margin: 0 2px 2px 0;">'.$a.'</a>  ';
                        } else {
                              if($a == $total_pages) {
                                    echo '<a href="?page='.$a.'" style="display:inline-block;text-decoration:none;color:#404040;padding: 2px 7px;margin: 0 2px 2px 0;">Last</a>  ';
                              } else if ($a == 1) {
                                    echo '<a href="?page='.$a.'" style="display:inline-block;text-decoration:none;color:#404040;padding: 2px 7px;margin: 0 2px 2px 0;">First</a>  ';
                              }
                              else {
                                    echo '<a href="?page='.$a.'" style="display:inline-block;text-decoration:none;color:#404040;padding: 2px 7px;margin: 0 2px 2px 0;">'.$a.'</a>  ';
                              }
                        }
                  }
                  echo '<div style="float:right;margin-right:5px;margin-top:3px;">Page '.$page.' of '.$total_pages.'</div>';
                  echo '</div>';

                  ?>
            </div>
      </div>
</body>
</html>