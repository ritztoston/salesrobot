<?php

/**
 * MessageStatisticsPlugin for phplist.
 *
 * This file is a part of MessageStatisticsPlugin.
 *
 * This plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @category  phplist
 *
 * @author    Duncan Cameron
 * @copyright 2011-2017 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 */

/**
 * Sub-class that provides the populator and exportable functions
 * for links.
 *
 * @category  phplist
 */
class MessageStatisticsPlugin_Controller_Links extends MessageStatisticsPlugin_Controller implements CommonPlugin_IPopulator, CommonPlugin_IExportable
{
    const IMAGE_HEIGHT = 300;
    const EXCLUDE_REGEX = 'p=preferences|p=unsubscribe|phplist.com';

    private $messageResults;
    protected $itemsPerPage = array(array(15, 25), 15);


    protected function createChart($chartDiv)
    {
        if (count($this->messageResults) == 0) {
            return '';
        }
        $chart = new Chart('ComboChart');
        $data = array();

        foreach ($this->messageResults as $row) {
            $data[] = array(
                'URL' => $row['url'],
                'Total Clicks' => (int) $row['numclicks'],
                'Unique Subscribers' => (int) $row['usersclicked'],
            );
        }

        $chart->load($data, 'array');
        $options = array(
            'height' => self::IMAGE_HEIGHT,
            'axisTitlesPosition' => 'out',
            'vAxis' => array('title' => 'Total Clicks', 'gridlines' => array('count' => 10), 'logScale' => false, 'format' => '#'),
            'hAxis' => array('title' => 'URL'),
            'seriesType' => 'line',
            'series' => array(0 => array('type' => 'bars')),
            'legend' => array('position' => 'bottom'),
            'colors' => array('blue', 'green', 'yellow', 'red'),
        );
        $result = $chart->draw($chartDiv, $options);

        return $result;
    }
    /*
     * Implementation of CommonPlugin_IExportable
     */
    public function exportFieldNames()
    {
        return $this->i18n->get(array(
            'URL', 'subscribers', 'subscribers %', 'total clicks', 'firstclick', 'latestclick',
        ));
    }

    public function exportRows()
    {
        return $this->model->links(0, 'all');
    }

    public function exportValues(array $row)
    {
        return array(
            $row['url'],
            $row['usersclicked'],
            $this->calculateRate($row['usersclicked'], $row['totalsent']),
            $row['numclicks'],
            $row['firstclick'],
            $row['numclicks'] > 1 ? $row['latestclick'] : '',
        );
    }

    /*
     * Implementation of CommonPlugin_IPopulator
     */
    public function populate(WebblerListing $w, $start = null, $limit = null)
    {
        /*
         * Populates the webbler list with link details
         */
        $w->setTitle($this->i18n->get('Links in the campaign'));
        $w->setElementHeading('URL');

        $resultSet = $this->model->links($start, $limit);
        $rows = iterator_to_array($this->model->links($start, $limit));
        $this->messageResults = array_reverse($rows);

        $query = array(
            'listid' => $this->model->listid,
            'msgid' => $this->model->msgid,
            'type' => 'linkclicks',
        );

        foreach ($resultSet as $row) {
            $key = preg_replace('%^(http|https)://%i', '', $row['url']);

            if (strlen($key) > 39) {
                $key = htmlspecialchars(substr($key, 0, 22)) . '&nbsp;...&nbsp;' . htmlspecialchars(substr($key, -12));
            }
            $key = sprintf('<span title="%s">%s</span>', htmlspecialchars($row['url']), $key);
            $query['forwardid'] = $row['forwardid'];
            $w->addElement($key, new CommonPlugin_PageURL(null, $query));
            $destinationLink = CHtml::tag(
                'a',
                ['target' => '_blank', 'href' => $row['url'], 'class' => 'nobutton', 'title' => $row['url']],
                new CommonPlugin_ImageTag('external.png', '')
            );
            $w->addColumnHtml(
                $key,
                $this->i18n->get('Link'),
                $row['personalise']
                    ? new CommonPlugin_ImageTag('user.png', 'URL is personalised')
                    : $destinationLink
            );
            $w->addColumn(
                $key,
                $this->i18n->get('subscribers'),
                $row['usersclicked'] > 0
                    ? sprintf('%d (%s%%)', $row['usersclicked'], $this->calculateRate($row['usersclicked'], $row['totalsent']))
                    : 0
            );
            $w->addColumn($key, $this->i18n->get('total clicks'), $row['numclicks']);
            $w->addColumn($key, $this->i18n->get('firstclick'), $row['firstclick']);
            $w->addColumn($key, $this->i18n->get('latestclick'), $row['numclicks'] > 1 ? $row['latestclick'] : '');
        }
    }

    public function total()
    {
        return $this->model->totalLinks();
    }
}
