<?php
    $this->load->view('includes/siteheader');
    //get event sponsors banner/pisture
    if($this->security->xss_clean($this->input->get('page')) == 'S')
    {
        $event_sponsors = $this->Global->get_event_sponsors($this->security->xss_clean($this->input->get('events')));
        $events = $this->Global->upcoming_events_byID($this->security->xss_clean($this->input->get('events')));
        $registration_src = $this->security->xss_clean($this->input->get('src')) ? $this->security->xss_clean($this->input->get('events')).'?src='.$this->security->xss_clean($this->input->get('src')) : $this->security->xss_clean($this->input->get('events'));
    }
    //event sponsors
    if($this->hexiros->logged_in()){
        $user_info = $this->Users->getdetails_bySID($this->session->userdata('account')['salesforceId']); 
    }

?>
    <link rel="stylesheet" data-them="" href="cssv2/styles.css">
    <title>Registration Form - Event</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/specific/revolution-slider/css/settings.css');?>" media="screen" />
    <link href= "https://fonts.googleapis.com/css?family=Open+Sans:100,400,600,700,300" rel="stylesheet" type="text/css">
    <link href= "https://fonts.googleapis.com/css?family=Lato:300,400,300italic,400italic" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="<?php echo base_url('assets/dahon.png'); ?>" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?php echo base_url('assets/images/poc-logo_poc-site.jpg'); ?>" type="image/x-icon" />
    <link rel="stylesheet" data-them="" href="<?php echo base_url('assets/cssv2/box_custom.css');?>">

    <!-- AddThisEvent -->
    <script type="text/javascript" src="https://addthisevent.com/libs/1.6.0/ate.min.js"></script>

    <style type="text/css">
        .copyx{
            display: none !important;
        }
        .dima-navbar-wrap .dima-navbar nav .dima-nav > li > a{
            padding-right: 10px !important;
        }
    </style>
</head>

<body class="responsive boxed checkered_pattern">
    <!-- LOADING -->
    <div class="all_content">
        <!-- HEADER -->
        <header role="banner">
            <div class="dima-navbar-wrap">
                <div class="dima-navbar fix-one  ">
                    <div class="clearfix dima-nav-fixed"></div>
                    <div class="container">
                        <!-- Nav bar button -->
                        <!-- LOGO -->
                        <div class="logo">
                            <h1>
                                <a href="<?php echo base_url(); ?>" title="potomacofficersclub.com logo">
                                    <span class="vertical-middle">
                                        <img src="<?php echo base_url('assets/images/poc-logo_poc-site.jpg')?>" alt="Potomac Officers Club Logo" title="Potomac Officers Club">
                                    </span>
                                </a>
                            </h1>
                        </div>
                        <!-- MENU -->
                        <nav role="navigation" class="clearfix" >
                            <!-- menu content -->
                            <ul class="dima-nav sf-menu">
                                <li class="sub-icon" style="margin-top:8em"></li>
                            </ul>
                            <!-- !menu content -->
                        </nav>
                        <!-- MENU -->
                    </div>

                    <?php 
                        if($this->security->xss_clean($this->input->get('page')) == 'S'){
                            $name=$events->Web_Event_Name__c;
                            $id=$events->salesforce_id;
                            $pic=$events->EventBannerLinkShort__c;
                            $desc=$events->Description;
                            $date=$events->StartDate;
                            $dateStart=$events->Start_Time__c;
                            $dateEnd=$events->Time_End__c;
                            $addr1=$events->Address_1;
                            $addr2=$events->Address_2;
                            $addr=$events->Address;
                            $phone=$events->Phone;
                            $sponsor1=$events->Sponsor_1;
                            $sponsor2=$events->Sponsor_2;
                            $sponsor3=$events->Sponsor_3;
                            $sponsor4=$events->Sponsor_4;
                            $sponsor5=$events->Sponsor_5;
                            $book_link=$events->Book_Website__c;
                            $book_desc=$events->Book_Description__c;
                            $book_price=$events->Book_Price__c;
                            $book_signing=$events->Book_Signing__c;

                        }else{
                            $book_price=0;
                        }                 
                    ?>
                </div>
                <div class="dima"></div>
            </div>
        </header>
        <!--! HEADER -->

         <!-- ALL CONTENTS -->
        <div class="dima-main">
            <!-- SECTION -->
            <section class="section">
                <div class="page-section-content boxed-blog blog-list" style="padding:0.25em 0;">
                    <div class="container blog">
                        <div class=" first main cl_8">
                            <!-- POST -->
                            <div class="post ">
                                <div class="post-meta box" style="border-bottom:none;padding:0px;">
                                </div>
                                <div class="post-content box">
                                    <h3 style="line-height:1.3em; text-align:center; margin-top: 17px;">
                                        Registration Form
                                    </h3>
                                    <div class="topaz-line full" style="margin:0;">
                                        <i class="di-separator" style="color:black">
                                        </i>
                                    </div>
                                    <!--FORM-->
                                    <?php
                                        echo form_open('',array('id'=>'register_guest','class'=>'form'));
                                        $Company = $this->Users->getCompany();
                                            $company_arr = array();
                                            foreach($Company->result() as $company){
                                                array_push($company_arr, array("ID" => $company->ID,"name" => $company->name)); 
                                            }
                                            $company__ = json_encode($company_arr);
                                            $events = $this->security->xss_clean($this->input->get('events'));
                                            $events_array = explode(",",$events);
                                            $size_events_array = count($events_array);
                                            $professional = 0;
                                            $guest = 0;
                                            $government= 0;
                                            $non_government= 0;
                                            $invite = 0;
                                            $details = [];
                                            //OBJECT
                                            foreach ($events_array as $_events_id) {
                                                $event = $this->Global->upcoming_events_byID($_events_id);
                                                $professional += $event->professional_price;
                                                $government += $event->government_price;
                                                $non_government += $event->non_government_price;
                                                $guest += $event->guest_price;
                                                $invite += $event->invite_guest_price; 
                                                array_push($details, array('id'=>$event->salesforce_id,'name' =>$event->Web_Event_Name__c, 'professional_' =>$event->professional_price,'guest_' =>$event->guest_price,'invite_' => $event->invite_guest_price,'government_' => $event->government_price, 'non_government_' => $event->non_government_price));

                                            }
                                            $details_json = json_encode($details);
                                    ?>
                                        <input type='hidden' name='checked_events' id='checked_events' value='<?php print_r($this->security->xss_clean($this->input->get('events'))); ?>'>
                                        <input type='hidden' name='page' id='page' value='<?php print_r($this->security->xss_clean($this->input->get('page'))); ?>'>
                                        <div class="hidden" id='member_page'>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6 " style="  margin-bottom: -14px;">
                                                <div class="field">
                                                    <label class="required label_field">
                                                        Email Address <b style="color:red">(Enter your registered email address) </b>
                                                    </label>
                                                    <input name="email_address" id="email_address" class='size_field' type="text" placeholder="" value="<?php echo isset($_GET['email']) ? htmlentities(urldecode($this->input->get('email'))) : (isset($user_info->company_email) ? $user_info->company_email :''); ?>" required="" aria-required="true">
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="margin-bottom: -14px;">
                                                <div class="field"> 
                                                    <label class='label_field required'>
                                                        Company
                                                    </label>
                                                    <input type="text" class="size_field" data-provide="typeahead" autocomplete='off' id="company" name="company" value="" />
                                                    <input type="hidden"  id="name_id" name="name_id" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <div class="col-md-6 " style="  margin-bottom: -14px;">
                                                <div class="field">
                                                    <label class="required label_field">
                                                        First Name
                                                    </label>
                                                    <input name="firstname" id="firstname" class='size_field' type="text" placeholder="" value="" required="" aria-required="true">
                                                </div>
                                            </div>
                                            <div class="col-md-6 " style="  margin-bottom: -14px;">
                                                <div class="field">
                                                    <label class="required label_field">
                                                        Last Name
                                                    </label>
                                                    <input name="lastname" id="lastname" class='size_field' type="text" placeholder="" value="" required="" aria-required="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6 " style="margin-bottom: -14px;">
                                                <div class="field">
                                                    <label class=" label_field required" >Title:</label>
                                                    <input name="title" id="title" class='size_field' type="text" placeholder="" value="" aria-required="true">
                                                </div>
                                            </div>
                                            <div class="col-md-6 " style="margin-bottom: -14px;">
                                                <div class="field">
                                                    <label class=" label_field required" >Phone Number:</label>
                                                    <input name="phone_number" id="phone_number" class='size_field' type="text" placeholder="" value="" aria-required="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6 " style="margin-bottom: -14px;">
                                                <div class="field">
                                                    <label class=" label_field required" >Mobile Number:</label>
                                                    <input name="mobile_number" id="mobile_number" class='size_field' type="text" placeholder="" value="" aria-required="true">
                                                </div>
                                            </div>
                                            <div class="col-md-6 " style="margin-bottom: -14px;">
                                                <div class="field">
                                                    <label class=" label_field" >How did you Hear About Us?</label>
                                                    <select name="hdyhaboutus" id="hdyhaboutus">
                                                        <option value="" selected> </option>
                                                        <option value="LinkedIn">LinkedIn</option>
                                                        <option value="Google">Google</option>
                                                        <option value="Colleague">Colleague</option>
                                                        <option value="POC Email">POC Email</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($this->input->get('page') == 'S') {?>
                                        <div class="col-md-12">
                                            <div class="col-md-6 " style="  margin-bottom: -14px;">
                                                <div class="field">
                                                    <label class=" label_field">
                                                    Promo code
                                                    </label>
                                                    <input name="promo_code" id="promo_code" class='size_field' type="text" placeholder="" aria-required="true" >
                                                    <input name='value__c' id='value__c' type='hidden'>
                                                    <input name='discount_type__c' id='discount_type__c' type='hidden'>
                                                    <input name='promo_code_id' id='promo_code_id' type='hidden'>
                                                    <input name="promo_for__c" id="promo_for__c" type="hidden" >
                                                    <input name="src_event" id="src_event" type="hidden" value="<?php echo $this->security->xss_clean($this->input->get('src'))?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <!-- BOOK SIGNING -->
                                        <?php if($book_signing == "TRUE"){ ?>
                                        <div class="" id='book_div'>
                                            <div class="col-md-12" style="color: black !important; padding-bottom: 10px;">
                                                <input type="checkbox" class="book_signing" id="book_signing" name="book_signing" style="  width: 15px;  padding: 6px;  margin-bottom: 7px; color: black !important"> 
                                                        <?php echo $book_desc; ?> - (<a href="<?php echo $book_link; ?>" target="_blank"> View More </a>)
                                                <input type="hidden" name="isBookSigning" id="isBookSigning" value="N">
                                            </div>
                                        </div>
                                        <!-- BOOK SIGNING -->
                                        <?php } ?>
                                        <div class="" id='bbg_div'>
                                            <div class="col-md-12">
                                                <button type='button' class="button ladda-button  " data-style="slide-up" id="bbg" style="padding:9px;width:100% !important"> 
                                                    <span class="ladda-label" style="color:white">
                                                        Click here to bring a Guest
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class=" hidden" id='bg_div' >
                                            <legend data-animate="fadeInDown" id='l_g' style="background: rgb(14, 147, 251);color: white;padding: 3px;margin-bottom: 1em;" >
                                                Guest Information - 
                                                <b id="r_g" style="cursor: pointer;">
                                                    [ Remove ]
                                                </b>
                                            </legend>
                                            <div class=" hidden" id='bg_' >
                                            </div>
                                        </div>
                                        <div class="hidden" id='_summary'>
                                            <div class="col-md-12" >
                                                <div class="field" style="  background: #FFFFFF;border: 1px solid #d0cdbc;padding: 15px;">
                                                    <input type='hidden' name='with_guest' id='with_guest' value='0'>
                                                    <input name="with_promo" id="with_promo" type="hidden" value="0">
                                                    <h6 class="label_field " id='status'>
                                                        Membership Status: GUEST
                                                    </h6>
                                                    <input type='hidden' name='mstatus' id='mstatus' value='GUEST'>
                                                    <h6 class="label_field text-center">
                                                        Summary of Charges
                                                    </h6>
                                                    <ul class="categories-posts list_events" id="list_events_">
                                                    </ul>
                                                    <div class='clear'></div>
                                                    <ul class="with-border1 categories-posts">
                                                        <li class="text-center">
                                                            <i class="di-separator">
                                                            </i>
                                                        </li>
                                                        <li>Total Payable:
                                                            <strong id="total_fees" style="float:right">
                                                                <span class="amt_p" id="amt_p">
                                                                    $0.00
                                                                </span>
                                                            </strong>
                                                            <input type="hidden" name="total_fees__c" id="total_fees__c" value="">
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" >
                                            <div class="col-md-6" id='zip_div' style="  margin-bottom: -14px;">
                                                <div class="field">
                                                    <label class="required label_field">
                                                        Please enter your credit card billing zip
                                                    </label>
                                                    <input name="b_zip" id="b_zip" class='size_field' type="text" placeholder="" value="" required="" aria-required="true">
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="float: right !important;padding-left: 25% !important;margin-bottom:2em;">
                                                <button type='submit' class="button ladda-button  fill" data-style="slide-up" id="sub_payment" > 
                                                    <span class="ladda-label" style="color:white">
                                                        Continue
                                                    </span>
                                                    <span class="ladda-spinner"></span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </form>
                                    <!--END of FORM-->
                                    
                                    <div class="post ">
                                        <div class="post-meta box" style="border-bottom:none;padding:0px;">
                                        </div>
                                        <div class="post-content box" style="margin-top: -2em;">
                                            <h5 style="line-height:1.3em;">
                                                POC Membership and Event Attendance Requirements:
                                            </h5>
                                            <div class="topaz-line full" style="margin:0;margin-top: -15px;">
                                                <i class="di-separator" style="color:black">
                                                </i>
                                            </div>
                                            <ul class="list-style angle">
                                                <li>
                                                    Individual requesting POC membership or event registration must hold or have recently held a director level or above position with a company*, federal government agency or non-profit organization.
                                                    <br>
                                                </li>
                                                <p style="color: black !important">(*Company must sell directly to the Federal Government, have a minimum annual revenue of $5 million and may not be a service provider)</p>
                                            </ul>
                                        </div>
                                    </div>
                                    <!--SEAL-->
                                    <div class='box_seal_r' style="border: 2px solid;height: 7em;margin-top: 2em;padding: 10px;width: 39%;">
                                        <div style="color: black;font-weight: bolder;margin-bottom: 5px;"> VERIFIED SERVICES </div>
                                        <div class="col-md-12 seal_div_12" style="">
                                            <div class="col-md-6 seal_div_6_trustwave_r" style="width: 55%;">
                                                <script type="text/javascript" src="https://sealserver.trustwave.com/seal.js?style=invert&code=368b082741a74b129a22614643e85c29"></script>
                                            </div>
                                            <div class="AuthorizeNetSeal col-md-6 seal_div_6_authorize_r" style="    width: 33%;margin-left: 10px;"> 
                                                <script type="text/javascript" language="javascript">var ANS_customer_id="d4af6c27-bfb6-448a-b555-07ad3df739f4";</script> 
                                                <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> 
                                                <!--<a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank">Credit Card Processing</a> -->
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END of SEAL-->
                                
                            <!--! POST -->
                        </div>

                        <div class="cl_4 top_" style="  margin-top: 0em ">
                            <section class="well" style="background-color: transparent !important; padding: 3px; border: 0px !important; padding-bottom: 8px;">
                            <?php if($this->input->get('page') == 'S'){ 
                                if(!$this->hexiros->logged_in()){?>
                                    <div class="hidden">
                                        <div class='hidden' id='upgrade_elite'>
                                            <?php $current_url = base_url($_SERVER['REQUEST_URI']);?>
                                            <a href="<?php echo base_url("../auth/signup?next=".$current_url);?>">                                                   
                                                <img src='<?php echo base_url('assets/images/register_banner_.jpg');?>' title='Click here to upgrade to elite membership' style='margin-bottom:1em'>    
                                            </a> 
                                        </div>
                                    </div>
                                 <?php } ?> 
                                <img src="<?php echo $pic;?>">
                            <?php }else{ 
                                if(!$this->hexiros->logged_in()){?>
                                    <div class="hidden">
                                        <div class='hidden' id='upgrade_elite'>
                                            <?php $current_url = base_url($_SERVER['REQUEST_URI']);?>
                                            <a href="<?php echo base_url('../auth/signup?next='.$current_url);?>">                                                   
                                                <img src='<?php echo base_url('assets/images/register_banner_.jpg');?>' title='Click here to upgrade to elite membership' style='margin-bottom:1em'>    
                                            </a>
                                        </div>
                                    </div>
                                <?php } ?>
                                <h4 style="line-height:1.3em; text-align:center; margin-top: 17px;">
                                    UPCOMING EVENTS
                                </h4>
                                    <?php $events = $this->Global->upcoming_events();
                                        foreach ($events->result() as $e) {
                                            echo '<img src="'.$e->EventBannerLinkShort__c.'" title="'.$e->Web_Event_Name__c.'" style="margin-top: 10px;">';
                                        }
                                    ?>
                                
                            <?php }?>
                            </section>
                            <?php if($this->input->get('page') == 'S'){?> 
                            <div class="post" style="margin-top:17px;" >
                                <div class="post-meta box">
                                    <h5>
                                        Event Details
                                    </h5>
                                </div>

                                <div class="post-content box">
                                    <ul class="with-border featured-posts">
                                        <li style="padding-bottom:0%;">
                                            <a>
                                                <h6 class="uppercase">
                                                    When
                                                </h6>
                                            </a>
                                            <p>
                                                <?php echo date('l, F j, Y',strtotime($date))?>
                                            </p>
                                            <p>
                                                <?php echo $dateStart."-".$dateEnd ?>
                                            </p>
                                            <p>
                                                Eastern Time Zone
                                            </p>
                                            <p>
                                                <div title="Add to Calendar" class="addthisevent" style=" border: 1px solid #FFFFFF; box-shadow: rgb(35, 82, 119) 0px 0px 1px 1px; background-color: #0079D7 !important; color:white !important; border-radius: 0px; margin-top:6px;">
                                                    Add to Calendar <i class="fa fa-caret-down"></i>
                                                    <span class="start">
                                                        <?php echo $date." ".$dateStart; ?>
                                                    </span>
                                                    <span class="end">
                                                        <?php echo $date." ".$dateEnd; ?>
                                                    </span>
                                                    <span class="timezone">
                                                        America/New_York
                                                    </span>
                                                    <span class="title">
                                                        <?php echo $name; ?> 
                                                    </span>
                                                    <span class="location">
                                                        <?php echo $addr2." ".$addr; ?>
                                                    </span>
                                                    <span class="organizer"></span>
                                                    <span class="organizer_email"></span>
                                                    <span class="facebook_event"></span>
                                                    <span class="all_day_event">
                                                        false
                                                    </span>
                                                    <span class="date_format">
                                                        MM/DD/YYYY
                                                    </span>
                                                </div>
                                            </p>
                                            <br>
                                            <button class="icon di_darckblue button fill" style="width:100%;display:none;">
                                                <i class="fa fa-calendar">
                                                </i> 
                                                Add To Calendar
                                            </button>
                                        </li>
                                        <li style="padding-top:0%;">
                                            <a>
                                                <h6 class="uppercase">
                                                    WHERE
                                                </h6>
                                            </a>
                                            <p>
                                                <?php echo $addr1 ?>
                                            </p>
                                            <p>
                                                <?php echo $addr2 ?>
                                            </p>
                                            <p>
                                                <?php echo $addr ?>
                                            </p>
                                            <p>
                                                <?php echo $phone ?>
                                            </p>
                                            <p>
                                                <form action="https://maps.google.com/maps" method="get" target="_blank">
                                                    <p>
                                                        <p>
                                                        </p>
                                                        <input type="text" name="saddr" id="mapsUsSaddr" value="" size="20"  value="<?php echo $addr2.', '.$addr.', USA';?>" style="display:none;">
                                                        <button class="icon button fill" style="margin-top:6px; border: 1px solid #FFFFFF; box-shadow: 0px 0px 1px 1px #0C202F;background: #0079D7;color: white;width: auto; height: 66%;">
                                                            <i class="di-map-marker ">
                                                            </i>
                                                            Get Map
                                                        </button>
                                                        <input type="hidden" name="daddr" value="<?php echo $addr2.', '.$addr.', USA';?>" />
                                                        <input type="hidden" name="hl" value="en" />
                                                    </p>
                                                </form>
                                            </p>
                                            <br>
                                            <div>
                                                <button class="icon di_darckblue button fill" style="width:100%;display:none;">
                                                    <i class="fa fa-compass">
                                                    </i>
                                                    Get Driving Directions
                                                </button>
                                                <br/>
                                                <button class="icon di_darckblue button fill" style="width:100%;display:none;">
                                                    <i class="fa fa-map-maker">
                                                    </i>
                                                    Get Map
                                                </button>
                                                <br/>
                                                <button class="icon di_darckblue button fill" style="width:100%;display:none;">
                                                    <i class="fa fa-sun-o">
                                                    </i>
                                                    Get Weather
                                                </button>
                                            </div>
                                        </li>
                                    </ul>   
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php if($this->input->get('page') == 'S'){?> 
                        <div class="cl_4" style="float: right">
                         <!-- POST -->
                            <div class="post ">
                                <div class="" style="border-bottom:none;padding:0px;">
                                </div>

                                <div class="post-content box" style="background-color:white;">
                                    <h5 style="line-height:1.3em;">
                                        Sponsored By
                                    </h5>
                                    <div class="topaz-line full" style="margin:0;">
                                        <i class="di-separator" style="color:black"></i>
                                    </div>
                                    <ul>
                                    <?php
                                        if(!empty($event_sponsors)){
                                            foreach ($event_sponsors->result() as $ev) {
                                            ?>
                                                <p>
                                                    <?php 
                                                        $sponsor_website = $ev->sponsor_company_website;
                                                    ?>
                                                    <li>
                                                        <a href="<?php echo "".$sponsor_website.""; ?>" target="_blank"> 
                                                            <img src="<?php echo $ev->attachment_url;?>" style="width: <?php echo $ev->banner_size; ?>; margin-bottom: 0px;">
                                                        </a>
                                                        <?php
                                                            $sponsor_types = explode(" ", $ev->sponsor_type);
                                                            $s_type = "";
                                                            if($sponsor_types[1] == "Platinum"){
                                                                $s_type = base_url('assets/images/sponsor_types/platinum1.png'); 
                                                            }else if($sponsor_types[1] == "Gold"){
                                                                $s_type = base_url('assets/images/sponsor_types/gold1.jpg'); 
                                                            }else if($sponsor_types[1] == "Silver"){
                                                                $s_type = base_url('assets/images/sponsor_types/silver1.jpg'); 
                                                            }
                                                        ?>
                                                        <img src="<?php echo $s_type; ?>" style="width: 51px;" alt="M">
                                                    </li>
                                                </p>
                                            <?php
                                            }//end of loop   
                                        }
                                    ?>
                                    </ul>


                                </div>
                            </div>
                        </div>
                   
                        <div class="cl_4" style="display:none;">
                            <div class="post" style="margin-top:17px;">
                                <div class="post-meta box">
                                    <h5>
                                        Event Details
                                    </h5>
                                </div>

                                <div class="post-content box">
                                    <ul class="with-border featured-posts">
                                        <li>
                                            <a href="#">
                                                <h6 class="uppercase">
                                                    When
                                                </h6>
                                            </a>
                                            <p>
                                                <?php echo date('l, F j, Y',strtotime($date))?>
                                            </p>
                                            <p>
                                                <?php echo $dateStart."-".$dateEnd ?>
                                            </p>
                                            <p>
                                                Eastern Time Zone
                                            </p>
                                            <p>
                                                <div title="Add to Calendar" class="addthisevent" style="border: 1px solid #FFFFFF; box-shadow: 0px 0px 1px 1px #0079D7;">
                                                    Add to Calendar
                                                    <span class="start">
                                                        <?php echo $date." ".$dateStart; ?>
                                                    </span>
                                                    <span class="end">
                                                        <?php echo $date." ".$dateEnd; ?>
                                                    </span>
                                                    <span class="timezone">
                                                        America/New_York
                                                    </span>
                                                    <span class="title">
                                                        <?php echo $name; ?> 
                                                    </span>
                                                    <span class="location">
                                                        <?php echo $addr2." ".$addr; ?>
                                                    </span>
                                                    <span class="organizer"></span>
                                                    <span class="organizer_email"></span>
                                                    <span class="facebook_event"></span>
                                                    <span class="all_day_event">
                                                        false
                                                    </span>
                                                    <span class="date_format">
                                                        MM/DD/YYYY 
                                                    </span>
                                                </div>
                                            </p>
                                            <br>
                                            <button class="icon di_darckblue button fill" style="width:100%;display:none;">
                                                <i class="fa fa-calendar">
                                                </i> 
                                                Add To Calendar
                                            </button>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <h6 class="uppercase">WHERE</h6>
                                            </a>
                                            <p>
                                                <?php echo $addr1 ?>
                                            </p>
                                            <p>
                                                <?php echo $addr2 ?>
                                            </p>
                                            <p>
                                                <?php echo $addr ?>
                                            </p>
                                            <p>
                                                <?php echo $phone ?>
                                            </p>
                                            <br>
                                            <div>
                                                <button class="icon di_darckblue button fill" style="width:100%;display:none;">
                                                    <i class="fa fa-compass"></i>
                                                    Get Driving Directions
                                                </button>
                                                <br/>
                                                <button class="icon di_darckblue button fill" style="width:100%;display:none;">
                                                    <i class="fa fa-map-maker"></i>
                                                    Get Map
                                                </button>
                                                <br/>
                                                <button class="icon di_darckblue button fill" style="width:100%;display:none;">
                                                    <i class="fa fa-sun-o"></i>
                                                    Get Weather
                                                </button>
                                            </div>
                                        </li>
                                    </ul>   
                                </div>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </section>
            <!--! SECTION -->
        </div>
        <!--! ALL CONTENTS -->

        <!-- FOOTER -->       
        <!-- BOTTOM FOOTER -->
        <footer role="contentinfo" class="dima-footer">
            <div class="container">
                <!-- COPYRIGHT -->
                <div class="flexbox di_1_of_3 first">
                    <div class="copyright">
                        <p>
                            © <?php echo date('Y'); ?> by Executive Mosaic. All rights reserved
                        </p>
                    </div>
                </div>
                <!--! COPYRIGHT -->
            </div>
        </footer>
        <!--! BOTTOM FOOTER -->

        <!-- AddThisEvent Settings -->
        <script type="text/javascript">
        addthisevent.settings({
            license    : "00000000000000000000",
            mouse      : false,
            css        : true,
            outlook    : {show:true, text:"Outlook"},
            google     : {show:true, text:"Google <em>(online)</em>"},
            yahoo      : {show:true, text:"Yahoo <em>(online)</em>"},
            outlookcom : {show:true, text:"Outlook.com <em>(online)</em>"},
            appleical  : {show:true, text:"Apple Calendar"},
            facebook   : {show:true, text:"Facebook Event"},
            dropdown   : {order:"outlook,google,appleical,yahoo"},
            callback   : ""
        });
        </script>
    <script src="<?php echo base_url('assets/js/core/jquery.easing.1.3.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/core/modernizr-2.8.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/core/imagesloaded.pkgd.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/core/respond.src.js'); ?>"></script>

    <script src="<?php echo base_url('assets/js/libs.min.js'); ?>"></script>
   

    <script src="<?php echo base_url('assets/js/specific/mediaelement/mediaelement-and-player.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/specific/video.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/specific/bigvideo.js'); ?>"></script>

   
    <script src="<?php echo base_url('assets/js/specific/gmap3.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/map.js'); ?>"></script>

    <script type="text/javascript" src="<?php echo base_url('assets/js/specific/revolution-slider/js/jquery.themepunch.tools.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/specific/revolution-slider/js/jquery.themepunch.revolution.min.js'); ?>"></script>
    

    <script src="<?php echo base_url('assets/js/main_new.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-printme.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootbox.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bootstrap-typeahead/bootstrap3-typeahead.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bootstrap-typeahead/bootstrap3-typeahead.js'); ?>"></script>
    <script src="<?php echo base_url('assets/ladda/ladda.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/app/custom.js'); ?>"></script>              
    <script src="<?php echo base_url('assets/js/event_page.js');?>"></script>

<script>
    var professional = "<?php echo $professional; ?>";
    var government = "<?php echo $government; ?>";
    var non_government = "<?php echo $non_government; ?>";
    var guest = "<?php echo $guest; ?>";
    var invite = "<?php echo $invite; ?>";
    var book_price = "<?php echo $book_price; ?>"
    var membership_type = '';
    var event_details = '<?php print_r($details_json); ?>';
    var total_ = 0;
    fixed_register_now_btn_event();
    //for register now button on mobile on scroll down
    $(window).scroll(function(){ 
        fixed_register_now_btn_event();
    });

    function fixed_register_now_btn_event(){
        var a = 106;
        var pos = $(window).scrollTop();
        if(pos > a) {

            $(".register_nowBtn_fixed_on_mobile_div").css({
                        position: 'fixed',
                        padding: '0px'
            });
        }
        else if(pos == 0){
            $(".register_nowBtn_fixed_on_mobile_div").css({
                        position: 'relative',
                        padding: '4px 3px 0px 3px'
            });
        }
    }

    $.each($.parseJSON(event_details), function(idx, obj) {
        total_ += parseInt(obj.guest_);
        $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10'>✔ "+obj.name+"</div><div class='pull-right'>$"+obj.guest_+"</div></li>");
        $('.amt_p').text("$"+total_);
        $('#total_fees__c').val(total_);
        $('#b_zip').prop('disabled',false);
    });

   /* window.onload = function () {
        $.ajax({
                url: "../ajax/unsupported_browser",
                type: 'POST',
                success: function(output) {
                    var json= JSON.parse(output);
                    console.log(json);
                        if(json.status === "redirect_"){
                            window.location.href = "http://www.potomacofficersclub.com/unsupported_browser"
                        }
                }
        });
    }*/

    $(document).ready(function(){

        Ladda.bind('.ladda-button');
        /****
            Company Typeahead function
        ****/

        if($('#page').val() == 'R'){
            var url_company='../auth/getCompany';
        }
        else{
            var url_company= '../../auth/getCompany';   
        }
        $('#promo_code').focus();
        var company___ = <?php echo json_encode($company__); ?>;
        var jsonObj = $.parseJSON(company___);
        $("[name=company]").typeahead({
            hint: true,                          
            source: jsonObj,
            displayField: 'name',
            valueField: 'ID',
            updater: function(item) {
                $('#name_id').val(item.ID);
                return item;
            }
           
            
        }); 
        
        /* --END--*/


        if($("#email_address").val() != null)
        {
            if($('#page').val() == 'R'){
                var url='../ajax/emailcheck_?events='+$('#checked_events').val();
            }
            else{
                var url= '../../ajax/emailcheck_?events='+$('#checked_events').val();   
            }

            $('#email_address').trigger('blur');
        }
    
        $("#promo_code").blur(function()
        {
             if( !this.value)
            {
                $('#value__c').val('');
                $('#discount_type__c').val('');
                $('#promo_code_id').val('');
                $('#with_promo').val('0');
                $('li.l_e_p').remove();
                if($('#mstatus').val() == 'GUEST'){
                    $(".amt_p").text("$"+guest);
                    $('#total_fees__c').val(guest);
                    $('#b_zip').prop('disabled',false);
                }else if($('#mstatus').val() == "PROFESSIONAL"){
                    $(".amt_p").text("$"+professional);
                    $('#total_fees__c').val(professional);
                    $('#b_zip').prop('disabled',false);
                }
            }

        });

    });


/***************************************************************
    Load user details
***************************************************************/
    function load_details(email,verified){
            if($('#page').val() == 'R'){
                var url='../ajax/load_details';
            }
            else{
                var url= '../../ajax/load_details';   
            }
            $.ajax({
                url: url,
                type: 'POST',
                data: 'email='+email,
                success: function(response){
                    var details = JSON.parse(response);
                    var total = 0;
                    $("#firstname").val(details.firstname);
                    $("#lastname").val(details.lastname);
                    $("#title").val(details.title);
                    $("#phone_number").val(details.phone_c);
                    $("#mobile_number").val(details.mobile_phone);
                    $('#company').val(details.company);
                    $('#amt').removeClass('hidden');
                    /*if(verified == 0){
                        membership_type = 'GUEST';
                    }else{*/
                        //date expiry condition
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth()+1; //January is 0!
                        var yyyy = today.getFullYear();

                        if(dd<10) {
                            dd='0'+dd
                        } 

                        if(mm<10) {
                            mm='0'+mm
                        } 

                        today = yyyy+'-'+mm+'-'+dd;
                        if(details.membership_type == 'ELITE' || details.membership_type == 'PROFESSIONAL' || details.membership_type == 'GOVERNMENT'){
                            if(details.date_expire <= today ){
                                membership_type = 'GUEST';
                            }else{
                                membership_type = details.membership_type;   
                            }
                        }else{
                            var email_user = $('#email_address').val(); 
                            var email_user_length = email_user.length;
                            var email_nonmembergov = email_user.substring(email_user_length,(email_user_length-3));
                            var canada_email = email_user.indexOf("gc.ca");
                            if(email_nonmembergov == 'mil' || email_nonmembergov == 'gov' || canada_email != -1){
                                membership_type = 'NON MEMBER GOVERNMENT';
                            }else{
                                membership_type = 'GUEST';
                            }
                        }
                    $.each($.parseJSON(event_details), function(idx, obj) {
                        if(membership_type == 'PROFESSIONAL' || membership_type == 'GOVERNMENT' || membership_type == 'GUEST' || membership_type == 'NON MEMBER GOVERNMENT' ){
                            if(membership_type == 'PROFESSIONAL'){
                                $("#amt_").text("$"+professional);
                                price = obj.professional_;
                            }else if(membership_type == 'GOVERNMENT'){
                                $("#amt_").text("$"+government);
                                price = obj.government_;
                            }else if(membership_type == 'NON MEMBER GOVERNMENT'){
                                $("#amt_").text("$"+non_government);
                                price = obj.non_government_;
                            }else if(membership_type == 'GUEST'){
                                $("#amt_").text("$"+guest);
                                price = obj.guest_;
                            }

                            if(parseInt(details.events_id.indexOf(obj.id)) == -1)
                            {
                                total += parseInt(price);
                                $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10'>✔ "+obj.name+"</div><div class='pull-right'>$"+price+"</div></li>");
                            }
                            else{
                                $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' style='text-decoration: line-through;'>✔ "+obj.name+"</div><div class='pull-right' style='text-decoration: line-through;'>$"+price+"</div></li>");
                            }
                        }
                        else{
                            if(parseInt(details.events_id.indexOf(obj.id)) == -1)
                            {
                                $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10'>✔ "+obj.name+"</div><div class='pull-right'>$0</div></li>");
                            }
                            else{
                                $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' style='text-decoration: line-through;'>✔ "+obj.name+"</div><div class='pull-right' style='text-decoration: line-through;'>$0</div></li>");
                            }
                        }
                    });
                    if(membership_type == 'PROFESSIONAL' || membership_type == 'GOVERNMENT' || membership_type == 'GUEST' || membership_type == 'NON MEMBER GOVERNMENT'){
                        $('#mstatus').val(membership_type);
                        $('.amt_p').text("$"+total);
                        $('#total_fees__c').val(total);
                        $('#zip_div').removeClass('hidden');
                        $('#upgrade_elite').removeClass('hidden');
                        $('#member_page').html("<input type='hidden' name='basic_page' id='basic_page' value=''>");
                        $('#promo_code').prop('disabled',false);
                    }else{
                        $('#mstatus').val('E&E');
                        $(".amt_p").text("$0");
                        $('#total_fees__c').val('0');
                        $('#upgrade_elite').addClass('hidden');
                        $('#zip_div').addClass('hidden');
                        $('#member_page').html("<input type='hidden' name='payment_page' id='payment_page' value=''>");
                        $('#promo_code').prop('disabled',true);
                    }
                    $('#status').text('Membership Status: '+ membership_type);
                }
            });
    }

/***************************************************************
    Load promo code datails
****************************************************************/
    function load_promocode(events,promo_code){
            if($('#page').val() == 'R'){
                var url='../ajax/load_promocode';
            }
            else{
                var url= '../../ajax/load_promocode';   
            }
            $.ajax({
                url: url,
                type: 'POST',
                data: 'events='+events+'&promo_code='+promo_code,
                success: function(response){
                    var details = JSON.parse(response);
                    var total = 0;

                    $('#value__c').val(details.Value__c);
                    $('#discount_type__c').val(details.Discount_Type__c);
                    $('#promo_code_id').val(details.Promo_code_id);
                    $('#promo_for__c').val(details.Promo_For__c);
                    $('#with_promo').val('1');
                    $("li.l_e").remove();
                    $("li.l_e_g").remove();
                    $("li.l_e_p").remove();

                    if(details.Discount_Type__c == "Fixed Amount"){
                        list_events_promo($('#mstatus').val(),'FA',details.Promo_For__c);
                    }else if(details.Discount_Type__c == "Discount Amount"){
                        list_events_promo($('#mstatus').val(),'DA',details.Promo_For__c);
                    }else if(details.Discount_Type__c == "Discount Percentage"){
                        list_events_promo($('#mstatus').val(),'DP',details.Promo_For__c);
                    }
                }
            }); // end of AJAX
    }

/*************************************************************
    URL for ajax validation
*************************************************************/
    if($('#page').val() == 'R'){
        var url='../ajax/emailcheck_?events='+$('#checked_events').val();
        var url_promocode='../ajax/check_promocode?events='+$('#checked_events').val();
    }
    else{
        var url= '../../ajax/emailcheck_?events='+$('#checked_events').val();   
        var url_promocode='../../ajax/check_promocode?events='+$('#checked_events').val();
    }

/*************************************************************
    List events
*************************************************************/
    function list_events(){
        var total__ = 0;
        var price = 0;
        var email_user = $('#email_address').val(); 
        var email_user_length = email_user.length;
        var email_nonmembergov = email_user.substring(email_user_length,(email_user_length-3));
        var canada_email = email_user.indexOf("gc.ca");
        $.each($.parseJSON(event_details), function(idx, obj) {
            if(email_nonmembergov == 'mil' || email_nonmembergov == 'gov' || canada_email != -1){
                membership_type = 'NON MEMBER GOVERNMENT';
                total__ += parseInt(obj.non_government_);
                price = obj.non_government_;
            }else{
                membership_type = 'GUEST';
                total__ += parseInt(obj.guest_);
                price = obj.guest_;
            }
            $('#status').text('Membership Status: '+membership_type);
            $('#mstatus').val(membership_type);
            $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10'>✔ "+obj.name+"</div><div class='pull-right'>$"+price+"</div></li>");
                
        })

        $('#zip_div').removeClass('hidden');
        $('#upgrade_elite').removeClass('hidden');
        $('#member_page').html("<input type='hidden' name='basic_page' id='basic_page' value=''>");
        $('.amt_p').text("$"+total__);
        $('#total_fees__c').val(total__);
        $('#b_zip').prop('disabled',false);
    }

/************************************************************
    Form Validation
************************************************************/
    $("#register_guest").validate({

        /* @validation states + elements 
        ------------------------------------------- */
        invalidHandler: function(form, validator){
            Ladda.stopAll();
        },
        errorClass: "error3",
        validClass: "success",
        errorElement: "em",

        /* @validation rules 
        ------------------------------------------ */

        rules: {
            email_address: {
                required:true,
                email: true,
                remote: {
                    url: url,
                    complete: function(x){
                        
                    },
                    dataFilter: function(data){
                        var json= JSON.parse(data);
                        // bootbox.dialog({message: "<img src='public/img/ajax-loader.gif'/> please wait..."});
                        if(json.status === "success"){
                            
                            $('#_summary').removeClass('hidden');
                            $("#firstname").val("");
                            $("#lastname").val("");
                            $('#amt').removeClass('hidden');
                            $("#amt_").text("$"+guest);
                            $("li.l_e").remove();
                            $("li.l_e_p").remove();
                            $("li.l_e_g").remove();
                            
                            list_events();
                            $('#promo_code').prop('disabled',false);
                            $('#promo_code').val('');
                                
                               return '"true"';
                        }

                        if(json.status === "register"){
                            bootbox.alert(json.error, function() {
                              window.location.href = "summary/"+$('#checked_events').val();
                            });
                            
                            return '"true"';
                        }
                        else if(json.status === "error"){
                            $("li.l_e").remove();
                            $("li.l_e_p").remove();
                            $("li.l_e_g").remove();
                            $('#promo_code').val('');
                            $('#_summary').removeClass('hidden');
                           
                            bootbox.hideAll();
                            bootbox.dialog({
                                    size: 'small',
                                    message: json.error,
                                    title: "",
                                    buttons: {
                                        Yes: {
                                        label: "<span id='yes' class='ladda-label' style='color:white'>Yes</span><span class='ladda-spinner'></span>",
                                        className: "btn-primary ladda-button",
                                            callback: function() {
                                                load_details($("#email_address").val(),json.verified);
                                            }
                                        },
                                    No:{
                                        label: "<span id='yes' class='ladda-label' style='color:white'>No</span><span class='ladda-spinner'></span>",
                                        className: "btn-primary ladda-button",
                                            callback: function() {
                                                if($("#page").val() == 'S')
                                                {
                                                    window.location.href = "../event/register_page?events="+$('#checked_events').val()+"&page=S";
                                                }
                                                else{
                                                    window.location.href = "../event/register_page?events="+$('#checked_events').val()+"&page=R";   
                                                }
                                            }
                                    }
                                    }
                            });

                            
                            return '"true"';
                        }else if(json.status === "unverified"){
                            $("li.l_e").remove();
                            $("li.l_e_p").remove();
                            $("li.l_e_g").remove();
                            $('#promo_code').val('');
                            $('#_summary').removeClass('hidden');
                           
                            bootbox.hideAll();
                            bootbox.dialog({
                                    size: 'medium',
                                    message: json.error,
                                    title: "",
                                    buttons: {
                                        Yes: {
                                        label: "<span id='yes' class='ladda-label' style='color:white'>Confirm</span><span class='ladda-spinner'></span>",
                                        className: "btn-primary ladda-button",
                                            callback: function() {
                                                $.ajax({
                                                    url: '../../ajax/login_user',
                                                    type: 'POST',
                                                    data: 'email_address='+$("#email_address").val(),
                                                    success: function(response){
                                                        var json= JSON.parse(response);
                                                        window.location.href = json.error; 
                                                    }
                                                }); // end of AJAX
                                               
                                            }
                                        },
                                    }
                            });

                            
                            return '"true"';
                        }else
                            return '"'+json.error+'"';
                    }

                }
            },
            firstname: {
                required:true
            },
            lastname: {
                required:true
            },
            firstname_g: {
                required:true
            },
            lastname_g: {
                required:true
            },
            title_g: {
                required:true
            },
            title: {
                required: true
            },
            phone_number: {
                required: true
            },
            mobile_number: {
                required: true,
                /*remote: {
                    url: '../ajax/saveinfo?events='+$('#checked_events').val(),
                    type: "get",
                    data: {
                      email: function() {
                        return $( "#email_address" ).val();
                      },
                      phone_number: function() {
                        return $( "#phone_number" ).val();
                      }
                    },
                    complete: function(x){
                        
                    },
                    dataFilter: function(data){
                        var json= JSON.parse(data);
                        if(json.status === "success"){
                            return '"true"';
                        }else
                        {
                            return '"'+json.error+'"';
                        }
                    }
                }*/
            },
            email_address_g:{
                required:true,
                email:true
            },
            company:{
                required: true
            },
            b_zip: {
                required:true,
                number: true,
                minlength:4
            },
            promo_code: {
                remote: {
                    url: url_promocode,
                    complete: function(x){
                        
                    },
                    dataFilter: function(data){
                        var json= JSON.parse(data);
                        if(json.status === "success"){
                            $("li.l_e").remove();
                            $("li.l_e_p").remove();
                            $("li.l_e_g").remove();
                            load_promocode($("#checked_events").val(),$('#promo_code').val());
                            return '"true"';
                        }else
                        {
                            $("li.l_e").remove();
                            $("li.l_e_p").remove();
                            $("li.l_e_g").remove();
                            $('#value__c').val('');
                            $('#discount_type__c').val('');
                            $('#promo_code_id').val('');
                            $('#promo_for__c').val('');
                            $('#with_promo').val('0');
                            var total_p=0;
                            var price = 0;
                            $.each($.parseJSON(event_details), function(idx, obj) {
                                if($('#mstatus').val() == "PROFESSIONAL"){
                                    total_p += parseInt(obj.professional_);
                                    price = obj.professional_;
                                }else if($('#mstatus').val() == 'GUEST'){
                                    total_p += parseInt(obj.guest_);
                                    price = obj.guest_;
                                }else if($('#mstatus').val() == 'ELITE'){
                                    total_p = 0;
                                    price = 0;
                                }else if($('#mstatus').val() == "GOVERNMENT"){
                                    total_p += parseInt(obj.government_);
                                    price = obj.government_;
                                }
                                else if($('#mstatus').val() == "NON MEMBER GOVERNMENT"){
                                    total_p += parseInt(obj.non_government_);
                                    price = obj.non_government_;
                                }
                                $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' >✔ "+obj.name+"</div><div class='pull-right' >$"+price+"</div></li>");
                                $('.amt_p').val(total_p);
                            });
                            
                            return '"'+json.error+'"';
                        }

                    }

                }
            }
            
          },

        /* @validation error messages 
        ---------------------------------------------- */

        messages: {
           
        },

        /* @validation highlighting + error placement  
        ---------------------------------------------------- */

        highlight: function(element, errorClass, validClass) {
            $(element).closest('.field').addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.field').removeClass(errorClass).addClass(validClass);
        },
        errorPlacement: function(error, element) {
            if (element.is(":radio") || element.is(":checkbox")) {
                console.log(error);
              element.closest('.option-group').after(error);
            } else {
              error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
                form.submit();
             
        
        }
    });

/**************************************************************
    List of events promo code (<li></li>)
**************************************************************/
    function list_events_promo(mstatus,discount_type,promo_for__c)
    {   
        var price =0;
        var totalxpromo = 0;
        $.each($.parseJSON(event_details), function(idx, obj) {
            if(mstatus == 'GUEST'){
                price = obj.guest_;
            }else if(mstatus == 'PROFESSIONAL'){
                price = obj.professional_;
            }else if(mstatus == 'GOVERNMENT'){
                price = obj.government_;
            }else if(mstatus == 'NON MEMBER GOVERNMENT'){
                price = obj.non_government_;
            }
            if(discount_type == 'FA'){
                    if(promo_for__c == 'registrant')
                    {
                        totalxpromo = parseInt($('#value__c').val());
                        $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' >✔ "+obj.name+"</div><div class='pull-right' style='text-decoration: line-through;'>$"+price+"</div></li>");
                        $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+")</div><div class='pull-right'>$"+$('#value__c').val()+"</div></li>");
                    }else if(promo_for__c == 'bring guest'){ 
                        totalxpromo = parseInt(price);
                        $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' >✔ "+obj.name+"</div><div class='pull-right'>$"+price+"</div></li>");
                    }else if(promo_for__c == 'total amount'){
                        totalxpromo = parseInt($('#value__c').val());
                        $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' >✔ "+obj.name+"</div><div class='pull-right'>$"+price+"</div></li>");
                        $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+" for Total Payable)</div><div class='pull-right'>$"+$('#value__c').val()+"</div></li>");
                    }
            }else if(discount_type == 'DA'){
                    if(promo_for__c == 'registrant'){
                        totalxpromo = parseInt(price) - parseInt($('#value__c').val());
                        $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' >✔ "+obj.name+"</div><div class='pull-right' >$"+price+"</div></li>");
                        $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+")</div><div class='pull-right'>($"+$('#value__c').val()+")</div></li>");
                    }else if(promo_for__c == 'bring guest'){
                        totalxpromo = parseInt(price);
                        $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' >✔ "+obj.name+"</div><div class='pull-right'>$"+price+"</div></li>");
                    }else if(promo_for__c == 'total amount'){
                        totalxpromo = parseInt(price) - parseInt($('#value__c').val());
                        $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' >✔ "+obj.name+"</div><div class='pull-right'>$"+price+"</div></li>");
                        $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+" for Total Payable)</div><div class='pull-right'>($"+$('#value__c').val()+")</div></li>");
                    }
            }else if(discount_type == 'DP'){
                    if(promo_for__c == 'registrant'){
                        totalxpromo = parseInt(price) - (parseInt(price) * (parseFloat($('#value__c').val()) / 100));
                        $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' >✔ "+obj.name+"</div><div class='pull-right' >$"+price+"</div></li>");
                        $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+")</div><div class='pull-right'>("+$('#value__c').val()+"%)</div></li>");
                    }else if(promo_for__c == 'bring guest'){
                        totalxpromo = parseInt(price);
                        $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' >✔ "+obj.name+"</div><div class='pull-right'>$"+price+"</div></li>");
                    }else if(promo_for__c == 'total amount'){
                        totalxpromo = parseInt(price) - (parseInt(price) * (parseFloat($('#value__c').val()) / 100));
                        $("#list_events_").append("<li class='l_e' id='Event_n'"+obj.id+"''><div class='col-md-10' >✔ "+obj.name+"</div><div class='pull-right'>$"+price+"</div></li>");
                        $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+" for Total Payable)</div><div class='pull-right'>("+$('#value__c').val()+"%)</div></li>");
                    }
            }
            $(".amt_p").text("$"+totalxpromo);
            $('#total_fees__c').val(totalxpromo);
            if(parseInt($('span.amt_p').text().substring(1)) == 0){
                $('#member_page').html("<input type='hidden' name='payment_page' id='payment_page' value=''>");
                $('#b_zip').prop('disabled',true);
            }else{
                $('#member_page').html("<input type='hidden' name='basic_page' id='basic_page' value=''>");
                $('#b_zip').prop('disabled',false);   
            }
        });
    }

/*****************************************************************
    When user click the button "Click here to bring a guest"
*****************************************************************/
    $("#bbg").on( "click", function(e) {
        e.preventDefault();
        $('#bbg_div').addClass('hidden');
        $('#bg_').removeClass('hidden');
        $('#bg_div').removeClass('hidden');
        $('#with_guest').val('1');
        var price = 0;
        var price_guest = 0;
        if(membership_type == 'ELITE' || membership_type == 'EMPLOYEE'){
            $(".amt_p").text("$"+parseInt(invite));
            $('#total_fees__c').val(invite);
            $("#list_events_").append("<li class='l_e_g' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Guest)</div><div class='pull-right'>$"+invite+"</div></li>");
            $('#zip_div').removeClass('hidden');
        }else{
            if(membership_type == 'PROFESSIONAL'){
                price = professional;
                price_guest = invite;
            }else if(membership_type == 'GOVERNMENT'){
                price = government;
                price_guest = invite;
            }else if(membership_type == 'NON MEMBER GOVERNMENT'){
                price = non_government;
                price_guest = invite;
            }else if(membership_type == 'GUEST'){
                price = guest;
                price_guest = guest;
            }
            
            if($('#with_promo').val() == 1)
            {
                if($('#discount_type__c').val() == 'Fixed Amount')
                {
                    list_events_promo_guest(membership_type,'FA',$('#promo_for__c').val());
                }else if($('#discount_type__c').val() == 'Discount Percentage')
                {
                    list_events_promo_guest(membership_type,'DP',$('#promo_for__c').val());
                }else if($('#discount_type__c').val() == 'Discount Amount')
                {
                    list_events_promo_guest(membership_type,'DA',$('#promo_for__c').val());
                }
            }
            else{
                $("li.l_e_b").remove();
                $("#list_events_").append("<li class='l_e_g' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Guest)</div><div class='pull-right'>$"+price_guest+"</div></li>");
                if(!$(".book_signing").is(':checked')){
                    var fee = parseInt(price)+ parseInt(price_guest);
                }else{
                    $("#list_events_").append("<li class='l_e_b' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Book)</div><div class='pull-right'>$"+book_price+"</div></li>");
                    var fee = parseInt(price)+ parseInt(price_guest) + parseInt(book_price);
                }
                $(".amt_p").text("$"+fee);
                $('#total_fees__c').val(fee);
                $('#b_zip').prop('disabled',false);
            }
        }

        $('#member_page').html("<input type='hidden' name='basic_page' id='basic_page' value=''>");
        $('#bg_').html('<div class="col-md-6 " style="  margin-bottom: -1em;">'+
                            '<div class="field">'+
                            '<label class="label_field required">Email Address:</label>'+
                                '<input type="text" class="size_field" id="email_address_g" name="email_address_g"  value="" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6 " style="  margin-bottom: -1em;">'+
                            '<div class="field">'+
                            '<label class="label_field required">Firstname:</label>'+
                                '<input type="text" class="size_field" id="firstname_g" name="firstname_g"  value="" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6 " style="  margin-bottom: -1em;">'+
                            '<div class="field">'+
                            '<label class="label_field required">Lastname:</label>'+
                                '<input type="text" class="size_field" id="lastname_g" name="lastname_g"  value="" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6 " style="  margin-bottom: -1em;">'+
                            '<div class="field">'+
                            '<label class="label_field required">Title:</label>'+
                                '<input type="text" class="size_field" id="title_g" name="title_g"  value="" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6" style="margin-bottom: -14px;">'+
                            '<div class="field"> '+
                                '<label class="label_field required">'+
                                    'Company'+
                                '</label>'+
                                '<input type="text" class="size_field" data-provide="typeahead" autocomplete="off" id="company_g" name="company_g" value="" />'+
                                '<input type="hidden"  id="name_id_g" name="name_id_g" value="" />'+
                            '</div>'+
                        '</div>'
        );
        var company___ = <?php echo json_encode($company__); ?>;
        var jsonObj = $.parseJSON(company___);
        $("[name=company_g]").typeahead({
            hint: true,                          
            source: jsonObj,
            displayField: 'name',
            valueField: 'ID',
            updater: function(item) {
                $('#name_id_g').val(item.ID);
                return item;
            }
           
            
        });  
    });

/*************************************************************
    List of events promo with guest (<li></li>)
**************************************************************/
    function list_events_promo_guest(mstatus,discount_type,promo_for__c)
    {   
        var price =0;
        var totalxpromo = 0;
        var fee = 0;
        var invitexpromo =0;
        $.each($.parseJSON(event_details), function(idx, obj) {
            if(mstatus == 'GUEST'){
                price = obj.guest_;
                invitexpromo = obj.guest_;
            }else if(mstatus == 'PROFESSIONAL'){
                price = obj.professional_;
                invitexpromo = obj.invite_;
            }else if(mstatus == 'GOVERNMENT'){
                price = obj.government_;
                invitexpromo = obj.invite_;
            }else if(mstatus == 'NON MEBER GOVERNMENT'){
                price = obj.non_government_;
                invitexpromo = obj.invite_;
            }

            if(discount_type == 'FA'){
                if(promo_for__c == 'registrant')
                {
                    fee = parseInt($('#value__c').val())+ parseInt(invitexpromo);
                    $("#list_events_").append("<li class='l_e_g' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Guest)</div><div class='pull-right'>$"+invitexpromo+"</div></li>");
                }else if(promo_for__c == 'bring guest'){
                    fee = parseInt(price) + parseInt($('#value__c').val());
                    $("#list_events_").append("<li class='l_e_g' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Guest)</div><div class='pull-right' style='text-decoration: line-through;'>$"+invitexpromo+"</div></li>");
                    $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+" for Guest )</div><div class='pull-right'>$"+$('#value__c').val()+"</div></li>");
                }else if(promo_for__c == 'total amount'){
                    $("li.l_e_p").remove();
                    $("#list_events_").append("<li class='l_e_g' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Guest)</div><div class='pull-right'>$"+invitexpromo+"</div></li>");
                    fee = parseInt($('#value__c').val());
                    $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+" for Total Payable )</div><div class='pull-right'>($"+$('#value__c').val()+")</div></li>");
                }
            }else if(discount_type == 'DA'){
                if(promo_for__c == 'registrant')
                {
                    fee = ( parseInt(price)  - parseInt($('#value__c').val())) + parseInt(invitexpromo);
                    $("#list_events_").append("<li class='l_e_g' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Guest)</div><div class='pull-right'>$"+invitexpromo+"</div></li>");
                }else if(promo_for__c == 'bring guest'){
                    fee = parseInt(price) + ( parseInt(invitexpromo) - parseInt($('#value__c').val()));
                    $("#list_events_").append("<li class='l_e_g' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Guest)</div><div class='pull-right' >$"+invitexpromo+"</div></li>");
                    $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+" for Guest )</div><div class='pull-right'>($"+$('#value__c').val()+")</div></li>");
                }else if(promo_for__c == 'total amount'){
                    $("li.l_e_p").remove();
                    $("#list_events_").append("<li class='l_e_g' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Guest)</div><div class='pull-right'>$"+invitexpromo+"</div></li>");
                    fee = ( parseInt(price) + parseInt(invitexpromo) ) - parseInt($('#value__c').val());
                    $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+" for Total Payable )</div><div class='pull-right'>($"+$('#value__c').val()+")</div></li>");
                }
            }else if(discount_type == 'DP'){
                    if(promo_for__c == 'registrant')
                    {
                        fee = ( parseInt(price) - (parseInt(price) * (parseFloat($('#value__c').val()) / 100))) + parseInt(invitexpromo);
                        $("#list_events_").append("<li class='l_e_g' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Guest)</div><div class='pull-right'>$"+invitexpromo+"</div></li>");
                    }else if(promo_for__c == 'bring guest'){
                        fee = parseInt(price) + ( parseInt(invitexpromo) - (parseInt(invitexpromo) * (parseFloat($('#value__c').val()) / 100)));
                        $("#list_events_").append("<li class='l_e_g' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Guest)</div><div class='pull-right' >$"+invitexpromo+"</div></li>");
                        $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+" for Guest )</div><div class='pull-right'>("+$('#value__c').val()+"%)</div></li>");
                    }else if(promo_for__c == 'total amount'){
                        $("li.l_e_p").remove();
                        $("#list_events_").append("<li class='l_e_g' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Guest)</div><div class='pull-right'>$"+invitexpromo+"</div></li>");
                        fee = ( parseInt(price) + parseInt(invitexpromo) ) -  ( ( parseInt(price) + parseInt(invitexpromo) ) * (parseFloat($('#value__c').val()) / 100));
                        $("#list_events_").append("<li class='l_e_p' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Promo Code Type: "+$('#discount_type__c').val()+" for Total Payable )</div><div class='pull-right'>("+$('#value__c').val()+"%)</div></li>");
                    }
            }

            $(".amt_p").text("$"+fee);
            $('#total_fees__c').val(fee);
            if(parseInt($('span.amt_p').text().substring(1)) == 0){
                $('#member_page').html("<input type='hidden' name='payment_page' id='payment_page' value=''>");
                $('#b_zip').prop('disabled',true);
            }else{
                $('#member_page').html("<input type='hidden' name='basic_page' id='basic_page' value=''>");
                $('#b_zip').prop('disabled',false);   
            }
        });
    }

/*****************************************************************
    When user click the "remove" link
*****************************************************************/
    $('#r_g').on( "click", function(e) {
        e.preventDefault();
        $('#bbg_div').removeClass('hidden');
        $('#bg_').addClass('hidden');
        $('#bg_div').addClass('hidden');
        $('#with_guest').val('0');
        $('.l_e_g').remove();
        $('.l_e_b').remove();
        $('.l_e').remove();
        $("li.l_e_p").remove();
        var totalxpromoxwoG =0;
        var price = 0;
        if(membership_type == 'ELITE' || membership_type == 'EMPLOYEE'){
            $(".amt_p").text("$0");
            $('#total_fees__c').val('0');
            $('#zip_div').addClass('hidden');
            $('#member_page').html("<input type='hidden' name='payment_page' id='payment_page' value=''>");
        }else{
            if(membership_type == 'PROFESSIONAL'){
                price = professional;
            }else if(membership_type == 'GOVERNMENT'){
                price = government;
            }else if(membership_type == 'NON MEMBER GOVERNMENT'){
                price = non_government;
            }else if(membership_type == 'GUEST'){
                price = guest;
            }
            if($('#with_promo').val() == 1){
                if($('#discount_type__c').val() == 'Fixed Amount'){
                    list_events_promo(membership_type,'FA',$('#promo_for__c').val());
                }else if($('#discount_type__c').val() == "Discount Amount"){
                    list_events_promo(membership_type,'DA',$('#promo_for__c').val());
                }else if($('#discount_type__c').val() == "Discount Percentage"){
                    list_events_promo(membership_type,'DP',$('#promo_for__c').val());
                }
            }else{
                if(!$(".book_signing").is(':checked')){
                    list_events();
                }else{
                    list_events();
                    $("#list_events_").append("<li class='l_e_b' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Book)</div><div class='pull-right'>$"+book_price+"</div></li>");
                    var fee = parseInt($('#total_fees__c').val()) + parseInt(book_price);
                    $('.amt_p').text("$"+fee);
                    $('#total_fees__c').val(fee);
                }
                
                
            }
        }
    });

/*****************************************************************
    Book Signing
*****************************************************************/
    $(document).on('click','.book_signing', function(){
        if(!$(this).is(':checked')){
            $('#isBookSigning').val('N');
            $("li.l_e_b").remove();
            var fee = parseInt($('#total_fees__c').val()) - parseInt(book_price);
            $('.amt_p').text("$"+fee);
            $('#total_fees__c').val(fee);
        }else{
            $('#isBookSigning').val('Y');
            $("#list_events_").append("<li class='l_e_b' ><div class='col-md-10'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Addional Fee for Book)</div><div class='pull-right'>$"+book_price+"</div></li>");
            var fee = parseInt($('#total_fees__c').val()) + parseInt(book_price);
            $('.amt_p').text("$"+fee);
            $('#total_fees__c').val(fee);
        }
    })
</script>
</body>

</html>