function sendLink(username){
    $.ajax({
        url: '../../ajax/sendSetuplink',
        type: 'POST',
        data: 'username='+username,
        success: function(response){

            //var json= JSON.parse(response);
            bootbox.hideAll();
            //bootbox.alert(json.error);
            url = "";
            $(location).attr("href", url);
            return '"true"';
        }
    });
}

/************************************************************
    Form Validation
************************************************************/
    $("#signin").validate({

        /* @validation states + elements 
        ------------------------------------------- */
        invalidHandler: function(form, validator){
        },
        errorClass: "error3",
        validClass: "success",
        errorElement: "em",

        /* @validation rules 
        ------------------------------------------ */

        rules: {
            username: {
                required: true,
                remote: {
                    url: '../../ajax/usernamecheck',
                    complete: function(x){
                        
                    },
                    dataFilter: function(data){
                        var json= JSON.parse(data);
                        if(json.status === "success"){
                            return '"true"';
                        }

                        if(json.status === "error"){
                            bootbox.dialog({
                              message: json.error,
                              buttons: {
                                success: {
                                  label: "<span class='ladda-label'>Send me an email</span>",
                                  className: "btn-primary ladda-button",
                                  callback: function() {
                                    sendLink($("#username").val());
                                    return false;

                                  }
                                }
                              }
                            });
                            Ladda.bind('.ladda-button');

                            var btns = $(".ladda-button");

                            for(var z = 0; z < btns.length; z++){
                                if(!btns[z].hasAttribute('data-style')){
                                    btns[z].setAttribute('data-style','expand-right');
                                }
                            }

                            return '"true"';
                        }else
                            return '"'+json.error+'"';
                    }
                }
            },
            password: {
                required: true
            }
        },

        /* @validation error messages 
        ---------------------------------------------- */

        messages: {
            username: {
              required: 'This field is required'
            },
            password: {
              required: 'This field is required'
            }
        },

        /* @validation highlighting + error placement  
        ---------------------------------------------------- */

        highlight: function(element, errorClass, validClass) {
            $(element).closest('.field').addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.field').removeClass(errorClass).addClass(validClass);
        },
        errorPlacement: function(error, element) {
            if (element.is(":radio") || element.is(":checkbox")) {
                console.log(error);
              element.closest('.option-group').after(error);
            } else {
             // error.insertAfter(element);
                element.after(error).after('<br>');
            }
        },
        submitHandler: function(form) {
                form.submit();
             
        
        }
    });
/***********************************************************
    End - Form Validation
***********************************************************/