<?php
?>
<!DOCTYPE html>
<html>
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
  <link rel="stylesheet" type="text/css" data-them="" href="cssv2/bootstrap.min.css">
  <script src="js/core/jquery-2.1.1.min.js"></script>
  <link rel="stylesheet" data-them="" href="cssv2/styles-green.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="cssv2/custom.css">
  <!-- CUSTOM CSS -->
  <title>SalesRobot Campaign</title>
  <meta name="description" content="">
  <link rel="stylesheet" data-them="" href="cssv2/mass_email.css">
  <link rel="stylesheet" data-them="" href="cssv2/spectrum.css">
  <link rel="stylesheet" data-them="" href="bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css">


  <script src="https://cdn.tinymce.com/4.8/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea',visual : false,
      toolbar: "code | undo redo | subscript superscript | fontselect fontsizeselect | bold italic underline | forecolor | alignleft aligncenter alignright alignjustify | bullist numlist | link image |",
      plugins: [
        "advlist autolink lists link image imagetools charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste",
        "textcolor colorpicker",
        "image imagetools",
        "noneditable"
      ] });</script>
</head>

<body class="responsive " id="demo-green">

<!-- LOADING -->
<div class="all_content">
  <div class="dima-main" style="float:none;">
    <section>
      <div class="col-md-12 ">
        <div class="col-md-2" style="padding: 30px 10px 10px 10px;">
          <h2 class="hidden"></h2>
          <h3 class="hidden"></h3>
          <h4 class="hidden"></h4>

          <h5 style="text-align: left;color: #828077 !important;">ELEMENTS</h5>
          <div style="padding: 0px 28px 0px 28px;">
            <table width="100%">
              <tr>
                <td colspan="3" align="center">
                  Basic
                  <div class="clear"></div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="div-element draggable-text" id="drag-text"
                       style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                    <img class="text-element-img" src="icons/text.png" alt="text icon element">
                  </div>
                </td>
                <td>
                  &nbsp;&nbsp;&nbsp;
                </td>
                <td>
                  <div class="div-element draggable-img" id="drag-img"
                       style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                    <img class="img-element-img" src="icons/img.png" alt="img icon element">
                  </div>
                  <br>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="div-element draggable-left" id="drag-left"
                       style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                    <img class="left-element-img" src="icons/left.png" alt="left icon element">
                  </div>
                </td>
                <td>
                  &nbsp;&nbsp;&nbsp;
                </td>
                <td>
                  <div class="div-element draggable-right" id="drag-right"
                       style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                    <img class="right-element-img" src="icons/right.png" alt="right icon element">
                  </div>
                  <br>
                </td>
              </tr>
            </table>
          </div>
          <div class="clear"></div>
          <div style="padding: 0px 28px 0px 28px">
            <table width="100%">
              <tr>
                <td colspan="3" align="center">
                  Structure
                  <div class="clear"></div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="div-element draggable-divider" id="drag-divider"
                       style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                    <img class="divider-element-img" src="icons/divider.png" alt="divider icon element">
                  </div>
                </td>
                <td>
                  &nbsp;&nbsp;&nbsp;
                </td>
                <td>
                  <div class="div-element draggable-btn" id="drag-btn"
                       style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                    <img class="btn-element-img" src="icons/btn.png" alt="btn icon element">
                  </div>
                  <br>
                </td>
              </tr>
            </table>
          </div>
          <br>
          <br>
        </div>
        <div class="col-md-9" id="<?php echo rand(0, 1000000000000); ?>">
          <div style="margin-top: 2em;border: 1px solid #5BC0DE;box-shadow: 0px 1px 3px 0px #888888;background: #5BC0DE;padding: 12px 10px 12px 10px;color: white;border-radius: 8px 8px 0px 0px;" id="<?php echo rand(0, 1000000000000); ?>">
            <nav role="navigation" class="clearfix">
              <!-- menu content -->
              <ul class="dima-menu sf-menu">
                <li class="sub-icon" style="font-size: 18px;float: left;padding: 10px;">
<!--                  kim-->
                  <p style="padding: 0;margin: 0;float: left;" class="my-title">Untitled Mailing</p><a id="edit-title" onclick="title_dialog(this)" data-toggle="modal" style="color: black;margin-left: 5px;"><i class="fa fa-pencil"></i></a>
                </li>
              </ul>
              <!-- !menu content -->
            </nav>
          </div>
          <div class="col-md-12"
               style="border: 1px solid #D0CDBC;background: #ddd;box-shadow: 0px 1px 3px 0px #888888;height: auto;">
            <div class="col-md-10 col-md-offset-1"
                 style="background: white;margin-top: 1em; margin-bottom: 1em;padding-top:10px; padding-bottom: 10px;">

              <div class="poc_img_bg poc_imgtag undrop-poc" id="<?php echo rand(0, 1000000000000); ?>"
                   style="width: auto;height: 135px;">
                <div class="poc_outline " id="<?php echo rand(0, 1000000000000); ?>">
                  <ul class="dima-menu sf-menu" style="float: right;" id="ul-action">
                    <li style="display: flex" class="actions-icon" id="li-action">
                      <a onclick="img_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-img"
                         style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i
                                class="fa fa-pencil" style="font-size: 20px;"></i></a>
                      <a onclick="delete_element(this)" class="a-icon-del"
                         style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i
                                class="fa fa-trash" style="font-size: 20px;"></i></a>
                    </li>
                  </ul>
                </div>
                <div class="poc_img_placeholder" id="poc_img_placeholder-id" style="width: auto;height: 135px;">
                  <img id="profile_picture_img_" class="hidden" src="" alt="banner"
                       style="width: 586px;height: 135px;"/>
                </div>
              </div>

              <div class="modal fade img-modal" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Replace Image (586px * 135px)</h4>
                    </div>
                    <div class="modal-body">
                      <form role="form" id="uploadMassEmail" method="post" enctype="multipart/form-data">
                        <div>
                          <input type="hidden" name="div-id-img" id="div-id-img" value="">
                          <input type='file' accept='image/*' name="profile_pic" class="profile_pic"
                                 style="margin-bottom: 7px;"/>
                          <img class="hover hidden pp-img" alt="image" style="width:586px;height:135px;"/>
                        </div>
                        <div class="hidden img_loader" style="text-align:center">
                          <p>Please wait</p>
                          <img src="icons/loader.gif" style="height: 2em;width: 24em;">
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="submit_btn text-center btn btn-success" id="update_pic">Update
                          </button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal fade title-modal" role="dialog">
              <div class="modal-dialog modal-sm">

                <!-- kim Modal content-->
                <div class="modal-content" style="width: 900px;">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">My Title</h4>
                  </div>
                  <div class="modal-body">
                    <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                    <div class="clear"></div>
                    <div class="">
                      <div class="accordion">
                        <div class="accordion-section">
                          <div class="accordion-section-title">
                            <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                              <tbody>
                              <tr>
                                <td style="width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                  Text
                                </td>
                                <td style="width:70%;border: none !important;">
                                  <div>
                                    <input type="text" name="btn_text" class="btn_text-title" style="background: white !important">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td rowspan="2" style="width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                  <div>
                                    <textarea>ew</textarea>
                                  </div>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                  </div>
                </div>
              </div>
            </div>

            <div class="modal fade btn-modal" role="dialog">
              <div class="modal-dialog modal-sm">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Button</h4>
                  </div>
                  <div class="modal-body">
                    <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                    <div class="clear"></div>
                    <div class="">
                      <div class="accordion">
                        <div class="accordion-section">
                          <div class="accordion-section-title">
                            <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                              <tbody>
                              <tr>
                                <td style="width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                  Text
                                </td>
                                <td style="width:70%;border: none !important;">
                                  <div>
                                    <input type="text" name="btn_text" class="btn_text" style="background: white !important">
                                  </div>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div class="accordion-section">
                          <div class="accordion-section-title">
                            <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                              <tbody>
                              <tr>
                                <td style="width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                  Link
                                </td>
                                <td style="width:70%;border: none !important;">
                                  <div>
                                    <input type="text" name="src_btn" class="src-btn" style="background: white !important">
                                  </div>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div class="accordion-section">
                          <a class="accordion-section-title" href="#accordion-1">Spacing<span style="float: right;"><i class="fa fa-chevron-down"></i></span></a>
                          <div id="accordion-1" class="accordion-section-content">
                            <div class="" style="">
                              <table width="100%">
                                <tbody>
                                <tr>
                                  <td style="width:50%">
                                    Top Margin
                                  </td>
                                  <td style="width:50%" align="center">
                                    <div class="input-group" style="width: 70%;">
                                      <input type="text" class="top-margin-btn" name="top-margin-btn" placeholder="" size="5px">
                                      <span class="input-group-addon">PX</span>
                                    </div>
                                    <br>
                                  </td>
                                </tr>
                                <tr>
                                  <td style="width:50%">
                                    Bottom Margin
                                  </td>
                                  <td style="width:50%" align="center">
                                    <div class="input-group" style="width: 70%;">
                                      <input type="text" class="bottom-margin-btn" name="bottom-margin-btn" placeholder="" size="5px">
                                      <span class="input-group-addon">PX</span>
                                    </div>
                                    <br>
                                  </td>
                                </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div class="accordion-section">
                          <a class="accordion-section-title" href="#accordion-3">Style<span style="float: right;"><i class="fa fa-chevron-down"></i></a>
                          <div id="accordion-3" class="accordion-section-content">
                            <div class="" style="">
                              <table width="100%">
                                <tbody>
                                <tr>
                                  <td style="width:50%">
                                    Border
                                  </td>
                                  <td style="width:50%" align="center">
                                    <div class="input-group" style="width: 70%;">
                                      <input type="text" class="border-btn" placeholder="" size="5px">
                                      <span class="input-group-addon">PX</span>
                                    </div>
                                    <br>
                                  </td>
                                </tr>
                                <tr>
                                  <td style="width:50%">
                                    Border Color
                                  </td>
                                  <td style="width:50%" align="center">
                                    <input type="text" class="border-color-btn">
                                    <br>
                                    <br>
                                  </td>
                                </tr>
                                <tr>
                                  <td style="width:50%">
                                    Text Color
                                  </td>
                                  <td style="width:50%" align="center">
                                    <input type="text" class="text-color-btn">
                                    <br>
                                    <br>
                                  </td>
                                </tr>
                                <tr>
                                  <td style="width:50%">
                                    Background Color
                                  </td>
                                  <td style="width:50%" align="center">
                                    <input type="text" class="background-color-btn">
                                    <br>
                                    <br>
                                  </td>
                                </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                  </div>
                </div>
              </div>
            </div>

            <div class="draggableContainer  ui-droppable" id="738894811832">
              <div class="poc-drop-zone-text" style="display:none;">
                <span class="poc-drop-here">Drop Here</span>
              </div>
            </div>

          </div>
        </div>


      </div>
    </section>
  </div>
</div>



<script src="js/core/jquery.easing.1.3.js"></script>
<script src="js/core/modernizr-2.8.2.min.js"></script>
<script src="js/core/imagesloaded.pkgd.min.js"></script>
<script src="js/core/respond.src.js"></script>
<script src="js/libs.min.js"></script>
<script src="js/specific/mediaelement/mediaelement-and-player.min.js"></script>
<script src="js/specific/video.js"></script>
<script src="js/specific/bigvideo.js"></script>
<script src="js/specific/gmap3.min.js"></script>
<script src="js/map.js"></script>
<script type="text/javascript" src="js/specific/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="js/specific/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/main.js"></script>
<script src="bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="jquery-ui/jquery-ui.js"></script>
<script src="js/spectrum.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
    $("[name='my-checkbox']").bootstrapSwitch();

    $(".src-img").on('blur', function () {

      if ($("#src-img-" + $('#div-id-img_').val()).val() == "") {
        $('a#a-src-img' + $('#div-id-img_').val()).contents().unwrap();
      } else {
        if ($('a#a-src-img' + $('#div-id-img_').val()).length) {
          $('#a-src-img' + $('#div-id-img_').val()).attr("href", $("#src-img-" + $('#div-id-img_').val()).val());
        } else {
          $('#' + $('#div-id-img_').val()).find("img#profile_picture_img_").wrap("<a href='" + $("#src-img-" + $('#div-id-img_').val()).val() + "' id='a-src-img" + $('#div-id-img_').val() + "' ></a>")
        }
      }
    });

    $(".btn_text").on('blur', function () {

      if ($("#txt-btn-" + $('#div-id-btn_').val()).val() === "") {
        $('#' + $('#div-id-btn_').val()).find("button.btn-element").text('');
      } else {
        $('#' + $('#div-id-btn_').val()).find("button.btn-element").text($("#txt-btn-" + $('#div-id-btn_').val()).val());
      }
    });

    // kim
    $(".btn_text-title").on('blur', function() {
      if ($("#txt-btn-" + $('#div-id-btn_').val()).val() === "") {
        $('#' + $('#div-id-btn_').val()).find("button.btn-element").text('');
      } else {
        document.getElementById("my-title-" + $('#div-id-btn_').val()).innerHTML = $("#txt-btn-" + $('#div-id-btn_').val()).val();
      }
    });

    $(".src-btn").on('blur', function () {

      if ($("#src-btn-" + $('#div-id-btn_').val()).val() == "") {
        $('a#a-src-btn' + $('#div-id-btn_').val()).contents().unwrap();
      } else {
        if ($('a#a-src-btn' + $('#div-id-btn_').val()).length) {
          $('#a-src-btn' + $('#div-id-btn_').val()).attr("href", $("#src-btn-" + $('#div-id-btn_').val()).val());
        } else {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").wrap("<a href='" + $("#src-btn-" + $('#div-id-btn_').val()).val() + "' id='a-src-btn" + $('#div-id-btn_').val() + "' ></a>")
        }
      }
    });

    $(".border-btn").on('blur', function () {
      if ($("#border-btn-" + $('#div-id-btn_').val()).val() == "") {
        $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('border');
      } else {
        $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('border', $("#border-btn-" + $('#div-id-btn_').val()).val() + 'px solid ' + $("#border-color-btn-" + $('#div-id-btn_').val()).val());
      }
    });

    $(".top-margin-btn").on('blur', function () {

      if ($("#top-margin-btn-" + $('#div-id-btn_').val()).val() == "") {
        $('#' + $('#div-id-btn_').val()).css("margin-top", "");
      } else {
        $('#' + $('#div-id-btn_').val()).css("margin-top", $("#top-margin-btn-" + $('#div-id-btn_').val()).val() + 'px');
      }
    });

    $(".bottom-margin-btn").on('blur', function () {

      if ($("#bottom-margin-btn-" + $('#div-id-btn_').val()).val() == "") {
        $('#' + $('#div-id-btn_').val()).css("margin-bottom", "");
      } else {
        $('#' + $('#div-id-btn_').val()).css("margin-bottom", $("#bottom-margin-btn-" + $('#div-id-btn_').val()).val() + 'px');
      }
    });

    drop_element();
  });
  //drag codes
  $('.draggable-btn').draggable({
    revert: function (socketObj) {
      if (socketObj === false) {
        return true;
      } else {
        return true;
      }
    },
    stack: ".draggable-btn"
  });
  $('.draggable-divider').draggable({
    revert: function (socketObj) {
      if (socketObj === false) {
        return true;
      } else {
        return true;
      }
    },
    stack: ".draggable-divider"
  });
  $('.draggable-text').draggable({
    revert: function (socketObj) {
      if (socketObj === false) {
        return true;
      } else {
        return true;
      }
    },
    stack: ".draggable-text"
  });
  $('.draggable-img').draggable({
    revert: function (socketObj) {
      if (socketObj === false) {
        return true;
      } else {
        return true;
      }
    },
    stack: ".draggable-img"
  });
  $('.draggable-left').draggable({
    revert: function (socketObj) {
      if (socketObj === false) {
        return true;
      } else {
        return true;
      }
    },
    stack: ".draggable-left"
  });
  $('.draggable-right').draggable({
    revert: function (socketObj) {
      if (socketObj === false) {
        return true;
      } else {
        return true;
      }
    },
    stack: ".draggable-right"
  });

  //end of drag codes
  function drop_element () {
    $(".draggableContainer").droppable({
      activeClass: "poc-active",
      hoverClass: "poc-drop-zone-hover",
      drop: function (event, ui) {
        if (ui.draggable.attr('id') == "drag-text") {
          $('<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div>' +
             '</div><div class="poc-texttag" id="<?php echo rand(0, 1000000000000); ?>"><p>Insert text here</p><div class="poc_outline" id="<?php echo rand(0, 1000000000000); ?>">' +
             '<ul class="dima-menu sf-menu" style="float: right;">' +
             '<li style="display: flex;" class="actions-icon">' +
             '<a onclick="" class="a-icon-edit" data-toggle="modal" id="edit-txt" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>' +
             '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>' +
             '</li>' +
             '</ul>' +
             '</div></div>' +
             '<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div></div>')
             .insertBefore(this);
          $(this).remove();
          drop_element();
        } else if (ui.draggable.attr('id') == "drag-img") {
          $('<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div></div>' +
             '<div class="poc_img_bg poc_imgtag undrop-poc" id="<?php echo rand(0, 1000000000000); ?>" style="width: auto;height: 135px;">' +
             '<div class="poc_outline" id="<?php echo rand(0, 1000000000000); ?>">' +
             '<ul class="dima-menu sf-menu" style="float: right;" id="ul-action">' +
             '<li style="display: flex" class="actions-icon" id="li-action">' +
             '<a onclick="img_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-img" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>' +
             '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>' +
             '</li>' +
             '</ul>' +
             '</div>' +
             '<div class="poc_img_placeholder" id="poc_img_placeholder-id" style="width: auto;height: 135px;">' +
             '<img id="profile_picture_img_" class="hidden" src="" alt="banner" style="width: 586px;height: 135px;"/>' +
             '</div>' +
             '</div>' +
             '<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div></div>')
             .insertBefore(this);
          $(this).remove();
          drop_element();
        } else if (ui.draggable.attr('id') == "drag-btn") {
          $('<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div></div>' +
             '<div class="poc-btntag" id="<?php echo rand(0, 1000000000000); ?>">' +
             '<button class="button-block btn-element" style="cursor: pointer; text-decoration: none; line-height: 110%; border: 1px solid; padding: 12px 18px; display: block; margin-left: auto; margin-right: auto; border-radius: 6px; font-family: Arial; color: rgb(255, 255, 255); text-align: center; font-size: 16px; font-weight: normal; font-style: normal; background: rgb(90, 195, 232);">Button</button>' +
             '<div class="poc_outline" id="<?php echo rand(0, 1000000000000); ?>">' +
             '<ul class="dima-menu sf-menu" style="float: right;">' +
             '<li style="display: flex;" class="actions-icon">' +
             '<a onclick="btn_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-btn" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>' +
             '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>' +
             '</li>' +
             '</ul>' +
             '</div>' +
             '</div>' +
             '<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div></div>')
             .insertBefore(this);
          $(this).remove();
          drop_element();
        } else if (ui.draggable.attr('id') == "drag-left") {
          $('<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div></div>' +
             '<div class="poc-combo-block poc-content-block we" id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-content-block-outline" id="<?php echo rand(0, 1000000000000); ?>">' +
             '<ul class="dima-menu sf-menu" style="float: right;">' +
             '<li style="display: flex;" class="actions-icon">' +
             '<a class="a-icon-edit" data-toggle="modal" id="edit-left" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>' +
             '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>' +
             '</li>' +
             '</ul>' +
             '</div>' +
             '<div class="poc-content-preview text-wrap image-position-left" style="margin: 5px; padding: 0px; border: 0px solid rgb(0, 0, 0); width: auto; background-color: transparent;">' +
             '<div class="poc-combo-block-imageHolder" style="display: block;">' +
             '<div class=" poc_img_bg" id="poc_img_bg-id" style="width: 220px;height: 150px;">' +
             '<div class="poc_img_placeholder" id="poc_img_placeholder-id" style="width: 220px;height: 150px;">' +
             '</div>' +
             '</div>' +
             '</div>' +
             '<div class="poc-combo-block-content" data-key="textholder" style="display: block;">' +
             '<div class="text-content poc-content-editable aloha-editable aloha-block-blocklevel-sortable ui-sortable" data-key="content" spellcheck="false" id="0d1b0ac0-221b-0c8f-88d4-c68cbdcc8c47" contenteditable="true">' +
             '<p>' +
             'Insert text here Insert text here Insert text hereInsert text hereInsert text hereInsert text here <br>' +
             'Insert text here Insert text here Insert text hereInsert text hereInsert text hereInsert text here ' +
             'Insert text here Insert text here Insert text hereInsert text hereInsert text hereInsert text here <br><br>' +
             'Insert text here Insert text here Insert text hereInsert text hereInsert text hereInsert text here ' +
             'Insert text here Insert text here Insert text hereInsert text hereInsert text hereInsert text here <br>' +
             '</p>' +
             '</div>' +
             '</div>' +
             '</div>' +
             '</div>' +
             '<div class="clear" style="padding-bottom: 0px !important;"></div>' +
             '<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div></div>')
             .insertBefore(this);
          $(this).remove();
          drop_element();
        } else if (ui.draggable.attr('id') == "drag-right") {
          $('<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div></div>' +
             '<div class="poc-combo-block poc-content-block we" id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-content-block-outline" id="<?php echo rand(0, 1000000000000); ?>">' +
             '<ul class="dima-menu sf-menu" style="float: right;">' +
             '<li style="display: flex;" class="actions-icon">' +
             '<a onclick="right_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-right" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>' +
             '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>' +
             '</li>' +
             '</ul>' +
             '</div>' +
             '<div class="poc-content-preview text-wrap image-position-right" style="margin: 5px; padding: 0px; border: 0px solid rgb(0, 0, 0); width: auto; background-color: transparent;">' +
             '<div class="poc-combo-block-imageHolder" style="display: block;">' +
             '<div class=" poc_img_bg" id="poc_img_bg-id" style="width: 220px;height: 150px;">' +
             '<div class="poc_img_placeholder" id="poc_img_placeholder-id" style="width: 220px;height: 150px;">' +
             '</div>' +
             '</div>' +
             '</div>' +
             '<div class="poc-combo-block-content" data-key="textholder" style="display: block;">' +
             '<div class="text-content poc-content-editable aloha-editable aloha-block-blocklevel-sortable ui-sortable" data-key="content" spellcheck="false" id="0d1b0ac0-221b-0c8f-88d4-c68cbdcc8c47" contenteditable="true">' +
             '<p>' +
             'Insert text here Insert text here Insert text hereInsert text hereInsert text hereInsert text here <br>' +
             'Insert text here Insert text here Insert text hereInsert text hereInsert text hereInsert text here ' +
             'Insert text here Insert text here Insert text hereInsert text hereInsert text hereInsert text here <br><br>' +
             'Insert text here Insert text here Insert text hereInsert text hereInsert text hereInsert text here ' +
             'Insert text here Insert text here <br>' +
             '</p>' +
             '</div>' +
             '</div>' +
             '</div>' +
             '</div>' +
             '<div class="clear" style="padding-bottom: 0px !important;"></div>' +
             '<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div></div>')
             .insertBefore(this);
          $(this).remove();
          drop_element();
        } else if (ui.draggable.attr('id') == "drag-divider") {
          $('<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div></div>' +
             '<div class="poc-dividertag" id="<?php echo rand(0, 1000000000000); ?>" >' +
             '<div style="height: 20px; overflow: hidden; width: 70%;"></div>' +
             '<hr class="styled-hr" style="width:70%;margin-bottom: 0px !important;margin-top: 0px !important;margin: auto;">' +
             '<div style="height: 20px; overflow: hidden; width: 70%;"></div>' +
             '<div class="poc_outline" id="<?php echo rand(0, 1000000000000); ?>">' +
             '<ul class="dima-menu sf-menu" style="float: right;">' +
             '<li style="display: flex;" class="actions-icon">' +
             '<a class="a-icon-edit" data-toggle="modal" id="edit-div" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>' +
             '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>' +
             '</li>' +
             '</ul>' +
             '</div>' +
             '</div>' +
             '<div class="draggableContainer " id="<?php echo rand(0, 1000000000000); ?>">' +
             '<div class="poc-drop-zone-text" style="display:none;">' +
             '<span class="poc-drop-here">Drop Here</span>' +
             '</div></div>')
             .insertBefore(this);
          $(this).remove();
          drop_element();
        }
      }
    });
  }

  //for profile picture select image preview
  function readURL (input) {
    if (input.files && input.files[0]) {
      let reader = new FileReader();

      reader.onload = function (e) {
        $('#pp-img-' + $('#div-id-img').val()).removeClass('hidden');
        $('#pp-img-' + $('#div-id-img').val()).attr('src', e.target.result);
      };

      reader.readAsDataURL(input.files[0]);
    }
  }

  function img_dialog (element) {
    $('div.img_loader').addClass('hidden');
    let div_id_ = $(element).closest("div").attr("id");
    let div_id = $("div#" + div_id_).parent("div").attr("id");
    $('.src-img').val('');
    if ($('#' + div_id).find("img#profile_picture_img_").attr('src') == '') {
      $('.img-modal').attr("id", "img-" + div_id);
      $(".modal-body #div-id-img").val(div_id);
      $(".modal-body .pp-img").attr("id", "pp-img-" + div_id);
      $(".modal-body .profile_pic").attr("id", "divpp-" + div_id);
      $('#' + div_id).find("a#edit-img").attr("data-target", "#img-" + div_id);

    } else {
      $('.img-modal-p').attr("id", "img-p" + div_id);
      $(".modal-body #div-id-img_").val(div_id);
      $(".modal-body .replace_img").attr("data-target", "#img-" + div_id);
      $(".src-img").attr("id", "src-img-" + div_id);
      if ($('a#a-src-img' + $('#div-id-img_').val()).length) {
        $('#src-img-' + div_id).val($('#a-src-img' + div_id).attr('href'));
      }
      $('#' + div_id).find("a#edit-img").attr("data-target", "#img-p" + div_id);
      $(".border-color-img").spectrum({
        allowEmpty: true,
        change: function (color) {
          alert(color.toHexString());
        }
      });
    }
  }

  // kim
  function title_dialog(element) {
    let div_id_ = $(element).closest("div").attr("id");
    let div_id = $("div#" + div_id_).parent("div").attr("id");
    $('.title-modal').attr("id", "title-" + div_id);
    $('.my-title').attr("id", "my-title-" + div_id);
    $(".modal-body #div-id-btn_").val(div_id);

    $(".btn_text-title").attr("id", "txt-btn-" + div_id);
    $('#' + div_id).find("a#edit-title").attr("data-target", "#title-" + div_id);

    $('#txt-btn-' + div_id).val(document.getElementById("my-title-" + div_id).innerHTML);
  }

  function btn_dialog (element) {
    let div_id_ = $(element).closest("div").attr("id");
    let div_id = $("div#" + div_id_).parent("div").attr("id");
    $('.btn_text').val('');
    $('.src-btn').val('');
    $('.btn-modal').attr("id", "btn" + div_id);
    $(".modal-body #div-id-btn_").val(div_id);
    $(".btn_text").attr("id", "txt-btn-" + div_id);
    $(".border-btn").attr("id", "border-btn-" + div_id);
    $(".src-btn").attr("id", "src-btn-" + div_id);
    $(".text-color-btn").attr("id", "text-color-btn-" + div_id);
    $(".border-color-btn").attr("id", "border-color-btn-" + div_id);
    $(".background-color-btn").attr("id", "background-color-btn-" + div_id);
    $(".top-margin-btn").attr("id", "top-margin-btn-" + div_id);
    $(".bottom-margin-btn").attr("id", "bottom-margin-btn-" + div_id);
    $('#txt-btn-' + div_id).val($('#' + div_id).find("button.btn-element").text());
    if ($('a#a-src-btn' + $('#div-id-btn_').val()).length) {
      $('#src-btn-' + div_id).val($('#a-src-btn' + div_id).attr('href'));
    }
    $('#border-btn-' + div_id).val($('#' + div_id).find("button.btn-element").css('border').substring(0, $('#' + div_id).find("button.btn-element").css('border').length - ($('#' + div_id).find("button.btn-element").css('border-color').length + 9)));
    $('#top-margin-btn-' + div_id).val($('#' + div_id).css('margin-top').substring(0, $('#' + div_id).css('margin-top').length - 2));
    $('#bottom-margin-btn-' + div_id).val($('#' + div_id).css('margin-bottom').substring(0, $('#' + div_id).css('margin-bottom').length - 2));
    $('#text-color-btn-' + div_id).val($('#' + div_id).find("button.btn-element").css('color'));
    $('#border-color-btn-' + div_id).val($('#' + div_id).find("button.btn-element").css('border-color'));
    $('#background-color-btn-' + div_id).val($('#' + div_id).find("button.btn-element").css('background-color'));
    $('#' + div_id).find("a#edit-btn").attr("data-target", "#btn" + div_id);
    $(".background-color-btn").spectrum({
      allowEmpty: true,
      showInput: true,
      preferredFormat: 'rgb',
      change: function (color) {
        if (color == null) {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('background', 'rgb(90, 195, 232)');
        } else {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('background', color.toHexString());
        }
      }
    });
    $(".text-color-btn").spectrum({
      allowEmpty: true,
      showInput: true,
      preferredFormat: 'rgb',
      change: function (color) {
        if (color == null) {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('color', 'rgb(255, 255, 255)');
        } else {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('color', color.toHexString());
        }
      }
    });

    $(".border-color-btn").spectrum({
      allowEmpty: true,
      showInput: true,
      preferredFormat: 'rgb',
      change: function (color) {
        if (color == null) {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('border-color', 'rgb(255, 255, 255)');
        } else {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('border-color', color.toHexString());
        }
      }
    });
  }

  $(".modal-body .replace_img").click(function () {
    $('#img-p' + $('#div-id-img_').val()).modal('hide');
  });

  function delete_element (element) {
    var div_id_ = $(element).closest("div").attr("id");
    var div_id = $("div#" + div_id_).parent("div").attr("id");
    if ($('#' + div_id).prev().hasClass('draggableContainer'))
      $('#' + div_id).prev().remove();
    if ($('#' + div_id).next().next().hasClass('draggableContainer')) {
      $('#' + div_id).next().remove();
      $('#' + div_id).next().remove();
    } else if ($('#' + div_id).next().hasClass('draggableContainer'))
      $('#' + div_id).next().remove();


    $('<div class="draggableContainer ui-droppable" id="<?php echo rand(0, 1000000000000); ?>">' +
       '<div class="poc-drop-zone-text" style="display:none;">' +
       '<span class="poc-drop-here">Drop Here</span>' +
       '</div>' +
       '</div>').insertBefore("#" + div_id);
    drop_element();
    $('#' + div_id).remove();
  }

  $("input#divpp-"+$('#div-id-img').val()).change(function(){
    readURL(this);
  });
</script>

<script>
  $("form#uploadMassEmail").on('submit', function (e) {
    e.preventDefault();
    $('div.img_loader').removeClass('hidden');
    let formData = new FormData($(this)[0]);
    $.ajax({
      url: '../update_Profilepic/update_mass_email?var_page=mass_email',
      type: 'POST',
      data: formData,
      success: function (data) {

        let res = jQuery.parseJSON(data);

        if (res.success) {

          var source_img = "uploads/mass_email/" + res.message;
          $('#' + $('#div-id-img').val()).find("img#profile_picture_img_").removeClass('hidden');
          $('#' + $('#div-id-img').val()).removeClass('poc_img_bg');
          $('#' + $('#div-id-img').val()).find("div#poc_img_placeholder-id").removeClass('poc_img_placeholder');
          $('#' + $('#div-id-img').val()).find("img#profile_picture_img_").attr('src', source_img);
          $('#img-' + $('#div-id-img').val()).modal('hide');
          $('.profile_pic').val("");
          //alert(source_img);
          //window.location.reload();

        } else {
          console.log(res.message);
          bootbox.alert(res.message);

          document.getElementById('loading_pic').style.display = "none";
        }

      },
      cache: false,
      contentType: false,
      processData: false
    });

    //return false;
  });
</script>
</body>
</html>