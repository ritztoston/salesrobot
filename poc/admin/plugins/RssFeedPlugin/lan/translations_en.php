<?php

return [
    ['%d items deleted', '%d items deleted'],
    ['%d items, %d new items', '%d items, %d new items'],
    ['Additional feed elements to be included in each item\'s data', 'Additional feed elements to be included in each item\'s data'],
    ['All items whose published date is earlier will be deleted.', 'All items whose published date is earlier will be deleted.'],
    ['Campaign repetition must be enabled in config.php', 'Campaign repetition must be enabled in config.php'],
    ['Custom template', 'Custom template'],
    ['Delete outdated RSS items', 'Delete outdated RSS items'],
    ['Embargo advanced for RSS message %s', 'Embargo advanced for RSS message %s'],
    ['Enter the number of days to be kept.', 'Enter the number of days to be kept.'],
    ['Failed to fetch URL %s %s', 'Failed to fetch URL %s %s'],
    ['Feed', 'Feed'],
    ['Fetch RSS items', 'Fetch RSS items'],
    ['Fetching', 'Fetching'],
    ['How to order feed items', 'How to order feed items'],
    ['Item HTML template', 'Item HTML template'],
    ['Latest items first', 'Latest items first'],
    ['Maximum number of items to send in an RSS email', 'Maximum number of items to send in an RSS email'],
    ['Minimum number of items to send in an RSS email', 'Minimum number of items to send in an RSS email'],
    ['Must have [RSS] placeholder in an RSS message', 'Must have [RSS] placeholder in an RSS message'],
    ['Not modified', 'Not modified'],
    ['Oldest items first', 'Oldest items first'],
    ['Published', 'Published'],
    ['RSS feed URL', 'RSS feed URL'],
    ['RSS message %d marked as "sent" because it has finished repeating', 'RSS message %d marked as "sent" because it has finished repeating'],
    ['Repeat interval must be selected for an RSS campaign', 'Repeat interval must be selected for an RSS campaign'],
    ['Sorry, only super users can delete RSS items from the database', 'Sorry, only super users can delete RSS items from the database'],
    ['Text to append when the title of the latest item is used in the subject', 'Text to append when the title of the latest item is used in the subject'],
    ['There are no active RSS feeds to fetch', 'There are no active RSS feeds to fetch'],
    ['View RSS items', 'View RSS items'],
];
