 
  var membership_type = $('#hidden_membership_type').val();
  var ifNULL = $('#ifNULL').val();
  var unit_price = 0;
  var amount_payable = 0;
  var response ='';
  var guest_array = [];
  
var events_checked_cnt = 0;
//for total amount fees
  $(document).on('click','.events', function(){
      var ID = $(this).val();
      var label = $(this).next("label").text();
      if(membership_type == 'ELITE')
    {
      unit_price = 0;
    $('#submit').text('Submit');  
    }
    else if(membership_type == 'PROFESSIONAL')
    {
      unit_price = $('#p_p_'+ID).val();
    }
    else if(membership_type == 'GUEST')
    {
      unit_price = $('#g_p_'+ID).val();
    }
    else
    {
      unit_price = $('#g_p_'+ID).val();
    }
        update_summary(ID,label,unit_price);
    });

 $("#sub_payment").click(function(){
      /*bootbox.alert('This page is currently under maintenance. <br>'+
        'You may go to our single event page to process your registration.', function() {
        Ladda.stopAll();
      });*/
      current_url = window.location.href;
      var checked_events = [];
      $.each($("input[name='_events']:checked"), function(){            
      checked_events.push($(this).val());
      })
      url = "../event/registration_form?events="+checked_events.join()+'&page=R';
      $(location).attr("href", url);
    
  }); 

  $(document).ready(function(){
      $.each($("input[name='_events']:checked"), function(){  
        var ID = $(this).val();
        var label = $(this).next("label").text();        
        if(membership_type == 'ELITE')
      {
        unit_price = 0;
      $('#submit').text('Submit');  
      }
      else if(membership_type == 'PROFESSIONAL')
      {
        unit_price = $('#p_p_'+ID).val();
      }
      else if(membership_type == 'GUEST')
      {
        unit_price = $('#g_p_'+ID).val();
      }
      else
      {
        unit_price = $('#g_p_'+ID).val();
      }
        update_summary(ID,label,unit_price);
      });

    if($('#payment_form').val() == 'pf_01')
    {
        var guest_array= [];
        payment_form(amount_payable,guest_array,'N'); 
    }
    
  });

  function payment_form(total_payable,guest_array,sign)
  {
    //console.log(guest_array.length);
    //console.log(JSON.stringify(guest_array));
    bootbox.hideAll();
    var checked_events = [];
          $.each($("input[name='_events']:checked"), function(){            
          checked_events.push($(this).val());
          })
    $.ajax({
              url: 'payment_form',
              type: 'POST',
              data: 'checked_='+checked_events+"&total_fees="+total_payable+"&unitPrice="+unit_price+"&guest_array="+JSON.stringify(guest_array)+"&sign="+sign,
              success: function(data){
                  bootbox.dialog({
                      message: data,
                      title: "Payment Information",
                      buttons: {
                          danger: {
                              label: "CANCEL",
                              className: "btn-default"
                          }
                      }
              });
              }
    })
  }


  function update_summary(ID,label,unit_price)
  {

             if(!$('input:checkbox[value="' + ID + '"]').is(':checked'))
             {
             
                amount_payable = parseInt(amount_payable) - parseInt(unit_price);
                $('ul #Event_n'+ID+'').remove();
                events_checked_cnt--;
             }
             else
             {
                amount_payable = parseInt(amount_payable) + parseInt(unit_price);
                $("#list_events").append('<li id=Event_n'+ID+' ><div class="col-md-10" >'+'&#10004 '+label+'</div><div class="pull-right ">$'+unit_price+'</div></div></li>');
                events_checked_cnt++;
                
             }
             
             $('#total_fees').html('<span class="amt_p">$'+parseInt(amount_payable)+'.00</span>');

             if(events_checked_cnt>=1)
                $('#sub_payment').prop('disabled', false);
             else
                $('#sub_payment').prop('disabled', true);
  }

  function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }



   
