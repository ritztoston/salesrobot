</div>
</div>
</div><!-- ENDOF #mainContent-->
</div><!-- ENDOF .wrapper -->
</div><!-- ENDOF #container -->

<div id="footer">
<div id="footerframe">
<ul class="list-unstyled">
<li>&nbsp;</li>
</ul>
</div>
</div>
<!-- <script type="text/javascript" src="admin/ui/phplist-ui-bootlist/js/jquery-1.12.1.min.js"></script> -->
<script type="text/javascript" src="admin/js/phplistapp.js"></script>
<script type="text/javascript" src="admin/ui/phplist-ui-bootlist/js/dist/phpList_ui_bootlist.min.js"></script>
<script>
/* do not remove this from here */
$(document).ready(function(){
    if ( $('body').hasClass('invisible') ){
        myfunction();
    }
	$('#edit_list_categories input.form-control').tagsinput('refresh');

});
</script>

<script src="myStyles/jquery.easing.1.3.js"></script>
  <script src="myStyles/modernizr-2.8.2.min.js"></script>
  <script src="myStyles/imagesloaded.pkgd.min.js"></script>
  <script src="myStyles/respond.src.js"></script>
  <script src="myStyles/libs.min.js"></script>
  <script src="myStyles/mediaelement-and-player.min.js"></script>
  <script src="myStyles/video.js"></script>
  <script src="myStyles/bigvideo.js"></script>
  <script src="myStyles/gmap3.min.js"></script>
  <script src="myStyles/map.js"></script>
  <script type="text/javascript" src="myStyles/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="myStyles/jquery.themepunch.revolution.min.js"></script>
  <script src="myStyles/main.js"></script>
  <script src="myStyles/bootstrap-switch.min.js"></script>
  <script src="myStyles/bootstrap.min.js"></script>
  <script src="myStyles/jquery-ui.js"></script>
  <script src="myStyles/spectrum.js"></script>

  <script type="text/javascript">
    $(document).ready(function () {
      $("[name='my-checkbox']").bootstrapSwitch();

      $(".src-img").on('blur', function () {

        if ($("#src-img-" + $('#div-id-img_').val()).val() == "") {
          $('a#a-src-img' + $('#div-id-img_').val()).contents().unwrap();
        } else {
          if ($('a#a-src-img' + $('#div-id-img_').val()).length) {
            $('#a-src-img' + $('#div-id-img_').val()).attr("href", $("#src-img-" + $('#div-id-img_').val()).val());
          } else {
            $('#' + $('#div-id-img_').val()).find("img#profile_picture_img_").wrap("<a href='" + $("#src-img-" + $('#div-id-img_').val()).val() + "' id='a-src-img" + $('#div-id-img_').val() + "' ></a>")
          }
        }
      });

      $(".btn_text").on('blur', function () {

        if ($("#txt-btn-" + $('#div-id-btn_').val()).val() === "") {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").text('');
        } else {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").text($("#txt-btn-" + $('#div-id-btn_').val()).val());
        }
      });

      // kim
      $(".btn_text-title").on('blur', function() {
        if ($("#txt-btn-" + $('#div-id-btn_').val()).val() === "") {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").text('');
        } else {
          document.getElementById("my-title-" + $('#div-id-btn_').val()).innerHTML = $("#txt-btn-" + $('#div-id-btn_').val()).val();
        }
      });

      // $(".m-tinymce").on('blur', function() {
      //   document.getElementById('right-text-content').innerHTML = tinymce.get('m-tinymce').getContent();
      //   alert("test");
      // });

      $(".src-btn").on('blur', function () {

        if ($("#src-btn-" + $('#div-id-btn_').val()).val() == "") {
          $('a#a-src-btn' + $('#div-id-btn_').val()).contents().unwrap();
        } else {
          if ($('a#a-src-btn' + $('#div-id-btn_').val()).length) {
            $('#a-src-btn' + $('#div-id-btn_').val()).attr("href", $("#src-btn-" + $('#div-id-btn_').val()).val());
          } else {
            $('#' + $('#div-id-btn_').val()).find("button.btn-element").wrap("<a href='" + $("#src-btn-" + $('#div-id-btn_').val()).val() + "' id='a-src-btn" + $('#div-id-btn_').val() + "' ></a>")
          }
        }
      });

      $(".border-btn").on('blur', function () {
        if ($("#border-btn-" + $('#div-id-btn_').val()).val() == "") {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('border');
        } else {
          $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('border', $("#border-btn-" + $('#div-id-btn_').val()).val() + 'px solid ' + $("#border-color-btn-" + $('#div-id-btn_').val()).val());
        }
      });

      $(".top-margin-btn").on('blur', function () {

        if ($("#top-margin-btn-" + $('#div-id-btn_').val()).val() == "") {
          $('#' + $('#div-id-btn_').val()).css("margin-top", "");
        } else {
          $('#' + $('#div-id-btn_').val()).css("margin-top", $("#top-margin-btn-" + $('#div-id-btn_').val()).val() + 'px');
        }
      });

      $(".bottom-margin-btn").on('blur', function () {

        if ($("#bottom-margin-btn-" + $('#div-id-btn_').val()).val() == "") {
          $('#' + $('#div-id-btn_').val()).css("margin-bottom", "");
        } else {
          $('#' + $('#div-id-btn_').val()).css("margin-bottom", $("#bottom-margin-btn-" + $('#div-id-btn_').val()).val() + 'px');
        }
      });

      drop_element();
    });
    //drag codes
    $('.draggable-btn').draggable({
      revert: function (socketObj) {
        if (socketObj === false) {
          return true;
        } else {
          return true;
        }
      },
      stack: ".draggable-btn"
    });
    $('.draggable-divider').draggable({
      revert: function (socketObj) {
        if (socketObj === false) {
          return true;
        } else {
          return true;
        }
      },
      stack: ".draggable-divider"
    });
    $('.draggable-text').draggable({
      revert: function (socketObj) {
        if (socketObj === false) {
          return true;
        } else {
          return true;
        }
      },
      stack: ".draggable-text"
    });
    $('.draggable-img').draggable({
      revert: function (socketObj) {
        if (socketObj === false) {
          return true;
        } else {
          return true;
        }
      },
      stack: ".draggable-img"
    });
    $('.draggable-left').draggable({
      revert: function (socketObj) {
        if (socketObj === false) {
          return true;
        } else {
          return true;
        }
      },
      stack: ".draggable-left"
    });
    $('.draggable-right').draggable({
      revert: function (socketObj) {
        if (socketObj === false) {
          return true;
        } else {
          return true;
        }
      },
      stack: ".draggable-right"
    });

    //end of drag codes
    function drop_element () {
      $(".draggableContainer").droppable({
        activeClass: "poc-active",
        hoverClass: "poc-drop-zone-hover",
        drop: function (event, ui) {
          if (ui.draggable.attr('id') == "drag-text") {
            $('<div class="draggableContainer " id="tag-id">' +
               '<div class="poc-drop-zone-text" style="display:none;">' +
               '<span class="poc-drop-here" style="display:none">Drop Here</span>' +
               '</div>' +
               '</div><div class="poc-texttag" id="tag-id"><p>Insert text here</p><div class="poc_outline" id="tag-id">' +
               '<ul class="dima-menu sf-menu" style="float: right;">' +
               '<li style="display: flex;" class="actions-icon">' +
               '<a onclick="" class="a-icon-edit" data-toggle="modal" id="edit-txt" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>' +
               '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>' +
               '</li>' +
               '</ul>' +
               '</div></div>' +
               '<div class="draggableContainer " id="tag-id">' +
               '<div class="poc-drop-zone-text" style="display:none;">' +
               '<span class="poc-drop-here" style="display:none">Drop Here</span>' +
               '</div></div>')
               .insertBefore(this);
            $(this).remove();
            drop_element();
          } else if (ui.draggable.attr('id') == "drag-img") {
            $('<div class="draggableContainer " id="tag-id">' +
               '<div class="poc-drop-zone-text" style="display:none;">' +
               '<span class="poc-drop-here" style="display:none">Drop Here</span>' +
               '</div></div>' +
               '<div class="poc_img_bg poc_imgtag undrop-poc" id="tag-id" style="width: auto;height: 135px;">' +
               '<div class="poc_outline" id="tag-id">' +
               '<ul class="dima-menu sf-menu" style="float: right;" id="ul-action">' +
               '<li style="display: flex" class="actions-icon" id="li-action">' +
               '<a onclick="img_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-img" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>' +
               '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>' +
               '</li>' +
               '</ul>' +
               '</div>' +
               '<div class="poc_img_placeholder" id="poc_img_placeholder-id" style="width: auto;height: 135px;">' +
               '<img id="profile_picture_img_" class="hidden" src="" alt="banner" style="width: 586px;height: 135px;"/>' +
               '</div>' +
               '</div>' +
               '<div class="draggableContainer " id="tag-id">' +
               '<div class="poc-drop-zone-text" style="display:none;">' +
               '<span class="poc-drop-here" style="display:none">Drop Here</span>' +
               '</div></div>')
               .insertBefore(this);
            $(this).remove();
            drop_element();
          } else if (ui.draggable.attr('id') == "drag-btn") {
            $('<div class="draggableContainer " id="tag-id">' +
               '<div class="poc-drop-zone-text" style="display:none;">' +
               '<span class="poc-drop-here" style="display:none">Drop Here</span>' +
               '</div></div>' +
               '<div class="poc-btntag" id="tag-id">' +
               '<button class="button-block btn-element" style="cursor: pointer; text-decoration: none; line-height: 110%; border: 1px solid; padding: 12px 18px; display: block; margin-left: auto; margin-right: auto; border-radius: 6px; font-family: Arial; color: rgb(255, 255, 255); text-align: center; font-size: 16px; font-weight: normal; font-style: normal; background: rgb(90, 195, 232);">Button</button>' +
               '<div class="poc_outline" id="tag-id">' +
               '<ul class="dima-menu sf-menu" style="float: right;">' +
               '<li style="display: flex;" class="actions-icon">' +
               '<a onclick="btn_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-btn" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>' +
               '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>' +
               '</li>' +
               '</ul>' +
               '</div>' +
               '</div>' +
               '<div class="draggableContainer " id="tag-id">' +
               '<div class="poc-drop-zone-text" style="display:none;">' +
               '<span class="poc-drop-here" style="display:none">Drop Here</span>' +
               '</div></div>')
               .insertBefore(this);
            $(this).remove();
            drop_element();
          } else if (ui.draggable.attr('id') == "drag-left") {
            $(
               '<div class="draggableContainer " id="tag-id">'+
               '<div class="poc-drop-zone-text" style="display:none;">'+
               '<span class="poc-drop-here" style="display:none">Drop Here</span>'+
               '</div></div>'+
               '<div class="poc-combo-block poc-content-block we" id="tag-id">'+
               '<div class="poc-content-block-outline" id="tag-id">'+
               '<ul class="dima-menu sf-menu" style="float: right;">'+
               '<li style="display: flex;" class="actions-icon">'+
               '<a onclick="left_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-right" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>'+
               '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>'+
               '</li>'+
               '</ul>'+
               '</div>'+
               '<!-- IMAGE AT LEFT OF TEXT - TEMPLATE START -->'+
               '<!--[if mso | IE]>'+
               '<table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600">'+
               '<tr>'+
               '<td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">'+
               '<![endif]-->'+
               '<div  style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">'+
               '<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">'+
               '<tbody>'+
               '<tr>'+
               '<td style="border:7px solid #eeeeee;border-bottom:0px;border-top:0px;direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:0px;text-align:center;vertical-align:top;">'+
               '<!--[if mso | IE]>'+
               '<table role="presentation" border="0" cellpadding="0" cellspacing="0">'+
               '<tr>'+
               '<td class="" style="vertical-align:top;width:120px;">'+
               '<![endif]-->'+
               '<div class="mj-column-per-20 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">'+
               '<table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">'+
               '<tbody><tr><td  style="vertical-align:top;padding:5px;">'+
               '<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">'+
               '<tr><td align="center" style="font-size:0px;padding:0px;word-break:break-word;">'+
               '<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">'+
               '<tbody><tr><td  style="width:110px;"><div class="left-img-content">'+
               '<img height="auto" src="http://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;" width="110"/>'+
               '</div></td></tr></tbody></table></td></tr></table></td></tr></tbody></table></div>'+
               '<!--[if mso | IE]>'+
               '</td><td class="" style="vertical-align:top;width:480px;"><![endif]-->'+
               '<div class="mj-column-per-80 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">'+
               '<table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">'+
               '<tbody><tr><td  style="vertical-align:top;padding:5px;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">'+
               '<tr><td align="justify" style="font-size:0px;padding:0px;word-break:break-word;">'+
               '<div class="left-text-content" style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:14px;text-align:justify;color:#000000;">'+
               '<span style="font-family: \'times new roman\', times, serif; font-size: 12pt;"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit. <em>Etiam vel condimentum libero</em>,<span style="text-decoration: underline;">eget tempus mi</span>. <a href="google.com">Pellentesque</a> id luctus dolor, eu viverra mauris. <strong>Suspendisse</strong> nec venenatis lectus. Morbi fringilla fermentum malesuada. Aliquam interdum odio lorem, <span style="text-decoration: underline;">non convallis lacus</span> ultricies ac.<strong>Orci varius natoque</strong> penatibus <a href="google.com">et magnis</a> dis parturient montes, nascetur ridiculus mus. Nulla tincidunt augue non arcu scelerisque luctus sit amet in ligula.</span>'+
               '</div></td></tr></table></td></tr></tbody></table></div>'+
               '<!--[if mso | IE]>'+
               '</td></tr></table>'+
               '<![endif]-->'+
               '</td></tr></tbody></table></div>'+
               '<!--[if mso | IE]>'+
               '</td></tr></table><![endif]--><!-- IMAGE AT LEFT OF TEXT - TEMPLATE END -->'+
               '</div>'+
               '<div class="clear" style="padding-bottom: 0px !important;"></div>'+
               '<div class="draggableContainer " id="tag-id">'+
               '<div class="poc-drop-zone-text" style="display:none;">'+
               '<span class="poc-drop-here" style="display:none">Drop Here</span>'+
               '</div></div>'
            )
               .insertBefore(this);
            $(this).remove();
            drop_element();
          } else if (ui.draggable.attr('id') == "drag-right") {
            $(
               '<div class="draggableContainer " id="tag-id">'+
               '<div class="poc-drop-zone-text" style="display:none;">'+
               '<span class="poc-drop-here" style="display:none">Drop Here</span>'+
               '</div></div>'+
               '<div class="poc-combo-block poc-content-block we" id="tag-id">'+
               '<div class="poc-content-block-outline" id="tag-id">'+
               '<ul class="dima-menu sf-menu" style="float: right;">'+
               '<li style="display: flex;" class="actions-icon">'+
               '<a onclick="right_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-right" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>'+
               '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>'+
               '</li>'+
               '</ul>'+
               '</div>'+
               '<!-- IMAGE AT RIGHT OF TEXT - TEMPLATE START -->'+
               '<!--[if mso | IE]>'+
               '<table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600">'+
               '<tr>'+
               '<td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">'+
               '<![endif]-->'+
               '<div  style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">'+
               '<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">'+
               '<tbody>'+
               '<tr>'+
               '<td style="border:7px solid #eeeeee;border-bottom:0px;border-top:0px;direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:0px;text-align:center;vertical-align:top;">'+
               '<!--[if mso | IE]>'+
               ' <table role="presentation" border="0" cellpadding="0" cellspacing="0">'+
               '<tr>'+
               '<td class="" style="vertical-align:top;width:480px;">'+
               '<![endif]-->'+
               '<div class="mj-column-per-80 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">'+
               '<table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">'+
               '<tbody>'+
               '<tr>'+
               '<td  style="vertical-align:top;padding:5px;">'+
               '<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">'+
               '<tr>'+
               '<td align="justify" style="font-size:0px;padding:0px;word-break:break-word;">'+
               '<div class="right-text-content" style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:14px;text-align:justify;color:#000000;">'+
               '<span style="font-family: \'times new roman\', times, serif; font-size: 12pt;"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit. <em>Etiam vel condimentum libero</em>,'+
               '<span style="text-decoration: underline;">eget tempus mi</span>. <a href="google.com">Pellentesque</a> id luctus dolor, eu viverra mauris. <strong>Suspendisse</strong> nec venenatis lectus. Morbi fringilla fermentum malesuada. Aliquam interdum odio lorem, <span style="text-decoration: underline;">non convallis lacus</span> ultricies ac.'+
               '<strong>Orci varius natoque</strong> penatibus <a href="google.com">et magnis</a> dis parturient montes, nascetur ridiculus mus. Nulla tincidunt augue non arcu scelerisque luctus sit amet in ligula.</span>'+
               '</div>'+
               '</td>'+
               '</tr>'+
               '</table>'+
               '</td>'+
               '</tr>'+
               '</tbody>'+
               '</table>'+
               '</div>'+
               '<!--[if mso | IE]>'+
               '</td>'+
               '<td class="" style="vertical-align:top;width:120px;">'+
               '<![endif]-->'+
               '<div class="mj-column-per-20 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">'+
               '<table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">'+
               '<tbody>'+
               '<tr>'+
               '<td style="vertical-align:top;padding:5px;">'+
               '<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">'+
               '<tr>'+
               '<td align="center" style="font-size:0px;padding:0px;word-break:break-word;">'+
               '<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">'+
               '<tbody>'+
               '<tr>'+
               '<td  style="width:110px;">'+
               '<div class="right-img-content">'+
               '<img height="auto" src="http://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;" width="110" />'+
               '</div>'+
               '</td>'+
               '</tr>'+
               '</tbody>'+
               '</table>'+
               '</td>'+
               '</tr>'+
               '</table>'+
               '</td>'+
               '</tr>'+
               '</tbody>'+
               '</table>'+
               '</div>'+
               '<!--[if mso | IE]>'+
               '</td>'+
               '</tr>'+
               '</table>'+
               '<![endif]-->'+
               '</td>'+
               '</tr>'+
               '</tbody>'+
               '</table>'+
               '</div>'+
               '<!--[if mso | IE]>'+
               '</td>'+
               '</tr>'+
               '</table>'+
               '<![endif]-->'+
               '<!-- IMAGE AT RIGHT OF TEXT - TEMPLATE END -->'+
               '</div>'+
               '<div class="clear" style="padding-bottom: 0px !important;"></div>'+
               '<div class="draggableContainer " id="tag-id">'+
               '<div class="poc-drop-zone-text" style="display:none;">'+
               '<span class="poc-drop-here" style="display:none">Drop Here</span>'+
               '</div></div>'
            )
               .insertBefore(this);
            $(this).remove();
            drop_element();
          } else if (ui.draggable.attr('id') == "drag-divider") {
            $('<div class="draggableContainer " id="tag-id">' +
               '<div class="poc-drop-zone-text" style="display:none;">' +
               '<span class="poc-drop-here" style="display:none">Drop Here</span>' +
               '</div></div>' +
               '<div class="poc-dividertag" id="tag-id" >' +
               '<div style="height: 20px; overflow: hidden; width: 70%;"></div>' +
               // '<hr class="styled-hr" style="width:70%;margin-bottom: 0px !important;margin-top: 0px !important;margin: auto;">' +
               '<div style="height: 20px; overflow: hidden; width: 70%;"></div>' +
               '<div class="poc_outline" id="tag-id">' +
               '<ul class="dima-menu sf-menu" style="float: right;">' +
               '<li style="display: flex;" class="actions-icon">' +
               // '<a class="a-icon-edit" data-toggle="modal" id="edit-div" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>' +
               '<a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>' +
               '</li>' +
               '</ul>' +
               '</div>' +
               '</div>' +
               '<div class="draggableContainer " id="tag-id">' +
               '<div class="poc-drop-zone-text" style="display:none;">' +
               '<span class="poc-drop-here" style="display:none">Drop Here</span>' +
               '</div></div>')
               .insertBefore(this);
            $(this).remove();
            drop_element();
          }
        }
      });
    }

    //for profile picture select image preview
    function readURL (input) {
      if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
          $('#pp-img-' + $('#div-id-img').val()).removeClass('hidden');
          $('#pp-img-' + $('#div-id-img').val()).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
      }
    }

    function img_dialog (element) {
      $('div.img_loader').addClass('hidden');
      let div_id_ = $(element).closest("div").attr("id");
      let div_id = $("div#" + div_id_).parent("div").attr("id");
      $('.src-img').val('');
      if ($('#' + div_id).find("img#profile_picture_img_").attr('src') == '') {
        $('.img-modal').attr("id", "img-" + div_id);
        $(".modal-body #div-id-img").val(div_id);
        $(".modal-body .pp-img").attr("id", "pp-img-" + div_id);
        $(".modal-body .profile_pic").attr("id", "divpp-" + div_id);
        $('#' + div_id).find("a#edit-img").attr("data-target", "#img-" + div_id);

      } else {
        $('.img-modal-p').attr("id", "img-p" + div_id);
        $(".modal-body #div-id-img_").val(div_id);
        $(".modal-body .replace_img").attr("data-target", "#img-" + div_id);
        $(".src-img").attr("id", "src-img-" + div_id);
        if ($('a#a-src-img' + $('#div-id-img_').val()).length) {
          $('#src-img-' + div_id).val($('#a-src-img' + div_id).attr('href'));
        }
        $('#' + div_id).find("a#edit-img").attr("data-target", "#img-p" + div_id);
        $(".border-color-img").spectrum({
          allowEmpty: true,
          change: function (color) {
            alert(color.toHexString());
          }
        });
      }
    }

    // kim
    function title_dialog(element) {
      let div_id_ = $(element).closest("div").attr("id");
      let div_id = $("div#" + div_id_).parent("div").attr("id");
      $('.title-modal').attr("id", "title-" + div_id);
      $('.my-title').attr("id", "my-title-" + div_id);
      $(".modal-body #div-id-btn_").val(div_id);

      $(".btn_text-title").attr("id", "txt-btn-" + div_id);
      $('#' + div_id).find("a#edit-title").attr("data-target", "#title-" + div_id);

      $('#txt-btn-' + div_id).val(document.getElementById("my-title-" + div_id).innerHTML);
    }

    function right_dialog(element) {
      let div_id_ = $(element).closest("div").attr("id");
      let div_id = $("div#" + div_id_).parent("div").attr("id");
      $('.right-modal').attr("id", "right-" + div_id);

      $('.right-text-content').attr("id", "right-text-content-" + div_id_);
      $('#right-text-content-' + div_id_).attr('class', '');
      $('#m-tinymce').attr("data-id", ""+div_id_);
      let content = document.getElementById('right-text-content-'+div_id_).innerHTML;
      tinymce.get('m-tinymce').setContent(content);

      $('.right-img-content').attr("id", "right-img-content-" + div_id_);
      $('#right-img-content-' + div_id_).attr('class', '');
      $('#img-tinymce').attr("data-id", ""+div_id_);
      let content2 = document.getElementById('right-img-content-'+div_id_).innerHTML;
      tinymce.get('img-tinymce').setContent(content2);

      $('#' + div_id).find("a#edit-right").attr("data-target", "#right-" + div_id);
    }

    function left_dialog(element) {
      let div_id_ = $(element).closest("div").attr("id");
      let div_id = $("div#" + div_id_).parent("div").attr("id");
      $('.left-modal').attr("id", "left-" + div_id);

      $('.left-text-content').attr("id", "left-text-content-" + div_id_);
      $('#left-text-content-' + div_id_).attr('class', '');
      $('#m-tinymce-second').attr("data-id", ""+div_id_);
      let content = document.getElementById('left-text-content-'+div_id_).innerHTML;
      tinymce.get('m-tinymce-second').setContent(content);

      $('.left-img-content').attr("id", "left-img-content-" + div_id_);
      $('#left-img-content-' + div_id_).attr('class', '');
      $('#img-tinymce-second').attr("data-id", ""+div_id_);
      let content2 = document.getElementById('left-img-content-'+div_id_).innerHTML;
      tinymce.get('img-tinymce-second').setContent(content2);

      $('#' + div_id).find("a#edit-left").attr("data-target", "#left-" + div_id);
    }

    function btn_dialog (element) {
      let div_id_ = $(element).closest("div").attr("id");
      let div_id = $("div#" + div_id_).parent("div").attr("id");
      $('.btn_text').val('');
      $('.src-btn').val('');
      $('.btn-modal').attr("id", "btn-" + div_id);
      $(".modal-body #div-id-btn_").val(div_id);
      $(".btn_text").attr("id", "txt-btn-" + div_id);
      $(".border-btn").attr("id", "border-btn-" + div_id);
      $(".src-btn").attr("id", "src-btn-" + div_id);
      $(".text-color-btn").attr("id", "text-color-btn-" + div_id);
      $(".border-color-btn").attr("id", "border-color-btn-" + div_id);
      $(".background-color-btn").attr("id", "background-color-btn-" + div_id);
      $(".top-margin-btn").attr("id", "top-margin-btn-" + div_id);
      $(".bottom-margin-btn").attr("id", "bottom-margin-btn-" + div_id);
      $('#txt-btn-' + div_id).val($('#' + div_id).find("button.btn-element").text());
      if ($('a#a-src-btn' + $('#div-id-btn_').val()).length) {
        $('#src-btn-' + div_id).val($('#a-src-btn' + div_id).attr('href'));
      }
      $('#border-btn-' + div_id).val($('#' + div_id).find("button.btn-element").css('border').substring(0, $('#' + div_id).find("button.btn-element").css('border').length - ($('#' + div_id).find("button.btn-element").css('border-color').length + 9)));
      $('#top-margin-btn-' + div_id).val($('#' + div_id).css('margin-top').substring(0, $('#' + div_id).css('margin-top').length - 2));
      $('#bottom-margin-btn-' + div_id).val($('#' + div_id).css('margin-bottom').substring(0, $('#' + div_id).css('margin-bottom').length - 2));
      $('#text-color-btn-' + div_id).val($('#' + div_id).find("button.btn-element").css('color'));
      $('#border-color-btn-' + div_id).val($('#' + div_id).find("button.btn-element").css('border-color'));
      $('#background-color-btn-' + div_id).val($('#' + div_id).find("button.btn-element").css('background-color'));

      $('#' + div_id).find("a#edit-btn").attr("data-target", "#btn-" + div_id);
      $(".background-color-btn").spectrum({
        allowEmpty: true,
        showInput: true,
        preferredFormat: 'rgb',
        change: function (color) {
          if (color == null) {
            $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('background', 'rgb(90, 195, 232)');
          } else {
            $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('background', color.toHexString());
          }
        }
      });
      $(".text-color-btn").spectrum({
        allowEmpty: true,
        showInput: true,
        preferredFormat: 'rgb',
        change: function (color) {
          if (color == null) {
            $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('color', 'rgb(255, 255, 255)');
          } else {
            $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('color', color.toHexString());
          }
        }
      });

      $(".border-color-btn").spectrum({
        allowEmpty: true,
        showInput: true,
        preferredFormat: 'rgb',
        change: function (color) {
          if (color == null) {
            $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('border-color', 'rgb(255, 255, 255)');
          } else {
            $('#' + $('#div-id-btn_').val()).find("button.btn-element").css('border-color', color.toHexString());
          }
        }
      });
    }

    $(".modal-body .replace_img").click(function () {
      $('#img-p' + $('#div-id-img_').val()).modal('hide');
    });

    function delete_element (element) {
      var div_id_ = $(element).closest("div").attr("id");
      var div_id = $("div#" + div_id_).parent("div").attr("id");
      if ($('#' + div_id).prev().hasClass('draggableContainer'))
        $('#' + div_id).prev().remove();
      if ($('#' + div_id).next().next().hasClass('draggableContainer')) {
        $('#' + div_id).next().remove();
        $('#' + div_id).next().remove();
      } else if ($('#' + div_id).next().hasClass('draggableContainer'))
        $('#' + div_id).next().remove();


      $('<div class="draggableContainer ui-droppable" id="tag-id">' +
         '<div class="poc-drop-zone-text" style="display:none;">' +
         '<span class="poc-drop-here" style="display:none">Drop Here</span>' +
         '</div>' +
         '</div>').insertBefore("#" + div_id);
      drop_element();
      $('#' + div_id).remove();
    }

    $("input#divpp-"+$('#div-id-img').val()).change(function(){
      readURL(this);
    });
  </script>

  <script>
    $("form#uploadMassEmail").on('submit', function (e) {
      e.preventDefault();
      $('div.img_loader').removeClass('hidden');
      let formData = new FormData($(this)[0]);
      $.ajax({
        url: '../update_Profilepic/update_mass_email?var_page=mass_email',
        type: 'POST',
        data: formData,
        success: function (data) {

          let res = jQuery.parseJSON(data);

          if (res.success) {

            var source_img = "uploads/mass_email/" + res.message;
            $('#' + $('#div-id-img').val()).find("img#profile_picture_img_").removeClass('hidden');
            $('#' + $('#div-id-img').val()).removeClass('poc_img_bg');
            $('#' + $('#div-id-img').val()).find("div#poc_img_placeholder-id").removeClass('poc_img_placeholder');
            $('#' + $('#div-id-img').val()).find("img#profile_picture_img_").attr('src', source_img);
            $('#img-' + $('#div-id-img').val()).modal('hide');
            $('.profile_pic').val("");
            //alert(source_img);
            //window.location.reload();

          } else {
            console.log(res.message);
            bootbox.alert(res.message);

            document.getElementById('loading_pic').style.display = "none";
          }

        },
        cache: false,
        contentType: false,
        processData: false
      });

      //return false;
    });
  </script>

  <script>


    setInterval(function(){
      const my_content = document.getElementById('w-template').innerHTML;
      const src_btn = my_content.search('tag-id');
      const src_btn2 = my_content.search('right-text-content-tag-id');
      const src_btn3 = my_content.search('right-img-content-tag-id');

      const src_btn4 = my_content.search('left-text-content-tag-id');
      const src_btn5 = my_content.search('left-img-content-tag-id');
      const rand = Math.floor(Math.random() * 1000000000000) + 1;

      if(src_btn > 0) {
        const e = document.getElementById('tag-id');
        e.id = rand;
      }

      if(src_btn2 > 0) {
        const e = document.getElementById('right-text-content-tag-id');
        e.id = rand;
      }
      if(src_btn3 > 0) {
        const rand = Math.floor(Math.random() * 1000000000000) + 1;
        const e = document.getElementById('right-img-content-tag-id');
        e.id = "right-img-content-"+rand;
      }

      if(src_btn4 > 0) {
        const e = document.getElementById('left-text-content-tag-id');
        e.id = rand;
      }
      if(src_btn5 > 0) {
        const rand = Math.floor(Math.random() * 1000000000000) + 1;
        const e = document.getElementById('left-img-content-tag-id');
        e.id = "left-img-content-"+rand;
      }


      $('.ui-helper-hidden-accessible').attr("style", "display:none;");


    }, 10);
  </script>
</body>

</html>
