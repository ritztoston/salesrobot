<?php

require_once dirname(__FILE__).'/accesscheck.php';

$actionresult = '';

if (!empty($_FILES['file_template']) && is_uploaded_file($_FILES['file_template']['tmp_name'])) {
    $content = file_get_contents($_FILES['file_template']['tmp_name']);
} elseif (isset($_POST['template'])) {
    $content = $_POST['template'];
} else {
    $content = '';
}
$sendtestresult = '';
$testtarget = getConfig('admin_address');
$systemTemplateID = getConfig('systemmessagetemplate');

if (isset($_REQUEST['id'])) {
    $id = sprintf('%d', $_REQUEST['id']);
} else {
    $id = 0;
}

function getTemplateImages($content)
{
    $image_types = array(
        'gif'  => 'image/gif',
        'jpg'  => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpe'  => 'image/jpeg',
        'bmp'  => 'image/bmp',
        'png'  => 'image/png',
        'tif'  => 'image/tiff',
        'tiff' => 'image/tiff',
        'swf'  => 'application/x-shockwave-flash',
    );
    $regexp = sprintf('/"([^"]+\.(%s))"/Ui', implode('|', array_keys($image_types)));
    preg_match_all($regexp, stripslashes($content), $images);

    return array_count_values($images[1]);
}

function getTemplateLinks($content)
{
    preg_match_all('/href="([^"]+)"/Ui', stripslashes($content), $links);

    return $links[1];
}

$msg = '';
$checkfullimages = !empty($_POST['checkfullimages']) ? 1 : 0;
$checkimagesexist = !empty($_POST['checkimagesexist']) ? 1 : 0;
$checkfulllinks = !empty($_POST['checkfulllinks']) ? 1 : 0;
$baseurl = '';

if (!empty($_POST['action']) && $_POST['action'] == 'addimages') {
    if (!$id) {
        $msg = $GLOBALS['I18N']->get('No such template');
    } else {
        $content_req = Sql_Fetch_Row_Query("select template from {$tables['template']} where id = $id");
        $images = getTemplateImages($content_req[0]);

        if (count($images)) {
            include 'class.image.inc';
            $image = new imageUpload();
            foreach ($images as $key => $val) {
                // printf('Image name: <b>%s</b> (%d times used)<br />',$key,$val);
                $image->uploadImage($key, $id);
            }
            $msg = $GLOBALS['I18N']->get('Images stored');
        } else {
            $msg = $GLOBALS['I18N']->get('No images found');
        }
    }
    $_SESSION['action_result'] = $msg.'<br/>'.s('Template saved and ready for use in campaigns');
    Redirect('templates');

    return;
    //print '<p class="actionresult">'.$msg.'</p>';
    //$msg = '';
} elseif (!empty($_POST['save']) || !empty($_POST['sendtest'])) { //# let's save when sending a test
    $templateok = 1;
    $title = $_POST['title'];
//    if (!empty($title) && strpos($content, '[CONTENT]') !== false) {
    if (!empty($title)) {
        $images = getTemplateImages($content);

        //   var_dump($images);

        if (($checkfullimages || $checkimagesexist) && count($images)) {
            foreach ($images as $key => $val) {
                if (!preg_match('#^https?://#i', $key)) {
                    if ($checkfullimages) {
                        $actionresult .= $GLOBALS['I18N']->get('Image')." $key => ".$GLOBALS['I18N']->get('"not full URL')."<br/>\n";
                        $templateok = 0;
                    }
                } else {
                    if ($checkimagesexist) {
                        $imageFound = testUrl($key);
                        if ($imageFound != 200) {
                            $actionresult .= $GLOBALS['I18N']->get('Image')." $key => ".$GLOBALS['I18N']->get('does not exist')."<br/>\n";
                            $templateok = 0;
                        }
                    }
                }
            }
        }
        if ($checkfulllinks) {
            $links = getTemplateLinks($content);
            foreach ($links as $key => $val) {
                if (!preg_match('#^https?://#i', $val) && !preg_match('#^mailto:#i', $val)
                    && !(strtoupper($val) == '[PREFERENCESURL]' || strtoupper($val) == '[UNSUBSCRIBEURL]' || strtoupper($val) == '[BLACKLISTURL]' || strtoupper($val) == '[FORWARDURL]' || strtoupper($val) == '[CONFIRMATIONURL]')
                ) {
                    $actionresult .= $GLOBALS['I18N']->get('Not a full URL').": $val<br/>\n";
                    $templateok = 0;
                }
            }
        }
    } else {
        if (!$title) {
            $actionresult .= $GLOBALS['I18N']->get('No Title').'<br/>';
        } /*else {
            $actionresult .= $GLOBALS['I18N']->get('Template does not contain the [CONTENT] placeholder').'<br/>';
        }*/
        $templateok = 0;
    }
    if ($templateok) {
        if (!$id) {
            Sql_Query(sprintf('insert into %s (title) values("%s")', $tables['template'], sql_escape($title)));
            $id = Sql_Insert_id();
        }
        Sql_Query(sprintf('update %s set title = "%s",template = "%s" where id = %d',
            $tables['template'], sql_escape($title), sql_escape($content), $id));
        Sql_Query(sprintf('select * from %s where filename = "%s" and template = %d',
            $tables['templateimage'], 'powerphplist.png', $id));
        if (!Sql_Affected_Rows()) {
            Sql_Query(sprintf('insert into %s (template,mimetype,filename,data,width,height)
      values(%d,"%s","%s","%s",%d,%d)',
                $tables['templateimage'], $id, 'image/png', 'powerphplist.png',
                $newpoweredimage,
                70, 30));
        }
        $actionresult .= '<p class="information">'.s('Template saved').'</p>';

        //# ##17419 don't prompt for remote images that exist
        $missingImages = array();
        foreach ($images as $key => $val) {
            $key = trim($key);
            if (preg_match('~^https?://~i', $key)) {
                $imageFound = testUrl($key);
                if (!$imageFound) {
                    $missingImages[$key] = $val;
                }
            } else {
                $missingImages[$key] = $val;
            }
        }

        if (count($missingImages) && empty($_POST['sendtest'])) {
            include dirname(__FILE__).'/class.image.inc';
            $image = new imageUpload();
            echo '<h3>'.$GLOBALS['I18N']->get('Images').'</h3><p class="information">'.$GLOBALS['I18N']->get('Below is the list of images used in your template. If an image is currently unavailable, please upload it to the database.').'</p>';
            echo '<p class="information">'.$GLOBALS['I18N']->get('This includes all images, also fully referenced ones, so you may choose not to upload some. If you upload images, they will be included in the campaigns that use this template.').'</p>';
            echo formStart('enctype="multipart/form-data" class="template1" ');
            echo '<input type="hidden" name="id" value="'.$id.'" />';
            ksort($images);
            reset($images);
            foreach ($images as $key => $val) {
                $key = trim($key);
                if (preg_match('~^https?://~i', $key)) {
                    $missingImage = true;
                    $imageFound = testUrl($key);
                    if ($imageFound != 200) {
                        printf($GLOBALS['I18N']->get('Image name:').' <b>%s</b> ('.$GLOBALS['I18N']->get('%d times used').')<br/>',
                            $key, $val);
                        echo $image->showInput($key, $val, $id);
                    }
                } else {
                    printf($GLOBALS['I18N']->get('Image name:').' <b>%s</b> ('.$GLOBALS['I18N']->get('%d times used').')<br/>',
                        $key, $val);
                    echo $image->showInput($key, $val, $id);
                }
            }

            echo '<input type="hidden" name="id" value="'.$id.'" /><input type="hidden" name="action" value="addimages" />
        <input class="submit" type="submit" name="addimages" value="' .$GLOBALS['I18N']->get('Save Images').'" /></form>';
            if (empty($_POST['sendtest'])) {
                return;
            }
            //    return;
        } else {
            $_SESSION['action_result'] = s('Template was successfully saved');
//      print '<p class="information">'.$GLOBALS['I18N']->get('Template does not contain local images')."</p>";
            if (empty($_POST['sendtest'])) {
                Redirect('templates');

                return;
            }
            //    return;
        }
    } else {
        $actionresult .= $GLOBALS['I18N']->get('Some errors were found, template NOT saved!');
        $data['title'] = $title;
        $data['template'] = $content;
    }
    if (!empty($_POST['sendtest'])) {
        //# check if it's the system message template or a normal one:

        $targetEmails = explode(',', $_POST['testtarget']);
        $testtarget = '';

        if ($id == $systemTemplateID) {
            $actionresult .= '<h3>'.$GLOBALS['I18N']->get('Sending test').'</h3>';
            foreach ($targetEmails as $email) {
                if (validateEmail($email)) {
                    $testtarget .= $email.', ';
                    $actionresult .= $GLOBALS['I18N']->get('Sending test "Request for confirmation" to').' '.$email.'  ';
                    if (sendMail($email, getConfig('subscribesubject'), getConfig('subscribemessage'))) {
                        $actionresult .= s('OK');
                    } else {
                        $actionresult .= s('FAILED');
                    }
                    $actionresult .= '<br/>';
                    $actionresult .= $GLOBALS['I18N']->get('Sending test "Welcome" to').' '.$email.'  ';
                    if (sendMail($email, getConfig('confirmationsubject'), getConfig('confirmationmessage'))) {
                        $actionresult .= s('OK');
                    } else {
                        $actionresult .= s('FAILED');
                    }
                    $actionresult .= '<br/>';
                    $actionresult .= $GLOBALS['I18N']->get('Sending test "Unsubscribe confirmation" to').' '.$email.'  ';
                    if (sendMail($email, getConfig('unsubscribesubject'), getConfig('unsubscribemessage'))) {
                        $actionresult .= s('OK');
                    } else {
                        $actionresult .= s('FAILED');
                    }
                } elseif (trim($email) != '') {
                    $actionresult .= '<p>'.$GLOBALS['I18N']->get('Error sending test messages to').' '.htmlspecialchars($email).'</p>';
                }
            }
        } else {
            //# Sending test emails of non system templates to be added.
            $actionresult .= '<p>'.s('Sending a test from templates only works for the system template.').' '.
                s('To test your template, go to campaigns and send a test campaign using the template.').
                '</p>';
        }
        if (empty($testtarget)) {
            $testtarget = getConfig('admin_address');
        }
        $testtarget = preg_replace('/, $/', '', $testtarget);
    }
}
if (!empty($actionresult)) {
    echo '<div class="actionresult">'.$actionresult.'</div>';
}

if ($id) {
    $req = Sql_Query("select * from {$tables['template']} where id = $id");
    $data = Sql_Fetch_Array($req);
    //# keep POSTED data, even if not saved
    if (!empty($_POST['template'])) {
        $data['template'] = $content;
    }
} else {
    $data = array();
    $data['title'] = '';
    $data['template'] = '';
}

?>
<div class="responsive">

  <!-- LOADING -->
  <div class="all_content">
    <div class="dima-main" style="float:none;">
      <section>
        <div class="col-md-12 ">
          <div class="col-md-2" style="padding: 30px 0">
            <h2 class="hidden"></h2>
            <h3 class="hidden"></h3>
            <h4 class="hidden"></h4>

            <h5 style="text-align: left;color: #828077 !important;">ELEMENTS</h5>
            <div>
              <table width="100%">
                <tr>
                  <td colspan="3" align="center">
                    Basic
                    <div class="clear"></div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="div-element draggable-text" id="drag-text"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="text-element-img" src="icons/text.png" alt="text icon element">
                    </div>
                  </td>
                  <td>
                    &nbsp;&nbsp;&nbsp;
                  </td>
                  <td>
                    <div class="div-element draggable-img" id="drag-img"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="img-element-img" src="icons/img.png" alt="img icon element">
                    </div>
                    <br>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="div-element draggable-left" id="drag-left"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="left-element-img" src="icons/left.png" alt="left icon element">
                    </div>
                  </td>
                  <td>
                    &nbsp;&nbsp;&nbsp;
                  </td>
                  <td>
                    <div class="div-element draggable-right" id="drag-right"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="right-element-img" src="icons/right.png" alt="right icon element">
                    </div>
                    <br>
                  </td>
                </tr>
              </table>
            </div>
            <div class="clear"></div>
            <div>
              <table width="100%">
                <tr>
                  <td colspan="3" align="center">
                    Structure
                    <div class="clear"></div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="div-element draggable-divider" id="drag-divider"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="divider-element-img" src="icons/divider.png" alt="divider icon element">
                    </div>
                  </td>
                  <td>
                    &nbsp;&nbsp;&nbsp;
                  </td>
                  <td>
                    <div class="div-element draggable-btn" id="drag-btn"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="btn-element-img" src="icons/btn.png" alt="btn icon element">
                    </div>
                    <br>
                  </td>
                </tr>
              </table>
            </div>
            <br>
            <br>
          </div>
          <div class="col-md-9" id="tag-id" style="margin-left:50px;padding-left:0;">
            <div style="margin-top: 2em;border: 1px solid #8A9798;box-shadow: 0px 1px 3px 0px #888888;background: #8A9798;color: white;border-radius: 8px 8px 0px 0px;" id="tag-id">
              <nav role="navigation" class="clearfix">
                <!-- menu content -->
                <ul class="dima-menu sf-menu">
                  <li class="sub-icon" style="font-size: 18px;float: left;padding: 10px 20px 5px 10px;">
                    <!--                  kim-->
                      <?php
                      if (!empty($data['title'])) {
                          echo '<p style="padding: 0;margin: 0;float: left;" class="my-title">'.stripslashes(htmlspecialchars($data['title'])).'</p>';
                      } else {
                          echo '<p style="padding: 0;margin: 0;float: left;" class="my-title">No Title</p>';
                      }?>
                    <a id="edit-title" onclick="title_dialog(this)" data-toggle="modal" style="color: black;margin-left: 5px;"><i class="fa fa-pencil">&nbsp;</i></a>
                    <div class="modal fade title-modal" role="dialog">
                      <div class="modal-dialog modal-sm">

                        <!-- kim Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="color: #000000;">Template Name</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                            <div class="clear"></div>
                            <div class="">
                              <div class="accordion">
                                <div class="accordion-section">
                                  <div class="accordion-section-title">
                                    <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                      <tbody>
                                      <tr>
                                        <td style="color: #000000;width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                          Text
                                        </td>
                                        <td style="width:70%;border: none !important;">
                                          <div>
                                            <input type="text" name="btn_text" class="btn_text-title" style="background: white !important"/>
                                          </div>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
                <!-- !menu content -->
              </nav>
            </div>
            <div id="w-template" class="col-md-12" style="border: 1px solid #D0CDBC;box-shadow: 0px 1px 3px 0px #888888;height: 600px;overflow: auto;padding:0;">
                <?php
                echo stripslashes($data['template']);
                ?>
            </div>


            <table id="modals">
              <tr>
                <td>
                  <div class="modal fade img-modal" role="dialog">
                    <div class="modal-dialog modal-sm">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Replace Image</h4>
                        </div>
                        <div class="modal-body">
                          <form role="form" id="uploadMassEmail" method="post" enctype="multipart/form-data">
                            <div>
                              <input type="hidden" name="div-id-img" id="div-id-img" value="">
                              <input type='file' accept='image/*' name="profile_pic" class="profile_pic"
                                     style="margin-bottom: 7px;"/>
                              <img class="hover hidden pp-img" alt="image" style="width:586px;height:135px;"/>
                            </div>
                            <div class="hidden img_loader" style="text-align:center">
                              <p>Please wait</p>
                              <img src="icons/loader.gif" style="height: 2em;width: 24em;">
                            </div>
                            <div class="modal-footer">
                              <button type="submit" class="submit_btn text-center btn btn-success" id="update_pic">Update
                              </button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="modal fade btn-modal" role="dialog">
                    <div class="modal-dialog modal-sm">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Button</h4>
                        </div>
                        <div class="modal-body">
                          <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                          <div class="clear"></div>
                          <div class="">
                            <div class="accordion">
                              <div class="accordion-section">
                                <div class="accordion-section-title">
                                  <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                    <tbody>
                                    <tr>
                                      <td style="width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                        Text
                                      </td>
                                      <td style="width:70%;border: none !important;">
                                        <div>
                                          <input type="text" name="btn_text" class="btn_text" style="background: white !important">
                                        </div>
                                      </td>
                                    </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                              <div class="accordion-section">
                                <div class="accordion-section-title">
                                  <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                    <tbody>
                                    <tr>
                                      <td style="width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                        Link
                                      </td>
                                      <td style="width:70%;border: none !important;">
                                        <div>
                                          <input type="text" name="src_btn" class="src-btn" style="background: white !important">
                                        </div>
                                      </td>
                                    </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                              <div class="accordion-section">
                                <a class="accordion-section-title" href="#accordion-1">Spacing<span style="float: right;"><i class="fa fa-chevron-down">&nbsp;</i></span></a>
                                <div id="accordion-1" class="accordion-section-content">
                                  <div class="" style="">
                                    <table width="100%">
                                      <tbody>
                                      <tr>
                                        <td style="width:50%">
                                          Top Margin
                                        </td>
                                        <td style="width:50%" align="center">
                                          <div class="input-group" style="width: 70%;">
                                            <input type="text" class="top-margin-btn" name="top-margin-btn" placeholder="" size="5px">
                                            <span class="input-group-addon">PX</span>
                                          </div>
                                          <br>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:50%">
                                          Bottom Margin
                                        </td>
                                        <td style="width:50%" align="center">
                                          <div class="input-group" style="width: 70%;">
                                            <input type="text" class="bottom-margin-btn" name="bottom-margin-btn" placeholder="" size="5px">
                                            <span class="input-group-addon">PX</span>
                                          </div>
                                          <br>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                              <div class="accordion-section">
                                <a class="accordion-section-title" href="#accordion-3">Style<span style="float: right;"><i class="fa fa-chevron-down">&nbsp;</i></a>
                                <div id="accordion-3" class="accordion-section-content">
                                  <div class="" style="">
                                    <table width="100%">
                                      <tbody>
                                      <tr>
                                        <td style="width:50%">
                                          Border
                                        </td>
                                        <td style="width:50%" align="center">
                                          <div class="input-group" style="width: 70%;">
                                            <input type="text" class="border-btn" placeholder="" size="5px">
                                            <span class="input-group-addon">PX</span>
                                          </div>
                                          <br>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:50%">
                                          Border Color
                                        </td>
                                        <td style="width:50%" align="center">
                                          <input type="text" class="border-color-btn">
                                          <br>
                                          <br>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:50%">
                                          Text Color
                                        </td>
                                        <td style="width:50%" align="center">
                                          <input type="text" class="text-color-btn">
                                          <br>
                                          <br>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:50%">
                                          Background Color
                                        </td>
                                        <td style="width:50%" align="center">
                                          <input type="text" class="background-color-btn">
                                          <br>
                                          <br>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="modal fade right-modal" role="dialog">
                    <div class="modal-dialog">

                      <!-- kim Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Right Dialog</h4>
                        </div>
                        <div class="modal-body">
                          <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                          <div class="clear"></div>
                          <div class="">
                            <div class="accordion">
                              <div class="accordion-section">
                                <div class="accordion-section-title">
                                  <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                    <tbody>
                                    <tr>
                                      <td style="width:100%;border: none !important;padding: 13px 0px 0px 0px;">
                                        <div id="accordion">
                                          <div class="card">
                                            <div class="card-header" id="headingOne">
                                              <h5 class="mb-0">
                                                <button class="btn btn-primary" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                  Add Image
                                                </button>
                                              </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="height:0px;">
                                              <div class="card-body">
                                                <textarea id="img-tinymce"><img height="auto" src="http://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100px;" width="100" data-mce-selected="1"/></textarea>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colspan="2" style="width:30%;border: none !important;padding: 13px 0 0 0;">
                                        Content
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">
                                        <textarea id="m-tinymce"></textarea>
                                      </td>
                                    </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="modal fade left-modal" role="dialog">
                    <div class="modal-dialog">
                      <!-- kim Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Left Dialog</h4>
                        </div>
                        <div class="modal-body">
                          <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                          <div class="clear"></div>
                          <div class="">
                            <div class="accordion">
                              <div class="accordion-section">
                                <div class="accordion-section-title">
                                  <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                    <tbody>
                                    <tr>
                                      <td style="width:100%;border: none !important;padding: 13px 0px 0px 0px;">
                                        <div id="accordion">
                                          <div class="card">
                                            <div class="card-header" id="headingOne">
                                              <h5 class="mb-0">
                                                <button class="btn btn-primary" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                  Add Image
                                                </button>
                                              </h5>
                                            </div>

                                            <div id="collapseTwo" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="height:0px;">
                                              <div class="card-body">
                                                <textarea id="img-tinymce-second"><img height="auto" src="http://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100px;" width="100" data-mce-selected="1" /></textarea>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colspan="2" style="width:30%;border: none !important;padding: 13px 0 0 0;">
                                        Content
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">
                                        <textarea id="m-tinymce-second"></textarea>
                                      </td>
                                    </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>

<p class="information"><?php echo $msg ?></p>
<?php echo '<p class="button pull-right">'.PageLink2('templates', $GLOBALS['I18N']->get('List of Templates')).'</p><div class="clearfix"></div>'; ?>

<?php echo formStart(' enctype="multipart/form-data" class="template2" ') ?>
<input type="hidden" name="id" id="post_id" value="<?php echo $id ?>"/>
<div class="panel"><div class="content">
    <table class="templateForm">
      <tr>

        <td>
            <?php echo $GLOBALS['I18N']->get('Title of this template') ?>
        </td>
        <td><input type="text" id="post_title" name="title" value="<?php echo stripslashes(htmlspecialchars($data['title'])) ?>"
                   size="30"/></td>
      </tr>
      <tr>
        <td colspan="2"><?php echo s('Content of the template.') ?>
          <br/><?php echo s('The content should at least have <b>[CONTENT]</b> somewhere.') ?>
          <br/><?php echo s('You can upload a template file or paste the text in the box below'); ?></td>
      </tr>
      <tr>
        <td><?php echo s('Template file.') ?></td>
        <td><input type="file" name="file_template"/></td>
      </tr>
      <tr>
        <td colspan="2">
          <!-- <textarea id="m-tinymce-main"></textarea> -->
            <?php
            echo $GLOBALS['plugins'][$GLOBALS['editorplugin']]->editor('template',
                    stripslashes($data['template'])).'</div>';
            ?>
        </td>
      </tr>

      <!--tr>
  <td>Make sure all images<br/>start with this URL (optional)</td>
  <td><input type="text" name="baseurl" size="40" value="<?php echo htmlspecialchars($baseurl) ?>" /></td>
</tr-->
      <tr>
        <td><?php echo $GLOBALS['I18N']->get('Check that all links have a full URL') ?></td>
        <td><input type="checkbox" name="checkfulllinks" <?php echo $checkfulllinks ? 'checked="checked"' : '' ?> />
        </td>
      </tr>
      <tr>
        <td><?php echo $GLOBALS['I18N']->get('Check that all images have a full URL') ?></td>
        <td><input type="checkbox"
                   name="checkfullimages" <?php echo $checkfullimages ? 'checked="checked"' : '' ?> /></td>
      </tr>

        <?php if ($GLOBALS['can_fetchUrl']) {
            ?>
          <tr>
            <td><?php echo $GLOBALS['I18N']->get('Check that all external images exist') ?></td>
            <td>
              <input type="checkbox"
                     name="checkimagesexist" <?php echo $checkimagesexist ? 'checked="checked"' : '' ?> />
            </td>
          </tr>
            <?php

        } ?>
      <tr>
        <td colspan="2">
          <input class="submit" type="submit" name="save"
                 value="<?php echo $GLOBALS['I18N']->get('Save Changes') ?>"/>
        </td>
      </tr>
    </table>
    <p id="information" style="transition: all .5s;background-color: #00ff5d;color: #FFFFFF;padding: 10px;-webkit-border-radius: 3px;-moz-border-radius: 3px;border-radius: 3px; position: fixed; bottom: 0; right: 1%; display: none;">&#10004</p>
  </div></div>
<?php $sendtest_content = sprintf('<div class="sendTest" id="sendTest">
    ' .$sendtestresult.'
    <input class="submit" type="submit" name="sendtest" value="%s"/>  %s: 
    <input type="text" name="testtarget" size="40" value="' .htmlspecialchars($testtarget).'"/><br />%s
    </div>',
    $GLOBALS['I18N']->get('Send test message'), $GLOBALS['I18N']->get('to email addresses'),
    $GLOBALS['I18N']->get('(comma separate addresses - all must be existing subscribers)'));
$testpanel = new UIPanel($GLOBALS['I18N']->get('Send Test'), $sendtest_content);
$testpanel->setID('testpanel');
//  if ($systemTemplateID == $id) { ## for now, testing only for system message templates
echo $testpanel->display();
//  }
?>

</form>
<script>
  let main_flag = document.getElementById('w-template').innerHTML;

  setInterval(function(){

    const my_content2 = document.getElementById('w-template').innerHTML;
    if(my_content2 !== main_flag) {
      main_flag = my_content2
      const $div = $('<div>').html(my_content2);
      $div.find('.remove-after').attr("style", "padding-bottom: 0 !important;display:none;");
      $div.find('.remove-after-2').attr("style", "display:none");
      const processedHTML = $div.html();
      CKEDITOR.instances.template.setData(processedHTML);
    }


    const my_title =  document.getElementsByClassName('my-title');
    $('#post_title').val(my_title[0].innerHTML);

    // const post_title = $('#post_title').val();
    // const post_id = $('#post_id').val();

    // if (post_title !== '' && post_description !== '') {
    //   $.ajax({
    //     // sales
    //     url: "../../admin/plugins/CKEditorPlugin/kcfinder/autosave_wk.php",
    //     // archintel
    //     // url: "../../admin/plugins/CKEditorPlugin/kcfinder/save_post.php",
    //     method: "POST",
    //     data: {postTitle: post_title, postDescription: newDesc, postId: post_id},
    //     dataType: "text",
    //     success: function (data) {
    //       if (data !== '') {
    //         $('#post_id').val(data);
    //       }
    //     }
    //   });
    // }
  }, 500);
</script>