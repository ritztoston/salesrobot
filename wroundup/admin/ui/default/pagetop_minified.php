<?php include_once( dirname(__FILE__) . '/functions.php');

/*
  We request you retain the full headers below including the links.
  This not only gives respect to the large amount of time given freely
  by the developers, but also helps build interest, traffic and use of
  phpList, which is beneficial to it's future development.

  Michiel Dethmers, phpList Ltd 2003 - 2017
*/
?>
    <!DOCTYPE html>
<html lang="<?php echo $_SESSION['adminlanguage']['iso']?>" dir="<?php echo $_SESSION['adminlanguage']['dir']?>" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8" />
    <?php if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) header('X-UA-Compatible: IE=edge'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="License" content="GNU Affero General Public License, http://www.gnu.org/licenses/agpl.html" />
    <meta name="Author" content="Michiel Dethmers - http://www.phplist.com" />
    <meta name="Copyright" content="Michiel Dethmers, phpList Ltd - http://phplist.com" />
    <meta name="Powered-By" content="phplist version <?php echo VERSION?>" />
    <meta property="og:title" content="phpList" />
    <meta property="og:url" content="http://phplist.com" />
    <meta name="theme-color" content="#2C2C2C"/>
    <link rel="shortcut icon" type="image/png" href="./images/favicon.png"/>
    <link rel="icon" type="image/png" href="./images/favicon.png"/>
    <link rel="apple-touch-icon" href="./images/phplist-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" href="./images/phplist-touch-icon.png" />

    <!-- initial styles and JS from basic application -->
    <!-- the CSS is already mostly minified -->
    <link rel="stylesheet" href="css/app.css" />
    <link rel="stylesheet" href="ui/phplist-ui-bootlist/css/style.css?v=<?php echo filemtime(dirname(__FILE__).'/css/style.css'); ?>" />
    
    <script src="myStyles/jquery-2.1.1.min.js"></script>
<script src="https://cdn.tinymce.com/4.8/tinymce.min.js"></script>
<script>
  tinymce.init({
    selector:"#img-tinymce",
    forced_root_block: '',
    setup: function(editor) {
      editor.on('blur', function(){
        if(!catching){
          bounceProtect('blur');
          let m_id = $("#img-tinymce").attr("data-id");
          setMyContentImg(m_id);
        }
      })
    },
    height: "115",
    width: "115",
    visual : false,
    image_dimensions: false,
    image_description: false,
    toolbar: "image |",
    plugins: [
      "image imagetools media fullscreen advlist autolink lists link image imagetools charmap print preview anchor"
    ],
    menubar: false,
    statusbar: false,
    convert_urls: false,
    file_picker_callback: elFinderBrowser
  });
  tinymce.init({
    selector:"#img-tinymce-second",
    forced_root_block: '',
    setup: function(editor) {
      editor.on('blur', function(){
        if(!catching){
          bounceProtect('blur');
          let m_id = $("#img-tinymce-second").attr("data-id");
          setMyContentImg2(m_id);
        }
      })
    },
    height: "115",
    width: "115",
    visual : false,
    image_dimensions: false,
    image_description: false,
    toolbar: "image |",
    plugins: [
      "image imagetools media fullscreen advlist autolink lists link image imagetools charmap print preview anchor"
    ],
    menubar: false,
    statusbar: false,
    convert_urls: false,
    file_picker_callback: elFinderBrowser
  });
  tinymce.init({
    selector:"#m-tinymce",
    forced_root_block: '',
    setup: function(editor) {
      editor.on('blur', function(){
        if(!catching){
          bounceProtect('blur');
          let m_id = $("#m-tinymce").attr("data-id");
          setMyContent(m_id);
        }
      })
    },
    visual : false,
    toolbar: "undo redo | subscript superscript | fontselect fontsizeselect | bold italic underline | forecolor backcolor | bullist numlist | link |",
    plugins: [
      "advlist autolink lists link image imagetools charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table contextmenu paste",
      "textcolor colorpicker",
      "image imagetools",
      "noneditable",
      "autoresize"
    ],
    menubar: false,
    statusbar: false,
    convert_urls: false,
    file_picker_callback: elFinderBrowser
  });
  tinymce.init({
    selector:"#m-tinymce-second",
    forced_root_block: '',
    setup: function(editor) {
      editor.on('blur', function(){
        if(!catching){
          bounceProtect('blur');
          let m_id = $("#m-tinymce-second").attr("data-id");
          setMyContent2(m_id);
        }
      })
    },
    visual : false,
    toolbar: "undo redo | subscript superscript | fontselect fontsizeselect | bold italic underline | forecolor backcolor | bullist numlist | link |",
    plugins: [
      "advlist autolink lists link image imagetools charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table contextmenu paste",
      "textcolor colorpicker",
      "image imagetools",
      "noneditable",
      "autoresize"
    ],
    menubar: false,
    statusbar: false,
    convert_urls: false,
    file_picker_callback: elFinderBrowser
  });

  let catching = false;

  function bounceProtect(src)
  {
    catching = true;
    setTimeout(function(){ catching = false;}, 250);
  }

  function setMyContent(m_id) {
    document.getElementById('right-text-content-'+m_id).innerHTML = tinymce.get('m-tinymce').getContent();
  }
  function setMyContentImg(m_id) {
    document.getElementById('right-img-content-'+m_id).innerHTML = tinymce.get('img-tinymce').getContent();
  }

  function setMyContent2(m_id) {
    document.getElementById('left-text-content-'+m_id).innerHTML = tinymce.get('m-tinymce-second').getContent();
  }
  function setMyContentImg2(m_id) {
    document.getElementById('left-img-content-'+m_id).innerHTML = tinymce.get('img-tinymce-second').getContent();
  }

  function elFinderBrowser (callback, value, meta) {
    tinymce.activeEditor.windowManager.open({
      file: 'elfinder/elfinder.html',// use an absolute path!
      title: 'Upload an Image',
      width: 900,
      height: 450,
      resizable: 'yes'
    }, {
      oninsert: function (file, fm) {
        var url, reg, info;

        // URL normalization
        url = fm.convAbsUrl(file.url);

        // Make file info
        info = file.name + ' (' + fm.formatSize(file.size) + ')';

        // Provide file and text for the link dialog
        if (meta.filetype == 'file') {
          callback(url);
        }

        // Provide image and alt text for the image dialog
        if (meta.filetype == 'image') {
          callback(url, {alt: info});
        }

        // Provide alternative source and posted for the media dialog
        if (meta.filetype == 'media') {
          callback(url);
        }
      }
    });
    return false;
  }
</script>

<?php
if (isset($GLOBALS['config']['head'])) {
    foreach ($GLOBALS['config']['head'] as $sHtml) {
        print $sHtml;
        print "\n";
        print "\n";
    }
}
