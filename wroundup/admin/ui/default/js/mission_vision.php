<?php $this->load->view('includes/header');?>
<title>Mission & Vision - Potomac Officers Club</title>
<?php $this->load->view('includes/navbar');?>

            <!-- About Us -->
            <section id="aboutus" class="aboutus" style="padding-top: 0em;margin-top: 0em">
                <div class="container">
                    <div class="features-content">
                        <div class="row">
                            <div class="col-md-8" style="margin-top: 4em;">
                                <div id="poc">
                                    <h4 class=" "  data-animate="fadeInDown"  data-delay="0" style="text-align: left;">ABOUT POTOMAC OFFICERS CLUB</h4>
                                    <div class="flexbox di_3_of_3  first" data-animate="fadeInUp" data-delay="50">
                                        <div class="no-box features-start">
                                            <div class="features-content">
                                                <p style="color: black;">
                                                 The Potomac Officers Club is owned and operated by Executive Mosaic. Since 2002, Executive Mosaic has been a leader in media and exclusive senior executive networking programs primarily targeted to the U.S. federal government contracting (GovCon) marketplace. In addition to connecting executives of consequence into a fabric of like minded communities, the company’s growing cross-media platform serves as a leading source of news about the people, policies, trends and events shaping the nation’s GovCon sector.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div id="mission">
                                    <h4 class=" "  data-animate="fadeInDown"  data-delay="0" style="text-align: left;">MISSION STATEMENT</h4>
                                    <div class="flexbox di_3_of_3  first" data-animate="fadeInUp" data-delay="50">
                                        <div class="no-box features-start">
                                            <div class="features-content">
                                                <p style="color: black;">
                                                 The Potomac Officers Club (POC) is a membership organization dedicated to connecting and empowering executives within the Government Contracting community. POC provides the opportunity to learn from peer business executives, hear from government thought leaders, and create an outstanding forum to develop key business and partnerships.

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div id="vission">
                                    <h4 class=" "  data-animate="fadeInDown"  data-delay="0" style="text-align: left;">VISION STATEMENT</h4>
                                    <div class="flexbox di_3_of_3  first" data-animate="fadeInUp" data-delay="50">
                                        <div class="no-box features-start">
                                            <div class="features-content">
                                                <p style="color: black;">
                                                 The Potomac Officers Club is dedicated to weaving an executive level fabric for the benefit of the government contracting community. We strive to be the perfect forum to engage in meaningful dialogue on current issues and find great value in building our members credibility and visibility.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $this->load->view('includes/sidebar');?>
                        </div><!-- End off row -->
                    </div>
            </div><!-- End off container -->
            </section><!-- End off Featured Section-->
        </div>
        <?php $this->load->view('includes/footer');?>
        <!-- JS includes -->
        <script src="<?php echo base_url('assets/js/vendor/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/owl.carousel.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.magnific-popup.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.easing.1.3.js'); ?>"></script>
        <script src="<?php echo base_url('assets/cssv2/slick/slick.js'); ?>"></script>
        <script src="<?php echo base_url('assets/cssv2/slick/slick.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.collapse.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/plugins.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/main_new.js'); ?>"></script>
        <script type="text/javascript">
        	$(document).ready(function(){
                $('.navbar-nav .dropdown > a:not(a[href="#"])').on('click', function() {
                    self.location = $(this).attr('href');
                });
				$(this).scrollTop(0);
			});
        	
        </script>
     </body>
</html>
