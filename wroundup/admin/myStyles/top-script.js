const content = CKEDITOR.instances.template.getData();
const $div = $('<div>').html(content);
$div.find('.remove-after').attr("style", "padding-bottom: 0 !important;");
$div.find('.remove-after-2').attr("style", "");
const processedHTML = $div.html();
document.getElementById('w-template').innerHTML = processedHTML;