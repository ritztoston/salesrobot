<?php

require_once dirname(__FILE__).'/accesscheck.php';

$actionresult = '';

if (!empty($_FILES['file_template']) && is_uploaded_file($_FILES['file_template']['tmp_name'])) {
    $content = file_get_contents($_FILES['file_template']['tmp_name']);
} elseif (isset($_POST['template'])) {
    $content = $_POST['template'];
} else {
    $content = '';
}
$sendtestresult = '';
$testtarget = getConfig('admin_address');
$systemTemplateID = getConfig('systemmessagetemplate');

if (isset($_REQUEST['id'])) {
    $id = sprintf('%d', $_REQUEST['id']);
} else {
    $id = 0;
}

function getTemplateImages($content)
{
    $image_types = array(
        'gif'  => 'image/gif',
        'jpg'  => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpe'  => 'image/jpeg',
        'bmp'  => 'image/bmp',
        'png'  => 'image/png',
        'tif'  => 'image/tiff',
        'tiff' => 'image/tiff',
        'swf'  => 'application/x-shockwave-flash',
    );
    $regexp = sprintf('/"([^"]+\.(%s))"/Ui', implode('|', array_keys($image_types)));
    preg_match_all($regexp, stripslashes($content), $images);

    return array_count_values($images[1]);
}

function getTemplateLinks($content)
{
    preg_match_all('/href="([^"]+)"/Ui', stripslashes($content), $links);

    return $links[1];
}

$msg = '';
$checkfullimages = !empty($_POST['checkfullimages']) ? 1 : 0;
$checkimagesexist = !empty($_POST['checkimagesexist']) ? 1 : 0;
$checkfulllinks = !empty($_POST['checkfulllinks']) ? 1 : 0;
$baseurl = '';

if (!empty($_POST['action']) && $_POST['action'] == 'addimages') {
    if (!$id) {
        $msg = $GLOBALS['I18N']->get('No such template');
    } else {
        $content_req = Sql_Fetch_Row_Query("select template from {$tables['template']} where id = $id");
        $images = getTemplateImages($content_req[0]);

        if (count($images)) {
            include 'class.image.inc';
            $image = new imageUpload();
            foreach ($images as $key => $val) {
                // printf('Image name: <b>%s</b> (%d times used)<br />',$key,$val);
                $image->uploadImage($key, $id);
            }
            $msg = $GLOBALS['I18N']->get('Images stored');
        } else {
            $msg = $GLOBALS['I18N']->get('No images found');
        }
    }
    $_SESSION['action_result'] = $msg.'<br/>'.s('Template saved and ready for use in campaigns');
    Redirect('templates');

    return;
    //print '<p class="actionresult">'.$msg.'</p>';
    //$msg = '';
} elseif (!empty($_POST['save']) || !empty($_POST['sendtest'])) { //# let's save when sending a test
    $templateok = 1;
    $title = $_POST['title'];
//    if (!empty($title) && strpos($content, '[CONTENT]') !== false) {
    if (!empty($title)) {
        $images = getTemplateImages($content);

        //   var_dump($images);

        if (($checkfullimages || $checkimagesexist) && count($images)) {
            foreach ($images as $key => $val) {
                if (!preg_match('#^https?://#i', $key)) {
                    if ($checkfullimages) {
                        $actionresult .= $GLOBALS['I18N']->get('Image')." $key => ".$GLOBALS['I18N']->get('"not full URL')."<br/>\n";
                        $templateok = 0;
                    }
                } else {
                    if ($checkimagesexist) {
                        $imageFound = testUrl($key);
                        if ($imageFound != 200) {
                            $actionresult .= $GLOBALS['I18N']->get('Image')." $key => ".$GLOBALS['I18N']->get('does not exist')."<br/>\n";
                            $templateok = 0;
                        }
                    }
                }
            }
        }
        if ($checkfulllinks) {
            $links = getTemplateLinks($content);
            foreach ($links as $key => $val) {
                if (!preg_match('#^https?://#i', $val) && !preg_match('#^mailto:#i', $val)
                    && !(strtoupper($val) == '[PREFERENCESURL]' || strtoupper($val) == '[UNSUBSCRIBEURL]' || strtoupper($val) == '[BLACKLISTURL]' || strtoupper($val) == '[FORWARDURL]' || strtoupper($val) == '[CONFIRMATIONURL]')
                ) {
                    $actionresult .= $GLOBALS['I18N']->get('Not a full URL').": $val<br/>\n";
                    $templateok = 0;
                }
            }
        }
    } else {
        if (!$title) {
            $actionresult .= $GLOBALS['I18N']->get('No Title').'<br/>';
        } /*else {
            $actionresult .= $GLOBALS['I18N']->get('Template does not contain the [CONTENT] placeholder').'<br/>';
        }*/
        $templateok = 0;
    }
    if ($templateok) {
        if (!$id) {
            Sql_Query(sprintf('insert into %s (title) values("%s")', $tables['template'], sql_escape($title)));
            $id = Sql_Insert_id();
        }
        Sql_Query(sprintf('update %s set title = "%s",template = "%s" where id = %d',
            $tables['template'], sql_escape($title), sql_escape($content), $id));
        Sql_Query(sprintf('select * from %s where filename = "%s" and template = %d',
            $tables['templateimage'], 'powerphplist.png', $id));
        if (!Sql_Affected_Rows()) {
            Sql_Query(sprintf('insert into %s (template,mimetype,filename,data,width,height)
      values(%d,"%s","%s","%s",%d,%d)',
                $tables['templateimage'], $id, 'image/png', 'powerphplist.png',
                $newpoweredimage,
                70, 30));
        }
        $actionresult .= '<p class="information">'.s('Template saved').'</p>';

        //# ##17419 don't prompt for remote images that exist
        $missingImages = array();
        foreach ($images as $key => $val) {
            $key = trim($key);
            if (preg_match('~^https?://~i', $key)) {
                $imageFound = testUrl($key);
                if (!$imageFound) {
                    $missingImages[$key] = $val;
                }
            } else {
                $missingImages[$key] = $val;
            }
        }

        if (count($missingImages) && empty($_POST['sendtest'])) {
            include dirname(__FILE__).'/class.image.inc';
            $image = new imageUpload();
            echo '<h3>'.$GLOBALS['I18N']->get('Images').'</h3><p class="information">'.$GLOBALS['I18N']->get('Below is the list of images used in your template. If an image is currently unavailable, please upload it to the database.').'</p>';
            echo '<p class="information">'.$GLOBALS['I18N']->get('This includes all images, also fully referenced ones, so you may choose not to upload some. If you upload images, they will be included in the campaigns that use this template.').'</p>';
            echo formStart('enctype="multipart/form-data" class="template1" ');
            echo '<input type="hidden" name="id" value="'.$id.'" />';
            ksort($images);
            reset($images);
            foreach ($images as $key => $val) {
                $key = trim($key);
                if (preg_match('~^https?://~i', $key)) {
                    $missingImage = true;
                    $imageFound = testUrl($key);
                    if ($imageFound != 200) {
                        printf($GLOBALS['I18N']->get('Image name:').' <b>%s</b> ('.$GLOBALS['I18N']->get('%d times used').')<br/>',
                            $key, $val);
                        echo $image->showInput($key, $val, $id);
                    }
                } else {
                    printf($GLOBALS['I18N']->get('Image name:').' <b>%s</b> ('.$GLOBALS['I18N']->get('%d times used').')<br/>',
                        $key, $val);
                    echo $image->showInput($key, $val, $id);
                }
            }

            echo '<input type="hidden" name="id" value="'.$id.'" /><input type="hidden" name="action" value="addimages" />
        <input class="submit" type="submit" name="addimages" value="' .$GLOBALS['I18N']->get('Save Images').'" /></form>';
            if (empty($_POST['sendtest'])) {
                return;
            }
            //    return;
        } else {
            $_SESSION['action_result'] = s('Template was successfully saved');
//      print '<p class="information">'.$GLOBALS['I18N']->get('Template does not contain local images')."</p>";
            if (empty($_POST['sendtest'])) {
                Redirect('templates');

                return;
            }
            //    return;
        }
    } else {
        $actionresult .= $GLOBALS['I18N']->get('Some errors were found, template NOT saved!');
        $data['title'] = $title;
        $data['template'] = $content;
    }
    if (!empty($_POST['sendtest'])) {
        //# check if it's the system message template or a normal one:

        $targetEmails = explode(',', $_POST['testtarget']);
        $testtarget = '';

        if ($id == $systemTemplateID) {
            $actionresult .= '<h3>'.$GLOBALS['I18N']->get('Sending test').'</h3>';
            foreach ($targetEmails as $email) {
                if (validateEmail($email)) {
                    $testtarget .= $email.', ';
                    $actionresult .= $GLOBALS['I18N']->get('Sending test "Request for confirmation" to').' '.$email.'  ';
                    if (sendMail($email, getConfig('subscribesubject'), getConfig('subscribemessage'))) {
                        $actionresult .= s('OK');
                    } else {
                        $actionresult .= s('FAILED');
                    }
                    $actionresult .= '<br/>';
                    $actionresult .= $GLOBALS['I18N']->get('Sending test "Welcome" to').' '.$email.'  ';
                    if (sendMail($email, getConfig('confirmationsubject'), getConfig('confirmationmessage'))) {
                        $actionresult .= s('OK');
                    } else {
                        $actionresult .= s('FAILED');
                    }
                    $actionresult .= '<br/>';
                    $actionresult .= $GLOBALS['I18N']->get('Sending test "Unsubscribe confirmation" to').' '.$email.'  ';
                    if (sendMail($email, getConfig('unsubscribesubject'), getConfig('unsubscribemessage'))) {
                        $actionresult .= s('OK');
                    } else {
                        $actionresult .= s('FAILED');
                    }
                } elseif (trim($email) != '') {
                    $actionresult .= '<p>'.$GLOBALS['I18N']->get('Error sending test messages to').' '.htmlspecialchars($email).'</p>';
                }
            }
        } else {
            //# Sending test emails of non system templates to be added.
            $actionresult .= '<p>'.s('Sending a test from templates only works for the system template.').' '.
                s('To test your template, go to campaigns and send a test campaign using the template.').
                '</p>';
        }
        if (empty($testtarget)) {
            $testtarget = getConfig('admin_address');
        }
        $testtarget = preg_replace('/, $/', '', $testtarget);
    }
}
if (!empty($actionresult)) {
    echo '<div class="actionresult">'.$actionresult.'</div>';
}

if ($id) {
    $req = Sql_Query("select * from {$tables['template']} where id = $id");
    $data = Sql_Fetch_Array($req);
    //# keep POSTED data, even if not saved
    if (!empty($_POST['template'])) {
        $data['template'] = $content;
    }
} else {
    $data = array();
    $data['title'] = '';
    $data['template'] = '';
}

?>
<div class="responsive">

  <!-- LOADING -->
  <div class="all_content">
    <div class="dima-main" style="float:none;">
      <section>
        <div class="col-md-12 ">
          <div class="col-md-2" style="padding: 30px 10px 10px 10px;">
            <h2 class="hidden"></h2>
            <h3 class="hidden"></h3>
            <h4 class="hidden"></h4>

            <h5 style="text-align: left;color: #828077 !important;">ELEMENTS</h5>
            <div style="padding: 0px 28px 0px 28px;">
              <table width="100%">
                <tr>
                  <td colspan="3" align="center">
                    Basic
                    <div class="clear"></div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="div-element draggable-text" id="drag-text"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="text-element-img" src="icons/text.png" alt="text icon element">
                    </div>
                  </td>
                  <td>
                    &nbsp;&nbsp;&nbsp;
                  </td>
                  <td>
                    <div class="div-element draggable-img" id="drag-img"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="img-element-img" src="icons/img.png" alt="img icon element">
                    </div>
                    <br>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="div-element draggable-left" id="drag-left"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="left-element-img" src="icons/left.png" alt="left icon element">
                    </div>
                  </td>
                  <td>
                    &nbsp;&nbsp;&nbsp;
                  </td>
                  <td>
                    <div class="div-element draggable-right" id="drag-right"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="right-element-img" src="icons/right.png" alt="right icon element">
                    </div>
                    <br>
                  </td>
                </tr>
              </table>
            </div>
            <div class="clear"></div>
            <div style="padding: 0px 28px 0px 28px">
              <table width="100%">
                <tr>
                  <td colspan="3" align="center">
                    Structure
                    <div class="clear"></div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="div-element draggable-divider" id="drag-divider"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="divider-element-img" src="icons/divider.png" alt="divider icon element">
                    </div>
                  </td>
                  <td>
                    &nbsp;&nbsp;&nbsp;
                  </td>
                  <td>
                    <div class="div-element draggable-btn" id="drag-btn"
                         style="text-align: center;background: white;border: 2px solid #ccc;border-radius: 5px;">
                      <img class="btn-element-img" src="icons/btn.png" alt="btn icon element">
                    </div>
                    <br>
                  </td>
                </tr>
              </table>
            </div>
            <br>
            <br>
          </div>
          <div class="col-md-9" id="tag-id">
            <div style="margin-top: 2em;border: 1px solid #5BC0DE;box-shadow: 0px 1px 3px 0px #888888;background: #5BC0DE;padding: 12px 10px 12px 10px;color: white;border-radius: 8px 8px 0px 0px;" id="tag-id">
              <nav role="navigation" class="clearfix">
                <!-- menu content -->
                <ul class="dima-menu sf-menu">
                  <li class="sub-icon" style="font-size: 18px;float: left;padding: 10px;">
                    <!--                  kim-->
                    <p style="padding: 0;margin: 0;float: left;" class="my-title">Untitled Mailing</p><a id="edit-title" onclick="title_dialog(this)" data-toggle="modal" style="color: black;margin-left: 5px;"><i class="fa fa-pencil"></i></a>
                    <div class="modal fade title-modal" role="dialog">
                      <div class="modal-dialog modal-sm">

                        <!-- kim Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Template Name</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                            <div class="clear"></div>
                            <div class="">
                              <div class="accordion">
                                <div class="accordion-section">
                                  <div class="accordion-section-title">
                                    <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                      <tbody>
                                      <tr>
                                        <td style="width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                          Text
                                        </td>
                                        <td style="width:70%;border: none !important;">
                                          <div>
                                            <input type="text" name="btn_text" class="btn_text-title" style="background: white !important">
                                          </div>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
                <!-- !menu content -->
              </nav>
            </div>
            <div id="w-template" class="col-md-12"
                 style="border: 1px solid #D0CDBC;background: #3b4856;box-shadow: 0px 1px 3px 0px #888888;height: 600px;text-align:center; overflow: auto">
              <div class=""
                   style="background: #3b4856;margin-top: 1em; margin-bottom: 1em;padding-top:10px; padding-bottom: 10px;display:inline-block;">
                <div class="poc-combo-block poc-content-block we" id="tag-id">
                  <div class="poc-content-block-outline" id="tag-id">
                    <ul class="dima-menu sf-menu" style="float: right;">
                      <li style="display: flex;" class="actions-icon">
                        <a onclick="" class="a-icon-edit" data-toggle="modal" id="edit-right" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>
                      </li>
                    </ul>
                  </div>
                  <!-- HEADER - TEMPLATE START -->

                  <!--[if mso | IE]>
                  <table
                          align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                  >
                    <tr>
                      <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->


                  <div  style="background:#eeeeee;background-color:#eeeeee;Margin:0px auto;max-width:600px;">

                    <table
                            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#eeeeee;background-color:#eeeeee;width:100%;"
                    >
                      <tbody>
                      <tr>
                        <td
                                style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;"
                        >
                          <!--[if mso | IE]>
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                              <td
                                      class="" style="width:600px;"
                              >
                          <![endif]-->

                          <div
                                  class="mj-column-per-100 outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;"
                          >
                            <!--[if mso | IE]>
                            <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
                              <tr>

                                <td
                                        style="vertical-align:top;width:300px;"
                                >
                            <![endif]-->

                            <div
                                    class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;"
                            >

                              <table
                                      border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                              >

                                <tr>
                                  <td
                                          align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                  >

                                    <div
                                            style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;font-weight:bold;line-height:1;text-align:left;color:#696969;"
                                    >
                                      The Week's Top GovCon News Stories
                                    </div>

                                  </td>
                                </tr>

                              </table>

                            </div>

                            <!--[if mso | IE]>
                            </td>

                            <td
                                    style="vertical-align:top;width:300px;"
                            >
                            <![endif]-->

                            <div
                                    class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;"
                            >

                              <table
                                      border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                              >

                                <tr>
                                  <td
                                          align="right" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                  >

                                    <div
                                            style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:right;color:#000000;"
                                    >
                                      <a href="#" style="color:#1e90ff;text-decoration:none;">[VIEWBROWSER]</a>
                                    </div>

                                  </td>
                                </tr>

                              </table>

                            </div>

                            <!--[if mso | IE]>
                            </td>

                            </tr>
                            </table>
                            <![endif]-->
                          </div>

                          <!--[if mso | IE]>
                          </td>

                          </tr>

                          </table>
                          <![endif]-->
                        </td>
                      </tr>
                      </tbody>
                    </table>

                  </div>


                  <!--[if mso | IE]>
                  </td>
                  </tr>
                  </table>

                  <table
                          align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                  >
                    <tr>
                      <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->


                  <div  style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">

                    <table
                            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                    >
                      <tbody>
                      <tr>
                        <td
                                style="border:7px solid #eeeeee;border-bottom:0px;direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;"
                        >
                          <!--[if mso | IE]>
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                              <td
                                      class="" style="width:600px;"
                              >
                          <![endif]-->

                          <div
                                  class="mj-column-per-100 outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;"
                          >
                            <!--[if mso | IE]>
                            <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
                              <tr>

                                <td
                                        style="vertical-align:top;width:300px;"
                                >
                            <![endif]-->

                            <div
                                    class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;"
                            >

                              <table
                                      border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                              >

                                <tr>
                                  <td
                                          align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                  >

                                    <table
                                            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                    >
                                      <tbody>
                                      <tr>
                                        <td  style="width:250px;">

                                          <img
                                                  alt="govcon_wire_weekly_logo" height="auto" src="https://gallery.mailchimp.com/9447c61bfb9058cb4553aedbf/images/865658f8-b047-4e2f-b7dc-f4329b208211.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;" width="250"
                                          />

                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>

                                  </td>
                                </tr>

                              </table>

                            </div>

                            <!--[if mso | IE]>
                            </td>

                            <td
                                    style="vertical-align:top;width:300px;"
                            >
                            <![endif]-->

                            <div
                                    class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;"
                            >

                              <table
                                      border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                              >

                                <tr>
                                  <td
                                          align="right" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                  >

                                    <table
                                            align="right" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                    >
                                      <tbody>
                                      <tr>
                                        <td  style="width:100px;">

                                          <img
                                                  alt="govcon_index" height="auto" src="https://gallery.mailchimp.com/9447c61bfb9058cb4553aedbf/images/9bb8626b-30de-4911-b444-4dbc81c9b6a2.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;" width="100"
                                          />

                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>

                                  </td>
                                </tr>

                              </table>

                            </div>

                            <!--[if mso | IE]>
                            </td>

                            </tr>
                            </table>
                            <![endif]-->
                          </div>

                          <!--[if mso | IE]>
                          </td>

                          </tr>

                          </table>
                          <![endif]-->
                        </td>
                      </tr>
                      </tbody>
                    </table>

                  </div>


                  <!--[if mso | IE]>
                  </td>
                  </tr>
                  </table>

                  <table
                          align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                  >
                    <tr>
                      <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->


                  <div  style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">

                    <table
                            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                    >
                      <tbody>
                      <tr>
                        <td
                                style="border:7px solid #eeeeee;border-bottom:0px;border-top:0px;direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;"
                        >
                          <!--[if mso | IE]>
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                              <td
                                      class="" style="vertical-align:top;width:600px;"
                              >
                          <![endif]-->

                          <div
                                  class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                          >

                            <table
                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                            >

                              <tr>
                                <td
                                        align="right" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >

                                  <div
                                          style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:right;color:#000000;"
                                  >
                                    <a href="http://www.govconindex.com/" style="text-decoration:none;color:#1e90ff;font-size:12px;">Click here to see Real-Time GovCon Sector Quotes</a>
                                  </div>

                                </td>
                              </tr>

                            </table>

                          </div>

                          <!--[if mso | IE]>
                          </td>

                          </tr>

                          </table>
                          <![endif]-->
                        </td>
                      </tr>
                      </tbody>
                    </table>

                  </div>


                  <!--[if mso | IE]>
                  </td>
                  </tr>
                  </table>
                  <table
                          align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                  >
                    <tr>
                      <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->


                  <div  style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">

                    <table
                            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                    >
                      <tbody>
                      <tr>
                        <td
                                style="border:7px solid #eeeeee;border-top:0px;direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;"
                        >
                          <!--[if mso | IE]>
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                              <td
                                      class="" style="vertical-align:top;width:600px;"
                              >
                          <![endif]-->

                          <div
                                  class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                          >

                            <table
                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                            >

                              <tr>
                                <td
                                        align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >

                                  <div
                                          style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;font-weight:bold;line-height:1;text-align:center;color:#696969;"
                                  >
                                    Weekly Roundup | July 9 - July 13, 2018
                                  </div>

                                </td>
                              </tr>

                            </table>

                          </div>

                          <!--[if mso | IE]>
                          </td>

                          </tr>

                          </table>
                          <![endif]-->
                        </td>
                      </tr>
                      </tbody>
                    </table>

                  </div>


                  <!--[if mso | IE]>
                  </td>
                  </tr>
                  </table>
                  <![endif]-->

                  <!-- HEADER - TEMPLATE END -->
                  <!-- JIM GARRETTSON - TEMPLATE START -->

                  <!--[if mso | IE]>
                  <table
                          align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                  >
                    <tr>
                      <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->


                  <div  style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">

                    <table
                            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                    >
                      <tbody>
                      <tr>
                        <td
                                style="border:7px solid #eeeeee;direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;"
                        >
                          <!--[if mso | IE]>
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                              <td
                                      class="" style="width:600px;"
                              >
                          <![endif]-->

                          <div
                                  class="mj-column-per-100 outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;"
                          >
                            <!--[if mso | IE]>
                            <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
                              <tr>

                                <td
                                        style="vertical-align:top;width:180px;"
                                >
                            <![endif]-->

                            <div
                                    class="mj-column-per-30 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:30%;"
                            >

                              <table
                                      border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                              >
                                <tbody>
                                <tr>
                                  <td  style="vertical-align:top;padding:0px;">

                                    <table
                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                    >

                                      <tr>
                                        <td
                                                align="center" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-right:0px;padding-bottom:0px;word-break:break-word;"
                                        >

                                          <table
                                                  align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                          >
                                            <tbody>
                                            <tr>
                                              <td  style="width:155px;">

                                                <img
                                                        alt="govcon_jim" height="auto" src="https://gallery.mailchimp.com/9447c61bfb9058cb4553aedbf/images/3599c14c-180e-42a6-abf2-dc49dbe725ca.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;" width="155"
                                                />

                                              </td>
                                            </tr>
                                            </tbody>
                                          </table>

                                        </td>
                                      </tr>

                                    </table>

                                  </td>
                                </tr>
                                </tbody>
                              </table>

                            </div>

                            <!--[if mso | IE]>
                            </td>

                            <td
                                    style="vertical-align:top;width:420px;"
                            >
                            <![endif]-->

                            <div
                                    class="mj-column-per-70 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:70%;"
                            >

                              <table
                                      border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                              >
                                <tbody>
                                <tr>
                                  <td  style="vertical-align:top;padding:0px;">

                                    <table
                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                    >

                                      <tr>
                                        <td
                                                align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                        >

                                          <div
                                                  style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:18px;font-weight:bold;line-height:1;text-align:center;color:#696969;"
                                          >
                                            Executive Mosaic&rsquo;s Weekly GovCon Round-Up: $400M+ July Contracts &amp; Top 10 Stories
                                          </div>

                                        </td>
                                      </tr>

                                      <tr>
                                        <td
                                                align="left" style="font-size:0px;padding:10px 25px;padding-top:10%;word-break:break-word;"
                                        >

                                          <div
                                                  style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:14px;line-height:1;text-align:left;color:#696969;"
                                          >
                                            Jim Garrettson, Executive Mosaic CEO
                                          </div>

                                        </td>
                                      </tr>

                                    </table>

                                  </td>
                                </tr>
                                </tbody>
                              </table>

                            </div>

                            <!--[if mso | IE]>
                            </td>

                            </tr>
                            </table>
                            <![endif]-->
                          </div>

                          <!--[if mso | IE]>
                          </td>

                          </tr>

                          </table>
                          <![endif]-->
                        </td>
                      </tr>
                      </tbody>
                    </table>

                  </div>


                  <!--[if mso | IE]>
                  </td>
                  </tr>
                  </table>
                  <![endif]-->
                  <!-- JIM GARRETTSON - TEMPLATE END -->
                  <!-- TEXT ONLY - TEMPLATE START -->
                  <!--[if mso | IE]>
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600">
                    <tr>
                      <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
                      <tbody>
                      <tr>
                        <td style="border:7px solid #eeeeee;border-bottom:0px;direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;"></td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                  <!--[if mso | IE]>
                  </td>
                  </tr>
                  </table>
                  <![endif]-->
                  <!-- TEXT ONLY - TEMPLATE END -->
                </div>
                <div class="clear" style="padding-bottom: 0px !important;"></div>
                <div class="draggableContainer " id="tag-id">
                  <div class="poc-drop-zone-text" style="display:none;padding: 10px;">
                    <span class="poc-drop-here" style="display:none">Drop Here</span>
                  </div>
                </div>
                <div class="poc-combo-block poc-content-block we" id="tag-id">
                  <div class="poc-content-block-outline" id="tag-id">
                    <ul class="dima-menu sf-menu" style="float: right;">
                      <li style="display: flex;" class="actions-icon">
                        <a onclick="left_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-left" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>
                        <a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>
                      </li>
                    </ul>
                  </div>
                  <!-- IMAGE AT LEFT OF TEXT - TEMPLATE START -->

                  <!--[if mso | IE]>
                  <table
                          align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                  >
                    <tr>
                      <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->


                  <div  style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">

                    <table
                            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                    >
                      <tbody>
                      <tr>
                        <td
                                style="border:7px solid #eeeeee;border-bottom:0px;border-top:0px;direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:0px;text-align:center;vertical-align:top;"
                        >
                          <!--[if mso | IE]>
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                              <td
                                      class="" style="vertical-align:top;width:120px;"
                              >
                          <![endif]-->

                          <div
                                  class="mj-column-per-20 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                          >

                            <table
                                    border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                            >
                              <tbody>
                              <tr>
                                <td  style="vertical-align:top;padding:5px;">

                                  <table
                                          border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                  >

                                    <tr>
                                      <td
                                              align="center" style="font-size:0px;padding:0px;word-break:break-word;"
                                      >

                                        <table
                                                align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                        >
                                          <tbody>
                                          <tr>
                                            <td  style="width:110px;">
                                              <div class="left-img-content">
                                                <img height="auto" src="http://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;" width="110"/>
                                              </div>
                                            </td>
                                          </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>

                                  </table>

                                </td>
                              </tr>
                              </tbody>
                            </table>

                          </div>

                          <!--[if mso | IE]>
                          </td>

                          <td
                                  class="" style="vertical-align:top;width:480px;"
                          >
                          <![endif]-->

                          <div
                                  class="mj-column-per-80 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                          >

                            <table
                                    border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                            >
                              <tbody>
                              <tr>
                                <td  style="vertical-align:top;padding:5px;">

                                  <table
                                          border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                  >

                                    <tr>
                                      <td
                                              align="justify" style="font-size:0px;padding:0px;word-break:break-word;"
                                      >

                                        <div class="left-text-content"
                                             style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:14px;text-align:justify;color:#000000;"
                                        >
                                          <span style="font-family: 'times new roman', times, serif; font-size: 12pt;"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit. <em>Etiam vel condimentum libero</em>,<span style="text-decoration: underline;">eget tempus mi</span>. <a href="google.com">Pellentesque</a> id luctus dolor, eu viverra mauris. <strong>Suspendisse</strong> nec venenatis lectus. Morbi fringilla fermentum malesuada. Aliquam interdum odio lorem, <span style="text-decoration: underline;">non convallis lacus</span> ultricies ac.<strong>Orci varius natoque</strong> penatibus <a href="google.com">et magnis</a> dis parturient montes, nascetur ridiculus mus. Nulla tincidunt augue non arcu scelerisque luctus sit amet in ligula.</span>
                                        </div>

                                      </td>
                                    </tr>

                                  </table>

                                </td>
                              </tr>
                              </tbody>
                            </table>

                          </div>

                          <!--[if mso | IE]>
                          </td>

                          </tr>

                          </table>
                          <![endif]-->
                        </td>
                      </tr>
                      </tbody>
                    </table>

                  </div>


                  <!--[if mso | IE]>
                  </td>
                  </tr>
                  </table>
                  <![endif]-->

                  <!-- IMAGE AT LEFT OF TEXT - TEMPLATE END -->
                </div>
                <div class="clear" style="padding-bottom: 0px !important;"></div>
                <div class="draggableContainer " id="tag-id">
                  <div class="poc-drop-zone-text" style="display:none;">
                    <span class="poc-drop-here" style="display:none">Drop Here</span>
                  </div>
                </div>
                <div class="poc-combo-block poc-content-block we" id="tag-id">
                  <div class="poc-content-block-outline" id="tag-id">
                    <ul class="dima-menu sf-menu" style="float: right;">
                      <li style="display: flex;" class="actions-icon">
                        <a onclick="right_dialog(this)" class="a-icon-edit" data-toggle="modal" id="edit-right" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-pencil" style="font-size: 20px;"></i></a>
                        <a onclick="delete_element(this)" class="a-icon-del" style="padding: 3px 10px 3px 10px;border: 1px solid;border-radius: 0px 0px 6px 6px;"><i class="fa fa-trash" style="font-size: 20px;"></i></a>
                      </li>
                    </ul>
                  </div>
                  <!-- IMAGE AT RIGHT OF TEXT - TEMPLATE START -->

                  <!--[if mso | IE]>
                  <table
                          align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
                  >
                    <tr>
                      <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->


                  <div  style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">

                    <table
                            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                    >
                      <tbody>
                      <tr>
                        <td
                                style="border:7px solid #eeeeee;border-bottom:0px;border-top:0px;direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:0px;text-align:center;vertical-align:top;"
                        >
                          <!--[if mso | IE]>
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                              <td
                                      class="" style="vertical-align:top;width:480px;"
                              >
                          <![endif]-->

                          <div
                                  class="mj-column-per-80 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                          >

                            <table
                                    border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                            >
                              <tbody>
                              <tr>
                                <td  style="vertical-align:top;padding:5px;">

                                  <table
                                          border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                  >

                                    <tr>
                                      <td
                                              align="justify" style="font-size:0px;padding:0px;word-break:break-word;"
                                      >

                                        <div class="right-text-content"
                                             style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:14px;text-align:justify;color:#000000;"
                                        >
                                          <span style="font-family: 'times new roman', times, serif; font-size: 12pt;"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit. <em>Etiam vel condimentum libero</em>,<span style="text-decoration: underline;">eget tempus mi</span>. <a href="google.com">Pellentesque</a> id luctus dolor, eu viverra mauris. <strong>Suspendisse</strong> nec venenatis lectus. Morbi fringilla fermentum malesuada. Aliquam interdum odio lorem, <span style="text-decoration: underline;">non convallis lacus</span> ultricies ac.<strong>Orci varius natoque</strong> penatibus <a href="google.com">et magnis</a> dis parturient montes, nascetur ridiculus mus. Nulla tincidunt augue non arcu scelerisque luctus sit amet in ligula.</span>
                                        </div>

                                      </td>
                                    </tr>

                                  </table>

                                </td>
                              </tr>
                              </tbody>
                            </table>

                          </div>

                          <!--[if mso | IE]>
                          </td>

                          <td
                                  class="" style="vertical-align:top;width:120px;"
                          >
                          <![endif]-->

                          <div
                                  class="mj-column-per-20 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                          >

                            <table
                                    border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                            >
                              <tbody>
                              <tr>
                                <td  style="vertical-align:top;padding:5px;">

                                  <table
                                          border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                  >

                                    <tr>
                                      <td
                                              align="center" style="font-size:0px;padding:0px;word-break:break-word;"
                                      >

                                        <table
                                                align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                        >
                                          <tbody>
                                          <tr>
                                            <td style="width:110px;">
                                              <div class="right-img-content">
                                                <img alt="right" height="auto" src="http://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;" width="110"/>
                                              </div>
                                            </td>
                                          </tr>
                                          </tbody>
                                        </table>

                                      </td>
                                    </tr>

                                  </table>

                                </td>
                              </tr>
                              </tbody>
                            </table>

                          </div>

                          <!--[if mso | IE]>
                          </td>

                          </tr>

                          </table>
                          <![endif]-->
                        </td>
                      </tr>
                      </tbody>
                    </table>

                  </div>


                  <!--[if mso | IE]>
                  </td>
                  </tr>
                  </table>
                  <![endif]-->

                  <!-- IMAGE AT RIGHT OF TEXT - TEMPLATE END -->
                </div>
                <div class="clear" style="padding-bottom: 0px !important;"></div>
                <div class="draggableContainer " id="tag-id">
                  <div class="poc-drop-zone-text" style="display:none;">
                    <span class="poc-drop-here" style="display:none">Drop Here</span>
                  </div>
                </div>


                <!--<div>
                  <textarea id="m1-tinymce">ew</textarea>
                </div>-->
              </div>
            </div>


            <table id="modals">
              <tr>
                <td>
                  <div class="modal fade img-modal" role="dialog">
                    <div class="modal-dialog modal-sm">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Replace Image</h4>
                        </div>
                        <div class="modal-body">
                          <form role="form" id="uploadMassEmail" method="post" enctype="multipart/form-data">
                            <div>
                              <input type="hidden" name="div-id-img" id="div-id-img" value="">
                              <input type='file' accept='image/*' name="profile_pic" class="profile_pic"
                                     style="margin-bottom: 7px;"/>
                              <img class="hover hidden pp-img" alt="image" style="width:586px;height:135px;"/>
                            </div>
                            <div class="hidden img_loader" style="text-align:center">
                              <p>Please wait</p>
                              <img src="icons/loader.gif" style="height: 2em;width: 24em;">
                            </div>
                            <div class="modal-footer">
                              <button type="submit" class="submit_btn text-center btn btn-success" id="update_pic">Update
                              </button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="modal fade btn-modal" role="dialog">
                    <div class="modal-dialog modal-sm">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Button</h4>
                        </div>
                        <div class="modal-body">
                          <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                          <div class="clear"></div>
                          <div class="">
                            <div class="accordion">
                              <div class="accordion-section">
                                <div class="accordion-section-title">
                                  <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                    <tbody>
                                    <tr>
                                      <td style="width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                        Text
                                      </td>
                                      <td style="width:70%;border: none !important;">
                                        <div>
                                          <input type="text" name="btn_text" class="btn_text" style="background: white !important">
                                        </div>
                                      </td>
                                    </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                              <div class="accordion-section">
                                <div class="accordion-section-title">
                                  <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                    <tbody>
                                    <tr>
                                      <td style="width:30%;border: none !important;padding: 13px 0px 0px 0px;">
                                        Link
                                      </td>
                                      <td style="width:70%;border: none !important;">
                                        <div>
                                          <input type="text" name="src_btn" class="src-btn" style="background: white !important">
                                        </div>
                                      </td>
                                    </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                              <div class="accordion-section">
                                <a class="accordion-section-title" href="#accordion-1">Spacing<span style="float: right;"><i class="fa fa-chevron-down"></i></span></a>
                                <div id="accordion-1" class="accordion-section-content">
                                  <div class="" style="">
                                    <table width="100%">
                                      <tbody>
                                      <tr>
                                        <td style="width:50%">
                                          Top Margin
                                        </td>
                                        <td style="width:50%" align="center">
                                          <div class="input-group" style="width: 70%;">
                                            <input type="text" class="top-margin-btn" name="top-margin-btn" placeholder="" size="5px">
                                            <span class="input-group-addon">PX</span>
                                          </div>
                                          <br>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:50%">
                                          Bottom Margin
                                        </td>
                                        <td style="width:50%" align="center">
                                          <div class="input-group" style="width: 70%;">
                                            <input type="text" class="bottom-margin-btn" name="bottom-margin-btn" placeholder="" size="5px">
                                            <span class="input-group-addon">PX</span>
                                          </div>
                                          <br>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                              <div class="accordion-section">
                                <a class="accordion-section-title" href="#accordion-3">Style<span style="float: right;"><i class="fa fa-chevron-down"></i></a>
                                <div id="accordion-3" class="accordion-section-content">
                                  <div class="" style="">
                                    <table width="100%">
                                      <tbody>
                                      <tr>
                                        <td style="width:50%">
                                          Border
                                        </td>
                                        <td style="width:50%" align="center">
                                          <div class="input-group" style="width: 70%;">
                                            <input type="text" class="border-btn" placeholder="" size="5px">
                                            <span class="input-group-addon">PX</span>
                                          </div>
                                          <br>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:50%">
                                          Border Color
                                        </td>
                                        <td style="width:50%" align="center">
                                          <input type="text" class="border-color-btn">
                                          <br>
                                          <br>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:50%">
                                          Text Color
                                        </td>
                                        <td style="width:50%" align="center">
                                          <input type="text" class="text-color-btn">
                                          <br>
                                          <br>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="width:50%">
                                          Background Color
                                        </td>
                                        <td style="width:50%" align="center">
                                          <input type="text" class="background-color-btn">
                                          <br>
                                          <br>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="modal fade right-modal" role="dialog">
                    <div class="modal-dialog">

                      <!-- kim Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Right Dialog</h4>
                        </div>
                        <div class="modal-body">
                          <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                          <div class="clear"></div>
                          <div class="">
                            <div class="accordion">
                              <div class="accordion-section">
                                <div class="accordion-section-title">
                                  <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                    <tbody>
                                    <tr>
                                      <td style="width:100%;border: none !important;padding: 13px 0px 0px 0px;">
                                        <div id="accordion">
                                          <div class="card">
                                            <div class="card-header" id="headingOne">
                                              <h5 class="mb-0">
                                                <button class="btn btn-primary" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                  Add Image
                                                </button>
                                              </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="height:0px;">
                                              <div class="card-body">
                                                <textarea id="img-tinymce"><img height="auto" src="http://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100px;" width="100" /></textarea>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colspan="2" style="width:30%;border: none !important;padding: 13px 0 0 0;">
                                        Content
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">
                                        <textarea id="m-tinymce">My Text Here</textarea>
                                      </td>
                                    </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="modal fade left-modal" role="dialog">
                    <div class="modal-dialog">

                      <!-- kim Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Left Dialog</h4>
                        </div>
                        <div class="modal-body">
                          <input type="hidden" name="div-id-btn_" id="div-id-btn_" value="">
                          <div class="clear"></div>
                          <div class="">
                            <div class="accordion">
                              <div class="accordion-section">
                                <div class="accordion-section-title">
                                  <table class="table" style="margin-bottom: -13px !important;margin-top: -13px !important;">
                                    <tbody>
                                    <tr>
                                      <td style="width:100%;border: none !important;padding: 13px 0px 0px 0px;">
                                        <div id="accordion">
                                          <div class="card">
                                            <div class="card-header" id="headingOne">
                                              <h5 class="mb-0">
                                                <button class="btn btn-primary" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                  Add Image
                                                </button>
                                              </h5>
                                            </div>

                                            <div id="collapseTwo" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="height:0px;">
                                              <div class="card-body">
                                                <textarea id="img-tinymce-second"><img height="auto" src="http://salesrobot.com/uploadimages/image/default/icon-placeholder-image.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100px;" width="100" /></textarea>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colspan="2" style="width:30%;border: none !important;padding: 13px 0 0 0;">
                                        Content
                                      </td>
                                    </tr>
                                    <tr>
                                      <td colspan="2">
                                        <textarea id="m-tinymce-second">My Text Here</textarea>
                                      </td>
                                    </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>